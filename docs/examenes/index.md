---
title: Examenes
---

# Examenes

| Título                                                                            | Tipo       | Entrega
|:----------------------------------------------------------------------------------|:----------:|:-------------:|
| **Examen 1**: [Primer examen parcial][examen-1] - Moodle FC                       | _Teórico_  | 2023-02-27
| **Examen 2**: [Segundo examen parcial][examen-2] - Moodle FC                      | _Teórico_  | 2023-03-27
| **Examen 3**: [Implementación de un sitio web </u>de producción</u>](examen-web)  | _Práctico_ | 2023-05-23 `¹`
| **Examen 4**: [Implementación de sitios web en **Kubernetes**](examen-kubernetes) | _Práctico_ | 2023-06-02 `¹`

<!--
!!! danger
    - `¹`: La evaluación de este proyecto final corresponderá al **tercer examen parcial** del curso
    - `²`: La evaluación de este proyecto final corresponderá al **cuarto examen parcial** del curso
-->

!!! warning
    - `¹`: Se ajustó el periodo de elaboración de las actividades y las fechas de entrega para compensar los días en los que la Facultad de Ciencias estuvo en paro activo 🟥⬛

[examen-1]: https://moodle.fciencias.unam.mx/cursos/mod/quiz/view.php?id=68560
[examen-2]: https://moodle.fciencias.unam.mx/cursos/mod/quiz/view.php?id=69224

--------------------------------------------------------------------------------

|                 ⇦            |        ⇧      |                  ⇨           |
|:-----------------------------|:-------------:|-----------------------------:|
| [Proyectos][proyectos]       | [Arriba](../) | [Tareas][tareas]             |

[arriba]: ../
[tareas]: ../tareas/
[laboratorio]: ../laboratorio/
[proyectos]: ../proyectos/
[examenes]: ../examenes/
