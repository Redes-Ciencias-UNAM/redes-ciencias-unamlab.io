---
title: Instalación del firewall pfSense
authors:
- Dante Erik Santiago Rodríguez Pérez
- Andrés Leonardo Hernández Bermúdez
---

# Instalación del _firewall_ pfSense

## Diagrama de red

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-------------------------------------------:|
| ![](img/diagrama_red.png)
| 🟠 <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵 <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢 <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet

## Descargar imagen ISO

- Descargar el sistema operativo pfSense, estableciendo el parametro _DVD Image_ (ISO) y l arquitectura _AMD64_, posteriormente descomprimir el archivo

| Descarga del ISO de instalación
|:-------------------------------:|
| ![](img/pfsense/pfsense-001-website.png)
| ![](img/pfsense/pfsense-002-download.png)

## Crear máquina virtual

Crear una máquina virtual en Virtualbox con 3 interfaces de red:

!!! note
    - La máquina virtual puede tener solo **512 MB** de RAM y **10 GB** de disco

| Máquina virtual pfSense
|:-----------------------:|
| ![](img/pfsense/pfsense-003-virtualbox-new.png)
| ![](img/pfsense/pfsense-004-virtualbox-new-type-os.png)
| ![](img/pfsense/pfsense-005-virtualbox-new-vcpu-ram.png)
| ![](img/pfsense/pfsense-006-virtualbox-new-disk.png)
| ![](img/pfsense/pfsense-007-virtualbox-new-summary.png)

Configurar las interfaces de red de la máquina virtual

|  # | Habilitado | Red                                 | Tipo                 | Nombre       | Modo promiscuo | Cable conectado
|:--:|:----------:|:-----------------------------------:|:--------------------:|:------------:|:--------------:|:---------------:|
|  1 | ☑️ Si       |  <span class="green">**WAN**</span> | NAT-Network `¹`      | `NatNetwork` | Deshabilitado  | ☑️ Si
|  2 | ☑️ Si       |   <span class="blue">**LAN**</span> | Host-only `²`        | `vboxnet0`   | **Habilitado** | ☑️ Si
|  3 | ☑️ Si       | <span class="orange">**DMZ**</span> | Internal Network `³` | `intnet`     | **Habilitado** | ☑️ Si

!!! warning
    - `¹`: Crear la red `NAT-Network` con el direccionamiento adecuado
    - `²`: Configurar la red `vboxnet0` con el direccionamiento adecuado
    - `³`: La red `intnet` se crea al asignarse a la máquina virtual

| Interfaces de red en VirtualBox
|:-------------------------------:|
| ![](img/pfsense/pfsense-008-virtualbox-new-settings.png)
| ![](img/pfsense/pfsense-009-virtualbox-new-adapter-nat.png)
| ![](img/pfsense/pfsense-010-virtualbox-new-adapter-hostonly.png)
| ![](img/pfsense/pfsense-011-virtualbox-new-adapter-intnet.png)
| ![](img/pfsense/pfsense-012-virtualbox-new-prepared.png)

- Instalación de pfSense

| Pantallas de instalación
|:------------------------:|
| ![](img/pfsense/pfsense-013-virtualbox-boot.png)
<!--
| ![](img/pfsense/pfsense-014-virtualbox-iso-loader.png)
| ![](img/pfsense/pfsense-015-virtualbox-iso-boot.png)
| ![](img/pfsense/pfsense-016-virtualbox-iso-copyright.png)
| ![](img/pfsense/pfsense-017-virtualbox-iso-welcome.png)
| ![](img/pfsense/pfsense-018-virtualbox-iso-keyboard.png)
| ![](img/pfsense/pfsense-019-virtualbox-iso-partitions.png)
| ![](img/pfsense/pfsense-020-virtualbox-iso-extracting.png)
| ![](img/pfsense/pfsense-021-virtualbox-iso-manual-config.png)
| ![](img/pfsense/pfsense-022-virtualbox-iso-finished.png)
-->

La máquina virtual muestra la pantalla de opciones del cargador de inicio de pfSense.

- Esperar a que se complete el inicio automático (_autoboot_):

```console
-          __
    _ __  / _|___  ___ _ __  ___  ___
   | '_ \| |_/ __|/ _ \ '_ \/ __|/ _ \
   | |_) |  _\__ \  __/ | | \__ \  __/
   | .__/|_| |___/\___|_| |_|___/\___|
   |_|


 +---------- Welcome to pfSense -----------+      __________________________
 |                                         |     /                       ___\
 |  1. Boot Multi user [Enter]             |    |                      /`
 |  2. Boot Single user                    |    |                     /    :-|
 |  3. Escape to loader prompt             |    |      _________  ___/    /_ |
 |  4. Reboot                              |    |    /` ____   / /__    ___/ |
 |  5. Cons: Dual (Serial primary)         |    |   /  /   /  /    /   /     |
 |                                         |    |  /  /___/  /    /   /      |
 |  Options:                               |    | /   ______/    /   /       |
 |  6. Kernel: default/kernel (1 of 1)     |    |/   /          /   /        |
 |  7. Boot Options                        |        /          /___/         |
 |                                         |       /                         |
 |                                         |      /_________________________/
 +-----------------------------------------+                                  /
   Autoboot in 0 seconds. [Space] to pause										⏳
```

<!--	------------------------------------------------------------------------

Loading kernel...
/boot/kernel/kernel text=0x1a5f37c data=0x140 data=0x1b63940 syms=[0x8+0x1c1370+0x8+0x1c7bb8]
Loading configured modules...
can't find '/etc/hostid'
/boot/kernel/opensolaris.ko size 0x9860 at 0x3cee000
/size 0x39adb0 at 0x3cf8000
can't find '/boot/entropy'
KDB: debugger backends: ddb
KDB: current backend: ddb
---<<BOOT>>---
Copyright (c) 1992-2021 The FreeBSD Project.
Copyright (c) 1979, 1980, 1983, 1986, 1988, 1989, 1991, 1992, 1993, 1994
The Regents of the University of California. All rights reserved.
FreeBSD is a registered trademark of The FreeBSD Foundation.
FreeBSD 12.3-STABLE RELENG_2_6_0-n226742-1285d6d205f pfSense amd64
FreeBSD clang version 10.0.1 (git@github.com:llvm/llvm-project.git llvmorg-10.0.1-0-gef32c611aa2)
VT(vga): text 80x25
CPU: Intel(R) Core(TM) i5-8500B CPU @ 3.00GHz (3000.05-MHz K8-class CPU)
  Origin="GenuineIntel"  Id=0x906ea  Family=0x6  Model=0x9e  Stepping=10
  Features=0x1783fbff<FPU,VME,DE,PSE,TSC,MSR,PAE,MCE,CX8,APIC,SEP,MTRR,PGE,MCA,CMOV,PAT,PSE36,MMX,FXSR,SSE,SSE2,HTT>
  Features2=0x5eda220b<SSE3,PCLMULQDQ,MON,SSSE3,CX16,PCID,SSE4.1,SSE4.2,MOVBE,POPCNT,AESNI,XSAVE,OSXSAVE,AVX,RDRAND>
  AMD Features=0x28100800<SYSCALL,NX,RDTSCP,LM>
  AMD Features2=0x121<LAHF,ABM,Prefetch>
  Structured Extended Features=0x842529<FSGSBASE,BMI1,AVX2,BMI2,INVPCID,NFPUSG,RDSEED,CLFLUSHOPT>
  Structured Extended Features3=0x30000400<MD_CLEAR,L1DFL,ARCH_CAP>
  TSC: P-state invariant
real memory  = 536805376 (511 MB)
avail memory = 450437120 (429 MB)
Event timer "LAPIC" quality 100
ACPI APIC Table: <VBOX   VBOXAPIC>
arc4random: no preloaded entropy cache
ioapic0: MADT APIC ID 1 != hw id 0
ioapic0 <Version 2.0> irqs 0-23 on motherboard
Timecounter "TSC-low" frequency 1500023793 Hz quality 1000
random: entropy device external interface
ipw_bss: You need to read the LICENSE file in /usr/share/doc/legal/intel_ipw.LICENSE.
ipw_bss: If you agree with the license, set legal.intel_ipw.license_ack=1 in /boot/loader.conf.
module_register_init: MOD_LOAD (ipw_bss_fw, 0xffffffff80739000, 0) error 1
ipw_ibss: You need to read the LICENSE file in /usr/share/doc/legal/intel_ipw.LICENSE.
ipw_ibss: If you agree with the license, set legal.intel_ipw.license_ack=1 in /boot/loader.conf.
module_register_init: MOD_LOAD (ipw_ibss_fw, 0xffffffff807390b0, 0) error 1
ipw_monitor: You need to read the LICENSE file in /usr/share/doc/legal/intel_ipw.LICENSE.
ipw_monitor: If you agree with the license, set legal.intel_ipw.license_ack=1 in /boot/loader.conf.
module_register_init: MOD_LOAD (ipw_monitor_fw, 0xffffffff80739160, 0) error 1
iwi_bss: You need to read the LICENSE file in /usr/share/doc/legal/intel_iwi.LICENSE.
iwi_bss: If you agree with the license, set legal.intel_iwi.license_ack=1 in /boot/loader.conf.
module_register_init: MOD_LOAD (iwi_bss_fw, 0xffffffff807609f0, 0) error 1
iwi_ibss: You need to read the LICENSE file in /usr/share/doc/legal/intel_iwi.LICENSE.
iwi_ibss: If you agree with the license, set legal.intel_iwi.license_ack=1 in /boot/loader.conf.
module_register_init: MOD_LOAD (iwi_ibss_fw, 0xffffffff80760aa0, 0) error 1
iwi_monitor: You need to read the LICENSE file in /usr/share/doc/legal/intel_iwi.LICENSE.
iwi_monitor: If you agree with the license, set legal.intel_iwi.license_ack=1 in /boot/loader.conf.
module_register_init: MOD_LOAD (iwi_monitor_fw, 0xffffffff80760b50, 0) error 1
wlan: mac acl policy registered
WARNING: Device "g_ctl" is Giant locked and may be deleted before FreeBSD 14.0.
WARNING: Device "pci" is Giant locked and may be deleted before FreeBSD 14.0.
module_register_init: MOD_LOAD (vesa, 0xffffffff8140a210, 0) error 19
WARNING: Device "kbd" is Giant locked and may be deleted before FreeBSD 14.0.
kbd1 at kbdmux0
[ath_hal] loaded
random: registering fast source Intel Secure Key RNG
random: fast provider: "Intel Secure Key RNG"
WARNING: Device "spkr" is Giant locked and may be deleted before FreeBSD 14.0.
000.000052 [4344] netmap_init               netmap: loaded module
mlx5en: Mellanox Ethernet driver 3.6.0 (December 2020)
nexus0
vtvga0: <VT VGA driver> on motherboard
cryptosoft0: <software crypto> on motherboard
acpi0: <VBOX VBOXXSDT> on motherboard
acpi0: Power Button (fixed)
acpi0: Sleep Button (fixed)
cpu0: <ACPI CPU> on acpi0
attimer0: <AT timer> port 0x40-0x43,0x50-0x53 on acpi0
Timecounter "i8254" frequency 1193182 Hz quality 0
Event timer "i8254" frequency 1193182 Hz quality 100
Timecounter "ACPI-fast" frequency 3579545 Hz quality 900
acpi_timer0: <32-bit timer at 3.579545MHz> port 0x4008-0x400b on acpi0
pcib0: <ACPI Host-PCI bridge> port 0xcf8-0xcff on acpi0
pci0: <ACPI PCI bus> on pcib0
isab0: <PCI-ISA bridge> at device 1.0 on pci0
isa0: <ISA bus> on isab0
atapci0: <Intel PIIX4 UDMA33 controller> port 0x1f0-0x1f7,0x3f6,0x170-0x177,0x376,0xd000-0xd00f at device 1.1 on pci0
ata0: <ATA channel> at channel 0 on atapci0
ata1: <ATA channel> at channel 1 on atapci0
vgapci0: <VGA-compatible display> port 0xd010-0xd01f mem 0xe0000000-0xe0ffffff,0xf0000000-0xf01fffff irq 18 at device 2.0 on pci0
vgapci0: Boot video device
em0: <Intel(R) Legacy PRO/1000 MT 82540EM> port 0xd020-0xd027 mem 0xf0200000-0xf021ffff irq 19 at device 3.0 on pci0
em0: Using 1024 TX descriptors and 1024 RX descriptors
em0: Ethernet address: 08:00:27:3c:fd:1e
em0: netmap queues/slots: TX 1/1024, RX 1/1024
pcm0: <Intel ICH (82801AA)> port 0xd100-0xd1ff,0xd200-0xd23f irq 21 at device 5.0 on pci0
pcm0: <SigmaTel STAC9700/83/84 AC97 Codec>
ohci0: <Apple KeyLargo/Intrepid USB controller> mem 0xf0804000-0xf0804fff irq 22 at device 6.0 on pci0
usbus0 on ohci0
pci0: <bridge> at device 7.0 (no driver attached)
em1: <Intel(R) Legacy PRO/1000 MT 82540EM> port 0xd240-0xd247 mem 0xf0820000-0xf083ffff irq 16 at device 8.0 on pci0
em1: Using 1024 TX descriptors and 1024 RX descriptors
em1: Ethernet address: 08:00:27:27:4a:4c
em1: netmap queues/slots: TX 1/1024, RX 1/1024
em2: <Intel(R) Legacy PRO/1000 MT 82540EM> port 0xd248-0xd24f mem 0xf0840000-0xf085ffff irq 17 at device 9.0 on pci0
em2: Using 1024 TX descriptors and 1024 RX descriptors
em2: Ethernet address: 08:00:27:76:8e:48
em2: netmap queues/slots: TX 1/1024, RX 1/1024
ehci0: <Intel 82801FB (ICH6) USB 2.0 controller> mem 0xf0860000-0xf0860fff irq 19 at device 11.0 on pci0
usbus1: EHCI version 1.0
usbus1 on ehci0
ahci0: <Intel ICH8M AHCI SATA controller> port 0xd250-0xd257,0xd258-0xd25b,0xd260-0xd267,0xd268-0xd26b,0xd270-0xd27f mem 0xf0862000-0xf0863fff irq 21 at device 13.0 on pci0
ahci0: AHCI v1.10 with 1 3Gbps ports, Port Multiplier not supported
ahcich0: <AHCI channel> at channel 0 on ahci0
uart0: <Non-standard ns8250 class UART with FIFOs> port 0x3f8-0x3ff irq 4 flags 0x10 on acpi0
uart0: console (115200,n,8,1)
acpi_acad0: <AC Adapter> on acpi0
atkbdc0: <Keyboard controller (i8042)> port 0x60,0x64 irq 1 on acpi0
atkbd0: <AT Keyboard> irq 1 on atkbdc0
kbd0 at atkbd0
atkbd0: [GIANT-LOCKED]
psm0: <PS/2 Mouse> irq 12 on atkbdc0
psm0: [GIANT-LOCKED]
WARNING: Device "psm" is Giant locked and may be deleted before FreeBSD 14.0.
psm0: model IntelliMouse Explorer, device ID 4
orm0: <ISA Option ROMs> at iomem 0xc0000-0xc7fff,0xe2000-0xe2fff pnpid ORM0000 on isa0
vga0: <Generic ISA VGA> at port 0x3c0-0x3df iomem 0xa0000-0xbffff pnpid PNP0900 on isa0
atrtc0: <AT realtime clock> at port 0x70 irq 8 on isa0
atrtc0: registered as a time-of-day clock, resolution 1.000000s
Event timer "RTC" frequency 32768 Hz quality 0
ZFS NOTICE: Prefetch is disabled by default if less than 4GB of RAM is present;
            to enable, add "vfs.zfs.prefetch_disable=0" to /boot/loader.conf.
ZFS WARNING: Recommended minimum RAM size is 512MB; expect unstable behavior.
ZFS WARNING: Recommended minimum kmem_size is 512MB; expect unstable behavior.
             Consider tuning vm.kmem_size and vm.kmem_size_max
             in /boot/loader.conf.
ZFS filesystem version: 5
ZFS storage pool version: features support (5000)
Timecounters tick every 10.000 msec
pcm0: measured ac97 link rate at 43671 Hz
usbus0: 12Mbps Full Speed USB v1.0
usbus1: 480Mbps High Speed USB v2.0
ugen1.1: <Intel EHCI root HUB> at usbus1
uhub0: <Intel EHCI root HUB, class 9/0, rev 2.00/1.00, addr 1> on usbus1
ugen0.1: <Apple OHCI root HUB> at usbus0
uhub1: <Apple OHCI root HUB, class 9/0, rev 1.00/1.00, addr 1> on usbus0
Trying to mount root from ufs:/dev/ufs/FreeBSD_Install [ro,noatime]...
Root mount waiting for: CAM usbus0 usbus1
uhub1: 12 ports with 12 removable, self powered
Root mount waiting for: CAM usbus1
Root mount waiting for: CAM usbus1
Root mount waiting for: CAM usbus1
Root mount waiting for: CAM usbus1
uhub0: 12 ports with 12 removable, self powered
Root mount waiting for: CAM
Root mount waiting for: CAM
Root mount waiting for: CAM
Root mount waiting for: CAM
ada0 at ata0 bus 0 scbus0 target 0 lun 0
ada0: <VBOX HARDDISK 1.0> ATA-6 device
ada0: Serial Number VB184dabaf-cc0f5ed9
ada0: 33.300MB/s transfers (UDMA2, PIO 65536bytes)
ada0: 922MB (1889937 512 byte sectors)
ada1 at ahcich0 bus 0 scbus2 target 0 lun 0
ada1: <VBOX HARDDISK 1.0> ATA8-ACS SATA 2.x device
ada1: Serial Number VB4b0d2c35-1576f99c
ada1: 300.000MB/s transfers (SATA 2.x, UDMA6, PIO 8192bytes)
ada1: Command Queueing enabled
ada1: 10240MB (20971520 512 byte sectors)
cd0 at ata1 bus 0 scbus1 target 0 lun 0
cd0: <VBOX CD-ROM 1.0> Removable CD-ROM SCSI device
cd0: Serial Number VB2-01700376
cd0: 33.300MB/s transfers (UDMA2, ATAPI 12bytes, PIO 65534bytes)
cd0: Attempt to query device size failed: NOT READY, Medium not present
mountroot: waiting for device /dev/ufs/FreeBSD_Install...
Dual Console: Serial Primary, Video Secondary
Setting hostuuid: f3b484a5-ed7e-8446-948a-f2096cc28726.
Setting hostid: 0xbf062321.
Starting file system checks:
/dev/ufs/FreeBSD_Install: FILE SYSTEM CLEAN; SKIPPING CHECKS
/dev/ufs/FreeBSD_Install: clean, 13600 free (120 frags, 1685 blocks, 0.1% fragmentation)
eval: cannot create /etc/hostid: Read-only file system
/etc/rc: WARNING: could not store hostuuid in /etc/hostid.
Generating host.conf.
eval: cannot create /etc/host.conf: Read-only file system
eval: cannot create /etc/host.conf: Read-only file system
eval: cannot create /etc/host.conf: Read-only file system
Mounting local filesystems:.
mtree: /etc/mtree/BSD.sendmail.dist: No such file or directory
ELF ldconfig path: /lib /usr/lib /usr/lib/compat
random: unblocking device.
32-bit compatibility ldconfig path:
/etc/rc: WARNING: $hostname is not set -- see rc.conf(5).
Setting up harvesting: PURE_RDRAND,[UMA],[FS_ATIME],SWI,INTERRUPT,NET_NG,NET_ETHER,NET_TUN,MOUSE,KEYBOARD,ATTACH,CACHED
Feeding entropy: dd: /entropy: Read-only file system
dd: /boot/entropy: Read-only file system
.
Autoloading module: intpm
intsmb0: <Intel PIIX4 SMBUS Interface> irq 23 at device 7.0 on pci0
intsmb0: intr IRQ 9 enabled revision 0
smbus0: <System Management Bus> on intsmb0
lo0: link state changed to UP
Starting Network: lo0 em0 em1 em2 enc0.
lo0: flags=8049<UP,LOOPBACK,RUNNING,MULTICAST> metric 0 mtu 16384
options=680003<RXCSUM,TXCSUM,LINKSTATE,RXCSUM_IPV6,TXCSUM_IPV6>
inet6 ::1 prefixlen 128
inet6 fe80::1%lo0 prefixlen 64 scopeid 0x5
inet 127.0.0.1 netmask 0xff000000
groups: lo
nd6 options=21<PERFORMNUD,AUTO_LINKLOCAL>
em0: flags=8802<BROADCAST,SIMPLEX,MULTICAST> metric 0 mtu 1500
options=81009b<RXCSUM,TXCSUM,VLAN_MTU,VLAN_HWTAGGING,VLAN_HWCSUM,VLAN_HWFILTER>
ether 08:00:27:3c:fd:1e
em0: link state changed to UP
media: Ethernet autoselect (1000baseT <full-duplex>)
status: active
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
em1: flags=8802<BROADCAST,SIMPLEX,MULTICAST> metric 0 mtu 1500
options=81009b<RXCSUM,TXCSUM,VLAN_MTU,VLAN_HWTAGGING,VLAN_HWCSUM,VLAN_HWFILTER>
ether 08:00:27:27:4a:4c
em1: link state changed to UP
media: Ethernet autoselect (1000baseT <full-duplex>)
status: active
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
em2: flags=8802<BROADCAST,SIMPLEX,MULTICAST> metric 0 mtu 1500
options=81009b<RXCSUM,TXCSUM,VLAN_MTU,VLAN_HWTAGGING,VLAN_HWCSUM,VLAN_HWFILTER>
ether 08:00:27:76:8e:48
em2: link state changed to UP
media: Ethernet autoselect (1000baseT <full-duplex>)
status: active
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
enc0: flags=0<> metric 0 mtu 1536
groups: enc
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
Starting devd.
Starting Network: em0.
em0: flags=8802<BROADCAST,SIMPLEX,MULTICAST> metric 0 mtu 1500
options=81009b<RXCSUM,TXCSUM,VLAN_MTU,VLAN_HWTAGGING,VLAN_HWCSUM,VLAN_HWFILTER>
ether 08:00:27:3c:fd:1e
media: Ethernet autoselect (1000baseT <full-duplex>)
status: active
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
Starting Network: em1.
em1: flags=8802<BROADCAST,SIMPLEX,MULTICAST> metric 0 mtu 1500
options=81009b<RXCSUM,TXCSUM,VLAN_MTU,VLAN_HWTAGGING,VLAN_HWCSUM,VLAN_HWFILTER>
ether 08:00:27:27:4a:4c
media: Ethernet autoselect (1000baseT <full-duplex>)
status: active
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
Starting Network: em2.
em2: flags=8802<BROADCAST,SIMPLEX,MULTICAST> metric 0 mtu 1500
options=81009b<RXCSUM,TXCSUM,VLAN_MTU,VLAN_HWTAGGING,VLAN_HWCSUM,VLAN_HWFILTER>
ether 08:00:27:76:8e:48
media: Ethernet autoselect (1000baseT <full-duplex>)
status: active
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
Starting Network: enc0.
enc0: flags=0<> metric 0 mtu 1536
groups: enc
nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
add host 127.0.0.1: gateway lo0 fib 0: route already in table
add host ::1: gateway lo0 fib 0: route already in table
add net fe80::: gateway ::1
add net ff02::: gateway ::1
add net ::ffff:0.0.0.0: gateway ::1
add net ::0.0.0.0: gateway ::1
Creating and/or trimming log files.
Updating motd: /etc/motd is not writable, update failed.
Updating /var/run/os-release done.
Starting syslogd.
Clearing /tmp (X related).
Starting local daemons:/dev/md3: 8.0MB (16384 sectors) block size 32768, fragment size 4096
using 4 cylinder groups of 2.03MB, 65 blks, 384 inodes.
super-block backups (for fsck_ffs -b #) at:
 192, 4352, 8512, 12672

Welcome to pfSense!

Please choose the appropriate terminal type for your system.
Common console types are:
   ansi     Standard ANSI terminal
   vt100    VT100 or compatible terminal
   xterm    xterm terminal emulator (or compatible)
   cons25w  cons25w terminal

Console type [vt100]:

------------------------------------------------------------------------	-->

El programa de instalación muestra la pantalla con las notas de _copyright_ de pfSense

- Presionar **Accept** para continuar

```console
 ┌────────────────────Copyright and distribution notice───────────────────────┐
 │ Copyright and Trademark Notices.                                           │
 │                                                                            │
 │ Copyright(c) 2004-2016. Electric Sheep Fencing, LLC ("ESF").               │
 │ All Rights Reserved.                                                       │
 │                                                                            │
 │ Copyright(c) 2014-2022. Rubicon Communications, LLC d/b/a Netgate          │
 │ ("Netgate").                                                               │
 │ All Rights Reserved.                                                       │
 │                                                                            │
 │ All logos, text, and content of ESF and/or Netgate, including underlying   │
 │ HTML code, designs, and graphics used and/or depicted herein are           │
 │ protected under United States and international copyright and trademark    │
 │ laws and treaties, and may not be used or reproduced without the prior     │
 │ express written permission of ESF and/or Netgate.                          │
 │                                                                            │
 │ "pfSense" is a registered trademark of ESF, exclusively licensed to        │
 │ Netgate, and may not be used without the prior express written             │
 │ permission of ESF and/or Netgate. All other trademarks shown herein are    │
 │ owned by the respective companies or persons indicated.                    │
 │                                                                            │
 ├───────────────────────────────────────────────────────────────────────28%──┤
 │                                 <Accept>                                   │	⬅️
 └────────────────────────────────────────────────────────────────────────────┘
```

El instalador muestra una serie de opciones

- Seleccionar **Install** y presionar **OK** para continuar

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────




   ┌───────────────────────────────Welcome──────────────────────────────────┐
   │ Welcome to pfSense!                                                    │
   │ ┌────────────────────────────────────────────────────────────────────┐ │
   │ │██ Install ███████████ Install pfSense ████████████████████████████ │ │	⬅️
   │ │   Rescue Shell        Launch a shell for rescue operations         │ │
   │ │   Recover config.xml  Recover config.xml from a previous install   │ │
   │ └────────────────────────────────────────────────────────────────────┘ │
   │                                                                        │
   │                                                                        │
   ├────────────────────────────────────────────────────────────────────────┤
   │                     <  OK  >           <Cancel>                        │	⬅️	OK
   └────────────────────────────────────────────────────────────────────────┘




```

El instalador muestra la pantalla para seleccionar una distribución de teclado.

- Seleccionar la distribución adecuada, o bien continuar con la opción predeterminada
- Presionar **Select** para continuar

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────
      ┌───────────────────────Keymap Selection──────────────────────────┐
      │ The system console driver for pfSense defaults to standard "US" │
      │ keyboard map. Other keymaps can be chosen below.                │
      │ ┌─────────────────────────────────────────────────────────────┐ │
      │ │>>> Continue with default keymap ███████████████████████████ │ │	⬅️
      │ │->- Test default keymap                                      │ │
      │ │( ) Armenian phonetic layout                                 │ │
      │ │( ) Belarusian                                               │ │
      │ │( ) Belgian                                                  │ │
      │ │( ) Belgian (accent keys)                                    │ │
      │ │( ) Brazilian (accent keys)                                  │ │
      │ │( ) Brazilian (without accent keys)                          │ │
      │ │( ) Bulgarian (BDS)                                          │ │
      │ │( ) Bulgarian (Phonetic)                                     │ │
      │ │( ) Canadian Bilingual                                       │ │
      │ └────┴(+)─────────────────────────────────────────────12%─────┘ │
      ├─────────────────────────────────────────────────────────────────┤
      │                   <Select>          <Cancel>                    │	⬅️	Select
      └──────────────────[Press arrows, TAB or ENTER]───────────────────┘
```

El instalador muestra la pantalla con las opciones de particionado del disco

- Seleccionar **Auto (UFS) BIOS** para continuar

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────



     ┌───────────────────────────Partitioning─────────────────────────────┐
     │ How would you like to partition your disk?                         │
     │ ┌────────────────────────────────────────────────────────────────┐ │
     │ │   Auto (ZFS)        Guided Root-on-ZFS                         │ │
     │ │██ Auto (UFS) BIOS █ Guided Disk Setup using BIOS boot method ██│ │	⬅️
     │ │   Auto (UFS) UEFI   Guided Disk Setup using UEFI boot method   │ │
     │ │   Manual            Manual Disk Setup (experts)                │ │
     │ │   Shell             Open a shell and partition by hand         │ │
     │ └────────────────────────────────────────────────────────────────┘ │
     │                                                                    │
     │                                                                    │
     ├────────────────────────────────────────────────────────────────────┤
     │                   <  OK  >           <Cancel>                      │	⬅️	OK
     └────────────────────────────────────────────────────────────────────┘



```

El instalador muestra los discos disponibles para particionar

- Seleccionar el disco `ada0`

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────





            ┌───────────────────Partitioning─────────────────────┐
            │ Select the disk on which to install pfSense        │
            │ ┌────────────────────────────────────────────────┐ │
            │ │██ ada0:  10 GB ATA Hard Disk <VBOX HARDDISK> ██│ │	⬅️
            │ └────────────────────────────────────────────────┘ │
            │                                                    │
            ├────────────────────────────────────────────────────┤
            │            <  OK  >          <Cancel>              │	⬅️	OK
            └────────────────────────────────────────────────────┘





```

El instalador pregunta si se utilizará todo el disco `ada0` para la pfSense

- Seleccionar **Entire Disk**

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────





                  ┌───────────────Partition──────────────────┐
                  │ Would you like to use this entire disk   │
                  │ (ada0) for pfSense or partition it to    │
                  │ share it with other operating systems?   │
                  │ Using the entire disk will erase any     │
                  │ data currently stored there.             │
                  ├──────────────────────────────────────────┤
                  │     <Entire Disk>   < Partition >        │	⬅️	Entire Disk
                  └──────────────────────────────────────────┘





```

El instalador pide confirmación para borrar el contenido del disco `ada0`

- Seleccionar **Yes**

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────






                         ┌──────Confirmation─────────┐
                         │ This will erase the disk. │
                         │ Are you sure you want to  │
                         │ proceed?                  │
                         ├───────────────────────────┤
                         │     < Yes >   < No  >     │	⬅️	Yes
                         └───────────────────────────┘






```

El instalador pregunta el esquema de particionamiento

- Seleccionar **MBR**

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────


                  ┌────────────Partition Scheme──────────────┐
                  │ Select a partition scheme for this       │
                  │ volume:                                  │
                  │ ┌──────────────────────────────────────┐ │
                  │ │   APM    Apple Partition Map         │ │
                  │ │   BSD    BSD Labels                  │ │
                  │ │   GPT    GUID Partition Table        │ │
                  │ │██ MBR ██ DOS Partitions █████████████│ │	⬅️
                  │ │   VTOC8  Sun VTOC8 Partition Table   │ │
                  │ └──────────────────────────────────────┘ │
                  │                                          │
                  │                                          │
                  ├──────────────────────────────────────────┤
                  │         <  OK  >     <Cancel>            │	⬅️	OK
                  └──────────────────────────────────────────┘




 Bootable on most x86 systems
```

El instalador muestra las particiones que se van a crear en el disco `ada0`

- Seleccionar **Finish** para particionar el disco

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────


            ┌──────────────────Partition Editor────────────────────┐
            │ Please review the disk setup. When complete, press   │
            │ the Finish button.                                   │
            │┌────────────────────────────────────────────────────┐│
            ││ada0            10 GB   MBR                         ││
            ││  ada0s1        10 GB   BSD                         ││
            ││    ada0s1a     9.5 GB  freebsd-ufs    /            ││
            ││    ada0s1b     512 MB  freebsd-swap   none         ││
            ││                                                    ││
            ││                                                    ││
            ││                                                    ││
            ││                                                    ││
            │└────────────────────────────────────────────────┴(+)┘│
            ├──────────────────────────────────────────────────────┤
            │<Create> <Delete> <Modify> <Revert> < Auto > <Finish> │	⬅️	Finish
            └──────────────────────────────────────────────────────┘



 Exit partitioner (will ask whether to save changes)
```

El instalador pide confirmación para hacer los cambios al disco `ada0`

- Seleccionar **Commit**


```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────






             ┌──────────────────Confirmation─────────────────────┐
             │ Your changes will now be written to disk. If you  │
             │ have chosen to overwrite existing data, it will   │
             │ be PERMANENTLY ERASED. Are you sure you want to   │
             │ commit your changes?                              │
             ├───────────────────────────────────────────────────┤
             │ <   Commit    > <Revert & Exit> <    Back     >   │	⬅️	Commit
             └───────────────────────────────────────────────────┘







 Exit partitioner (will ask whether to save changes)
```

Se revisa la suma de verificación de los archivos de instalación de pfSense

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────




                 ┌──────────Checksum Verification────────────┐
                 │                                           │
                 │ base.txz                   [In Progress ] │
                 │                                           │
                 │ Verifying checksums of selected           │
                 │ distributions.                            │
                 │                                           │
                 │  ┌─Overall Progress────────────────────┐  │
                 │  │                  0%                 │  │	⏳
                 │  └─────────────────────────────────────┘  │
                 └───────────────────────────────────────────┘




```

Esperar a que el proceso de instalación de pfSense termine de copiar los archivos al disco

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────





                      ┌───────Archive Extraction─────────┐
                      │ Extracting distribution files... │
                      │                                  │
                      │                                  │
                      │   Overall Progress:              │
                      │  ┌────────────────────────────┐  │
                      │  │██           10%            │  │	⏳
                      │  └────────────────────────────┘  │
                      └──────────────────────────────────┘





```

El proceso de instalación ha terminado

- Seleccionar **Yes** para abrir un _shell_ y apagar el equipo
- Esto es necesario para quitar el disco de instalación

```console
 pfSense Installer
 ──────────────────────────────────────────────────────────────────────────────





                    ┌───────Manual Configuration──────────┐
                    │ The installation is now finished.   │
                    │ Before exiting the installer, would │
                    │ you like to open a shell in the new │
                    │ system to make any final manual     │
                    │ modifications?                      │
                    ├─────────────────────────────────────┤
                    │         < Yes >     < No  >         │	⬅️	Yes
                    └─────────────────────────────────────┘





```

Ejecutar el comando `poweroff` para apagar el equipo y finalizar la instalación

```console
This shell is operating in a chroot in the new system. When finished making configuration changes, type "exit".
# poweroff	⬅️

2023-03-29T07:58:37.274233+00:00  shutdown 1551 - - power-down by root:

System shutdown time has arrived

Mar 29 07:58:37  syslogd: exiting on signal 15

Waiting (max 60 seconds) for system process `vnlru' to stop... done
Waiting (max 60 seconds) for system process `syncer' to stop...
Syncing disks, vnodes remaining... 0 0 0 done
Waiting (max 60 seconds) for system thread `bufdaemon' to stop... done
Waiting (max 60 seconds) for system thread `bufspacedaemon-0' to stop... done
All buffers synced.
Uptime: 10m11s
acpi0: Powering system off
```

Quitar la imagen ISO de instalación de la unidad de CD/DVD

| Quitar imagen ISO
|:-----------------:|
| ![](img/pfsense/pfsense-023-virtualbox-iso-eject.png)

--------------------------------------------------------------------------------

## Configuración inicial en modo texto

- Ingresar a la consola y establecer las direcciones IP de acuerdo a los parámetros indicados a continuación:

| Interfaz                                        | Red                  | Nombre       | Tipo     | Dirección IP   | DHCP VirtualBox
|:-----------------------------------------------:|:--------------------:|:------------:|:--------:|:--------------:|:---------------:|
| `em0` -  <span class="green">**Red WAN**</span> | NAT-Network `¹`      | `NatNetwork` | DHCP     | 10.0.2.**Y**   | **Habilitado**
| `em1` -   <span class="blue">**Red LAN**</span> | Host-Only `²`        | `vboxnet0`   | Estática | 192.168.42.254 | Deshabilitado
| `em2` - <span class="orange">**Red DMZ**</span> | Internal Network `³` | `intnet`     | Estática | 172.16.1.254   | Deshabilitado

!!! warning
    - `¹`: Crear la red `NAT-Network` con el direccionamiento adecuado
    - `²`: Configurar la red `vboxnet0` con el direccionamiento adecuado
    - `³`: La red `intnet` se crea al asignarse a la máquina virtual

<!--
- Seleccionar del menú la opción 1, para asignar las interfaces
    - Establecer que <u>no se necesitan VLANs</u>
    - Definir interfaz de la <span class="green">**Red WAN**</span>: `em0`
    - Definir interfaz de la <span class="blue">**Red LAN**</span>: `em1`
    - Definir interfaz de la <span class="orange">**Red DMZ**</span> (<span class="orange">**OPT**</span>): `em2`
    - Aceptar los cambios
    - Seleccionar del menú la opción 2, establecer las direcciones IP a cada interfaz
    - Seleccionar la interfaz 1 (<span class="green">**WAN**</span>)
    - Establecer dirección IP estática, máscara de red y gateway
    - No permitir el DHCP v6
    - No permitir revertir al protocolo http
    - Finalizar
- Repetir pasos para la interfaz <span class="blue">**LAN**</span>
- Establecer en la interfaz <span class="blue">**LAN**</span> el servidor DHCP y un rango de direcciones IP
- Repetir pasos para la interfaz <span class="orange">**OPT**</span> (<span class="orange">**Red DMZ**</span>)
- Cambiar el nombre de la interfaz <span class="orange">**OPT**</span> a `DMZ`
-->

Esperar a que la máquina virtual inicie para ver el menú de la consola en modo texto de pfSense:

```console
FreeBSD/amd64 (pfSense.home.arpa) (ttyu0)

VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***



 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Enable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option:
```

### Asignar interfaces de red

Seleccionar la opción 1 para asignar las interaces a la red correspondiente:

```console
VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***



 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces 🔆              10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Enable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option:	1	⬅️
```

Se muestran los nombres y direcciones MAC de las interfaces de red para ayudar a identificarlas:

```console
Valid interfaces are:

em0     08:00:27:3c:fd:1e   (up) Intel(R) Legacy PRO/1000 MT 82540EM	🟢
em1     08:00:27:27:4a:4c   (up) Intel(R) Legacy PRO/1000 MT 82540EM	🔵
em2     08:00:27:76:8e:48 (down) Intel(R) Legacy PRO/1000 MT 82540EM	🟠
```

La interfaz de configuración pregunta si se deben configurar vLANs para encapsulamiento de paquetes.

- Seleccionar la opción `n` porque esto no es requerido en esta actividad:

```console
Do VLANs need to be set up first?
If VLANs will not be used, or only for optional interfaces, it is typical to
say no here and use the webConfigurator to configure VLANs later, if required.

Should VLANs be set up now [y|n]?	n	⬅️
```

La interfaz muestra un mensaje sobre la auto-detección de las interfaces de red.

- Ignorar este mensaje porque esta actividad requiere que se asignen manualmente las interfaces:

```console
If the names of the interfaces are not known, auto-detection can
be used instead. To use auto-detection, please disconnect all
interfaces before pressing 'a' to begin the process.
```

Asignar la interfaz `em0` a la red `WAN`:

```console
Enter the WAN interface name or 'a' for auto-detection
(em0 em1 em2 or a):	em0	⬅️
```

Asignar la interfaz `em1` a la red `LAN`:

```console
Enter the LAN interface name or 'a' for auto-detection
NOTE: this enables full Firewalling/NAT mode.
(em1 em2 a or nothing if finished):	em1	⬅️
```

Asignar la interfaz `em2` a la red `OPT1`:

```console
Optional interface 1 description found: OPT1
Enter the Optional 1 interface name or 'a' for auto-detection
(em2 a or nothing if finished):	em2	⬅️
```

La interfaz muestra la asignación de interfaces a las redes correspondientes.

- Seleccionar la opción `y` para confirmar los cambios:

```console
The interfaces will be assigned as follows:

🟢 WAN  -> em0
🔵 LAN  -> em1
🟠 OPT1 -> em2

Do you want to proceed [y|n]?	y	⬅️

Writing configuration...done.
One moment while the settings are reloading... done!
```

### Asignar direcciones IP

#### Configurar interfaz **WAN** 🟢

Seleccionar la opción `2` en la interfaz de configuración de modo texto de pfSense:

```console
VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***

🟢 WAN (wan)       -> em0      -> v4/DHCP4:	10.0.2.15/24		✅
🔵 LAN (lan)       -> em1      -> v4:		192.168.1.1/24		❌
🟠 OPT1 (opt1)     -> em2      ->								❗

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address 🔆    11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option:	2	⬅️
```

La interfaz muestra la asignación actual de interfaces, redes y configuración IP.

- Seleccionar la opción `1` para configurar la interfaz **WAN**:

```console
Available interfaces:

1 - WAN (em0 - dhcp, dhcp6)	🔆
2 - LAN (em1 - static)
3 - OPT1 (em2)

Enter the number of the interface you wish to configure:	1	⬅️
```

Seleccionar las opciones para configurar la interfaz **WAN** via DHCP:

```console
Configure IPv4 address WAN interface via DHCP? (y/n)	y	⬅️

Configure IPv6 address WAN interface via DHCP6? (y/n)	y	⬅️
Disabling IPv4 DHCPD...
Disabling IPv6 DHCPD...
```

Seleccionar la opción `n` para dejar la interfaz de configuración web en HTTPS:

```console
Do you want to revert to HTTP as the webConfigurator protocol? (y/n)	n	⬅️
```

Esperar a que se apliquen los cambios:

```console
Please wait while the changes are saved to WAN...
 Reloading filter...
 Reloading routing configuration...
 DHCPD...

The IPv4 WAN address has been set to dhcp	✅

The IPv6 WAN address has been set to dhcp6	✅

Press <ENTER> to continue.	⬅️
```

#### Configurar interfaz **LAN** 🔵

Seleccionar la opción `2` en la interfaz de configuración de modo texto de pfSense:

```console
VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***

🟢 WAN (wan)       -> em0      -> v4/DHCP4:	10.0.2.15/24		✅
🔵 LAN (lan)       -> em1      -> v4:		192.168.1.1/24		❌
🟠 OPT1 (opt1)     -> em2      ->								❗

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address 🔆    11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option:	2	⬅️
```

La interfaz muestra la asignación actual de interfaces, redes y configuración IP.

- Seleccionar la opción `2` para configurar la interfaz **LAN**:

```console
Available interfaces:

1 - WAN (em0 - dhcp, dhcp6)
2 - LAN (em1 - static)		🔆
3 - OPT1 (em2)

Enter the number of the interface you wish to configure:	2	⬅️
```

Introducir la configuración IP de la interfaz **LAN**:

| Elemento           | Valor
|:------------------:|:----------------:|
| Dirección IP       | `192.168.42.254`
| Máscara de subred  | `255.255.255.0` (`24` bits)
| _Gateway_ IPv4     | (_ninguno_)
| Configuración IPv6 | (_ninguna_)

```console
Enter the new LAN IPv4 address.  Press <ENTER> for none:
> 192.168.42.254	⬅️

Subnet masks are entered as bit counts (as in CIDR notation) in pfSense.
e.g. 255.255.255.0 = 24	🔆
     255.255.0.0   = 16
     255.0.0.0     = 8

Enter the new LAN IPv4 subnet bit count (1 to 32):
> 24	⬅️

For a WAN, enter the new LAN IPv4 upstream gateway address.
For a LAN, press <ENTER> for none:
>	⬅️

Enter the new LAN IPv6 address.  Press <ENTER> for none:
>	⬅️
```

Seleccionar la opción `y` para activar el servidor DHCP en la red LAN y proporcionar los rangos de inicio y fin de asignación DHCP:

| Elemento               | Valor
|:----------------------:|:----------------:|
| Dirección inicial DHCP | `192.168.42.100`
| Dirección final DHCP   | `192.168.42.199`

```console
Do you want to enable the DHCP server on LAN? (y/n)	y	⬅️
Enter the start address of the IPv4 client address range:	192.168.42.100	⬅️
Enter the end address of the IPv4 client address range:		192.168.42.199	⬅️
Disabling IPv6 DHCPD...
```

Seleccionar la opción `n` para dejar la interfaz de configuración web en HTTPS:

```console
Do you want to revert to HTTP as the webConfigurator protocol? (y/n)	n	⬅️
```

Esperar a que se apliquen los cambios.
Se muestra la dirección IP configurada en la interfaz **LAN** y la URL HTTPS de la interfaz de configuración web que escucha en la **red LAN**:

```console
Please wait while the changes are saved to LAN...
 Reloading filter...
 Reloading routing configuration...
 DHCPD...

The IPv4 LAN address has been set to 192.168.42.254/24	✅
You can now access the webConfigurator by opening the following URL in your web browser:
		https://192.168.42.254/	✅

Press <ENTER> to continue.	⬅️
```

#### Configurar interfaz **OPT** 🟠

Seleccionar la opción `2` en la interfaz de configuración de modo texto de pfSense:

```console
VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***

🟢 WAN (wan)       -> em0      -> v4/DHCP4:	10.0.2.15/24		✅
🔵 LAN (lan)       -> em1      -> v4:		192.168.42.254/24	✅
🟠 OPT1 (opt1)     -> em2      ->								❗

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address 🔆    11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option:	2	⬅️
```

La interfaz muestra la asignación actual de interfaces, redes y configuración IP.

- Seleccionar la opción `3` para configurar la interfaz **OPT**:

```console
Available interfaces:

1 - WAN (em0 - dhcp, dhcp6)
2 - LAN (em1 - static)
3 - OPT1 (em2)			🔆

Enter the number of the interface you wish to configure:	3	⬅️
```

Introducir la configuración IP de la interfaz **OPT**:

| Elemento           | Valor
|:------------------:|:----------------:|
| Dirección IP       | `172.16.1.254`
| Máscara de subred  | `255.255.255.0` (`24` bits)
| _Gateway_ IPv4     | (_ninguno_)
| Configuración IPv6 | (_ninguna_)

```console
Enter the new OPT1 IPv4 address.  Press <ENTER> for none:
> 172.16.1.254	⬅️

Subnet masks are entered as bit counts (as in CIDR notation) in pfSense.
e.g. 255.255.255.0 = 24	🔆
     255.255.0.0   = 16
     255.0.0.0     = 8

Enter the new OPT1 IPv4 subnet bit count (1 to 32):
> 24	⬅️

For a WAN, enter the new OPT1 IPv4 upstream gateway address.
For a LAN, press <ENTER> for none:
>	⬅️

Enter the new OPT1 IPv6 address.  Press <ENTER> for none:
>	⬅️
```

Seleccionar la opción `n` para desactivar el servidor DHCP en la red OPT:

```console
Do you want to enable the DHCP server on OPT1? (y/n)	n	⬅️
Disabling IPv4 DHCPD...
Disabling IPv6 DHCPD...
```

Seleccionar la opción `n` para dejar la interfaz de configuración web en HTTPS:

```console
Do you want to revert to HTTP as the webConfigurator protocol? (y/n)	n	⬅️
```

Esperar a que se apliquen los cambios.
Se muestra la dirección IP configurada en la interfaz **OPT**:

```console
Please wait while the changes are saved to OPT1...
 Reloading filter...
 Reloading routing configuration...
 DHCPD...

The IPv4 OPT1 address has been set to 172.16.1.254/24	✅

Press <ENTER> to continue.	⬅️
```

### Habilitar el servicio de SSH

Seleccionar la opción `2` en la interfaz de configuración de modo texto de pfSense para habilitar el servicio de SSH:

```console
VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***

🟢 WAN (wan)       -> em0      -> v4/DHCP4:	10.0.2.15/24		✅
🔵 LAN (lan)       -> em1      -> v4:		192.168.42.254/24	✅
🟠 OPT1 (opt1)     -> em2      -> v4:		172.16.1.254/24		✅

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Enable Secure Shell (sshd)	🔆
 6) Halt system                       15) Restore recent configuration
 7) Ping host                         16) Restart PHP-FPM
 8) Shell

Enter an option:	14	⬅️
```

Confirmar que se habilitará el servicio de SSH y esperar a que se apliquen los cambios:

```console
SSHD is currently disabled.  Would you like to enable? [y/n]?	y	⬅️

Writing configuration... done.

Enabling SSHD...
Reloading firewall rules. done.
```

### Verificar la conectividad a Internet

#### Conectividad a Internet utilizando una dirección IP

Seleccionar la opción `7` para hacer PING a un equipo en Internet:

```console
VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***

🟢 WAN (wan)       -> em0      -> v4/DHCP4:	10.0.2.15/24		✅
🔵 LAN (lan)       -> em1      -> v4:		192.168.42.254/24	✅
🟠 OPT1 (opt1)     -> em2      -> v4:		172.16.1.254/24		✅

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host 🔆                      16) Restart PHP-FPM
 8) Shell

Enter an option: 7	⬅️
```

Introducir una dirección IP homologada como `1.1.1.1` y esperar a que se muestren los resultados:

```console
Enter a host name or IP address: 1.1.1.1

PING 1.1.1.1 (1.1.1.1): 56 data bytes
64 bytes from 1.1.1.1: icmp_seq=0 ttl=63 time=8.899 ms
64 bytes from 1.1.1.1: icmp_seq=1 ttl=63 time=8.538 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=63 time=8.618 ms

--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 8.538/8.685/8.899/0.155 ms

Press ENTER to continue.	⬅️
```

#### Conectividad utilizando un nombre de dominio

Seleccionar la opción `7` para hacer PING a un equipo en Internet:

```console
VirtualBox Virtual Machine - Netgate Device ID: 376bf7d400f115685791

*** Welcome to pfSense 2.6.0-RELEASE (amd64) on pfSense ***

🟢 WAN (wan)       -> em0      -> v4/DHCP4:	10.0.2.15/24		✅
🔵 LAN (lan)       -> em1      -> v4:		192.168.42.254/24	✅
🟠 OPT1 (opt1)     -> em2      -> v4:		172.16.1.254/24		✅

 0) Logout (SSH only)                  9) pfTop
 1) Assign Interfaces                 10) Filter Logs
 2) Set interface(s) IP address       11) Restart webConfigurator
 3) Reset webConfigurator password    12) PHP shell + pfSense tools
 4) Reset to factory defaults         13) Update from console
 5) Reboot system                     14) Disable Secure Shell (sshd)
 6) Halt system                       15) Restore recent configuration
 7) Ping host 🔆                      16) Restart PHP-FPM
 8) Shell

Enter an option:	7	⬅️
```

Introducir un nombre de dominio conocido como `example.com` y esperar a que se muestren los resultados:

```console
Enter a host name or IP address: example.com

PING example.com (93.184.216.34): 56 data bytes
64 bytes from 93.184.216.34: icmp_seq=0 ttl=63 time=14.779 ms
64 bytes from 93.184.216.34: icmp_seq=1 ttl=63 time=17.551 ms
64 bytes from 93.184.216.34: icmp_seq=2 ttl=63 time=16.415 ms

--- example.com ping statistics ---
3 packets transmitted, 3 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 14.779/16.248/17.551/1.137 ms

Press ENTER to continue.	⬅️
```

--------------------------------------------------------------------------------

## Configuraciónde inicial con la interfaz web

Ingresar a la interfaz web de pfSense conectandose a la dirección IP de interfaz **LAN** mediante HTTPS:

- <https://192.168.42.254:443/>

| Interfaz web de pfSense
|:-----------------------:|
| ![](img/pfsense/pfsense-024-webui-https-cert.png)

Iniciar sesión con las credenciales predeterminadas

| Elemento   | Valor
|:----------:|:-----:|
| Usuario    | `admin`
| Contraseña | `pfsense`

| Inicio de sesión de pfSense
|:---------------------------:|
| ![](img/pfsense/pfsense-025-webui-https-login.png)

### Asistente de inicio

Se presenta el mensaje de inicio

!!! danger
    - Establecer la contraseña de administrador de pfSense como `Redes-2023-2`

| Configuración inicial de pfSense
|:--------------------------------:|
| ![](img/pfsense/pfsense-026-webui-https-0-welcome.png)
| ![](img/pfsense/pfsense-027-webui-https-1-netgate.png)
| ![](img/pfsense/pfsense-028-webui-https-2-general-info.png)
| ![](img/pfsense/pfsense-029-webui-https-3-time-settings.png)
| ![](img/pfsense/pfsense-030-webui-https-4-wan-config.png)
| ![](img/pfsense/pfsense-030a-webui-https-4-wan-config-cont.png)
| ![](img/pfsense/pfsense-031-webui-https-5-lan-config.png)
| ![](img/pfsense/pfsense-032-webui-https-6-admin-password.png)
| ![](img/pfsense/pfsense-033-webui-https-7-reload.png)
| ![](img/pfsense/pfsense-034-webui-https-8-reload-progress.png)
| ![](img/pfsense/pfsense-035-webui-https-9-pfsense-configured.png)


### Verificar actualizaciones

| Verificación de actualizaciones
|:-------------------------------:|
| ![](img/pfsense/pfsense-036-webui-check-updates.png)

### Dashboard principal

| Dashboard de pfSense
|:--------------------:|
| ![](img/pfsense/pfsense-037-webui-https-10-pfsense-thankyou.png)
| ![](img/pfsense/pfsense-038-webui-dashboard.png)

--------------------------------------------------------------------------------

## Nombre de host y contraseña

- Establecer la contraseña de administrador de pfSense como `Redes-2023-2`
- Establecer el nombre de host como `pfSense-ABCD-EFGH-IJKL-MNOP-QRST.local`
    - Donde `ABCD-EFGH-IJKL-MNOP-QRST` representa el nombre del equipo que debió anotarse previamente en la lista del grupo

--------------------------------------------------------------------------------

## Guardar configuraciíón inicial

Guardar la configuración en el archivo `pfsense-inicial.xml` e incluirlo en la carpeta `files/pfSense/`

| Exportar configuración a archivo XML
|:------------------------------------:|
| ![](img/pfsense/pfsense-039-webui-diagnostics-backup.png)
| ![](img/pfsense/pfsense-040-webui-backup-settings.png)
| ![](img/pfsense/pfsense-041-webui-backup-file.png)

<!-- ---------------------------------------------------------------------- -->

<!--

- Configurar los clientes disponibles CentOS, Debian, etc., a la interfaz <span class="blue">**LAN**</span> para obtener una dirección IP mediante el DHCP:

| Servidor CentOS (<span class="orange">**Red DMZ**</span>)
|:----:|
| ![](img/pfsense/14.1.png)

| Cliente Debian (<span class="blue">**Red LAN**</span>)
|:----:|
| ![](img/pfsense/14.png)

| Cliente Alpine (<span class="blue">**Red LAN**</span>)
|:----:|
| ![](img/pfsense/16.png)

-->

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando tengas pfSense instalado

--------------------------------------------------------------------------------

|             ⇦             |         ⇧          |               ⇨               |
|:--------------------------|:------------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba][arriba] | [Página siguiente][siguiente]

[anterior]: ../virtualbox-networks
[arriba]: ../#procedimiento
[siguiente]: ../pfsense-configure
