---
title: Práctica 6 - Configuración de un firewall de red con pfSense
authors:
- Dante Erik Santiago Rodríguez Pérez
- Andrés Leonardo Hernández Bermúdez
---

# Práctica 6: Configuración de un firewall de red con pfSense

--------------------------------------------------------------------------------

## Objetivo

El alumno realizará la instalación y configuración de una máquina virtual pfSense con los servicios de red NAT, DHCP, DMZ y Port Forwarding para permitir que otras máquinas tengan salida a Internet, así como que otros dispositivos de Internet tengan acceso a servicios en la DMZ.

## Elementos de apoyo

Recursos de instalación

- [Imagen ISO de pfSense 💽][pfsense-iso]
- [Imagen ISO de Alpine Linux 💽][alpine-linux-iso]
- [Compactar y exportar una máquina virtual de VirtualBox 📝][virtualbox-compact-export-vm]

Videos de teoría

- [Protocolo ARP 📼][video-protocolo-arp]
- [Protocolo DHCP 📼][video-protocolo-dhcp]
- [Protocolo DNS 📼][video-protocolo-dns]

Videos de configuración

- [Configuración manual de direcciones IP en GNU/Linux 📼][video-ip-manual]
- [Configuración persistente de direcciones IP en GNU/Linux 📼][video-ip-persistente]

???+ Documentación de pfSense
    - [Guía de instalación de pfSense 📚][pfsense-docs-install]
    - [Guía de configuración de pfSense 📚][pfsense-docs-config]
    - [Guía de configuración de interfaces de pfSense 📚][pfsense-docs-interfaces]
    - [Guía de configuración de _firewall_ de pfSense 📚][pfsense-docs-firewall]
    - [Guía de configuración de NAT de pfSense 📚][pfsense-docs-nat]
    - [Guía de configuración de ruteo de pfSense 📚][pfsense-docs-routing]
    - [Guía de configuración de DHCP de pfSense 📚][pfsense-docs-dhcp]
    - [Guía de configuración de DNS de pfSense 📚][pfsense-docs-dns]
    - [Guía de diagnóstico de pfSense 📚][pfsense-docs-diagnostics]
    - [Guía de resolución de problemas de pfSense 📚][pfsense-docs-troubleshooting]
    - [Guía de respaldo de pfSense 📚][pfsense-docs-backup]

## Restricciones

- La fecha límite de entrega es el **lunes 17 de abril de 2023** a las 23:59 horas `¹`
- Esta actividad debe ser entregada **en equipo** de acuerdo al [flujo de trabajo para la entrega de tareas y prácticas][flujo-de-trabajo]
- Crear una nueva rama llamada `practica-6`
- Utilizar la carpeta `docs/practicas/practica-6/Equipo-ABCD-EFGH-IJKL-MNOP-QRST` para entregar la práctica
    - Donde `Equipo-ABCD-EFGH-IJKL-MNOP-QRST` representa el nombre del equipo que debió anotarse previamente en la [lista del grupo][lista-redes]
- Crear un _merge request_ en el [repositorio de tareas][repo-tareas] para entregar la actividad

!!! warning
    - `¹`: Se ajustó la fecha de entrega para compensar los días en los que la Facultad de Ciencias estuvo en paro activo 🟥⬛

--------------------------------------------------------------------------------

## Introducción

PfSense versión Community Edition es una plataforma de la compañía Netgate, desarrollada en el sistema FreeBSD UNIX con licencia de código libre, que proporciona servicios como _firewall_, router, red privada virtual (VPN por sus siglas en inglés), sistema de prevención/detección de Intrusos (IPS/IDS) y DNS entre otros.

## Procedimiento

Se presentan los pasos para elaborar la configuración de un NAT, _forwarder_ de DNS, DHCP y red DMZ utilizando la plataforma pfSense con base en la topología de red que se muestra a continuación:

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-----------------------------:|
| ![](img/diagrama_red.png)
| 🟠 <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵 <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢 <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet

!!! warning
    - Crear un _snapshot_ de la máquina virtual **pfSense** y exportar la configuración inicial a XML <u>antes</u> de realizar la configuración de los servicios de red

<a id="pasos" name="pasos"></a>

Seguir los pasos listados en cada página para configurar cada componente de la práctica:

- [Configuración de redes en VirtualBox](./virtualbox-networks)

- [Instalación del **_firewall_** pfSense](./pfsense-install)

- [Configuración del **_firewall_** pfSense](./pfsense-configure)

- [Instalación del **cliente** Alpine](./alpine-install)

- [Configuración de red del **cliente** Debian](./debian-network)

- [Configuración de red del **servidor** CentOS](./centos-network)

- [Configuración de servicios de red en el **servidor** CentOS](./centos-services)

- [Configuración del servicio DHCP en pfSense](./pfsense-dhcp)

- [Pruebas de conectividad](./network-test)

--------------------------------------------------------------------------------

## Entregables

- Archivo `README.md`
    - Explicación de la topología de red utilizada
    - Procedimiento de configuración de NAT, Alias, Port Forwarding, servicio DHCP y servicio DNS
    - <span class="red">Contraseña `Redes-2023-2` para pfSense</span>
    - <span class="red">El nombre de host debe ser `pfSense-ABCD-EFGH-IJKL-MNOP-QRST.local`</span>
    - Procedimiento para reservar una dirección IP en el servidor DHCP
    - Explicación de las bitácoras generadas
    - Explicación de las reglas configuradas
    - Visualizar la configuración de pfSense al conectarse via SSH:
        - Reglas PF (`pfctl -sr`)
        - Tablas (`pfctl -s Tables`)
        - Estados de NAT (`pfctl -ss`)
        - Reglas de NAT (`pfctl -s NAT`)
    - [Conclusiones](#conclusiones) sobre las capturas de tráfico de red

- Carpeta `files`
    - Capturas de tráfico en formato `PCAP` [para los servicios solicitados][network-test]
    - pfSense
        - Archivos de configuración XML de pfSense (**inicial** y **final**)
        - Estados del pfsense donde se indiquen las conexiones de NAT (**EN TEXTO**)
        - Bitácoras de pfSense (**EN TEXTO**)
    - Para los equipos en la red WAN (Alpine), LAN (Debian) y DMZ (CentOS)
        - Tabla ARP
        - Tabla de rutas
        - Salida de los comandos de configuración de red
        - Salida de los comandos de resolución DNS
        - Pruebas de conectividad con PING: red local, hacia otras redes y hacia Internet
    - Pruebas de conectividad del cliente WAN (alpine) al servidor DMZ (CentOS) utilizando la redirección de puertos

--------------------------------------------------------------------------------

### Conclusiones

- ¿Qué tipo de política de _firewall_ se utiliza en la práctica: permisiva o restrictiva?, ¿cuál se considera mejor?. Justifica tu respuesta.

## Extra

Elaboren un video donde expliquen la topología de red utilizada, la configuración del _firewall_, los servicios de red instalados en CentOS y las pruebas de conectividad entre los equipos de todas las redes y hacia Internet.

- Subir el video a YouTube
- Agregar la referencia de este video al archivo `README.md`

```text
- [Video de la topología de red utilizada 📼](https://youtu.be/0123456789ABCDEF)
```

--------------------------------------------------------------------------------

[network-test]: ./network-test

[flujo-de-trabajo]: https://redes-ciencias-unam.gitlab.io/workflow/
[repo-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests

[lista-redes]: https://tinyurl.com/Lista-Redes-2023-2

[video-protocolo-arp]: https://www.youtube.com/watch?v=bqNLVQDqmLk
[video-protocolo-dhcp]: https://www.youtube.com/watch?v=6l4WQJfD7o0
[video-protocolo-dns]: https://www.youtube.com/watch?v=r4PntflJs9E

[video-ip-manual]: https://www.youtube.com/watch?v=H74s4_oJNYY
[video-ip-persistente]: https://www.youtube.com/watch?v=UErZ4i9XmLM

[alpine-linux-iso]: https://dl-cdn.alpinelinux.org/alpine/v3.16/releases/x86_64/alpine-virt-3.16.2-x86_64.iso

[pfsense-iso]: https://atxfiles.netgate.com/mirror/downloads/pfSense-CE-2.6.0-RELEASE-amd64.iso.gz

[pfsense-docs-start]: https://www.pfsense.org/getting-started/
[pfsense-docs-install]: https://docs.netgate.com/pfsense/en/latest/install/index.html
[pfsense-docs-config]: https://docs.netgate.com/pfsense/en/latest/config/index.html
[pfsense-docs-interfaces]: https://docs.netgate.com/pfsense/en/latest/interfaces/index.html
[pfsense-docs-firewall]: https://docs.netgate.com/pfsense/en/latest/firewall/index.html
[pfsense-docs-nat]: https://docs.netgate.com/pfsense/en/latest/nat/index.html
[pfsense-docs-routing]: https://docs.netgate.com/pfsense/en/latest/routing/index.html
[pfsense-docs-dhcp]: https://docs.netgate.com/pfsense/en/latest/services/dhcp/index.html
[pfsense-docs-dns]: https://docs.netgate.com/pfsense/en/latest/services/dns/index.html
[pfsense-docs-diagnostics]: https://docs.netgate.com/pfsense/en/latest/diagnostics/index.html
[pfsense-docs-troubleshooting]: https://docs.netgate.com/pfsense/en/latest/troubleshooting/index.html
[pfsense-docs-backup]: https://docs.netgate.com/pfsense/en/latest/backup/index.html

[virtualbox-compact-export-vm]: ../../temas/virtualbox/compact-export-vm/
