---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Uso de pre-commit
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Uso de `pre-commit`

## Configuración de `pre-commit`

Este repositorio contiene [una configuración de `pre-commit`][pre-commit-config] que ayuda a detectar muchos de los problemas comúnes cuando se envían archivos a un repositorio de `git`.

- Enviar archivos muy grandes
- Enviar archivos Markdown con ligas rotas
- Enviar archivos de texto sin retorno de línea al final del archivo
- Enviar archivos con espacios en blanco al final de la línea
- Enviar archivos al repositorio con una mezcla de caracteres de fin de línea:
    - `\n` (Linux/Unix/macOS)
    - `\n\r` (Windows)

El _script_ `pre-commit` se instala como un _hook_ de `git` para el evento _commit_ y se invoca cada vez que se ejecuta `git commit`.
El programa debe de ejecutarse de manera exitosa para que se pueda completar la operación de _commit_ y que se puedan versionar los archivos en el repositorio.

!!! warning
    - Si la ejecución de `pre-commit` falla, entonces debes de verificar el mensaje de error que te dio
    - Los mensajes de error que regresa `pre-commit` explican cuál fue la condición y qué archivos fueron modificados

Instala los _hooks_ de `git` para que se pueda ejecutar `pre-commit`

```console
$ pwd
/ruta/hacia/tareas-redes

$ pre-commit install
pre-commit installed at .git/hooks/pre-commit
```

!!! note
    Verifica que estas dentro del directorio donde clonaste el repositorio, de lo contrario no se podrán instalar los _hooks_

Ejecuta `pre-commit` manualmente para inicializar el entorno y verificar que los _hooks_ se ejecuten de manera correcta.

- **Todos los _hooks_** deben de mostrar un estado <span class="green">Passed</span> o <span class="blue">Skipped</span>
- **Ningún _hook_** se debe mostrar como <span class="red">Failed</span>

```console
$ pre-commit run --all-files
[INFO] Initializing environment for https://github.com/pre-commit/pre-commit-hooks.
[INFO] Installing environment for https://github.com/pre-commit/pre-commit-hooks.
[INFO] Once installed this environment will be reused.
[INFO] This may take a few minutes...
	...
check for added large files..............................................Passed
	...
trim trailing whitespace.................................................Passed

$ echo $?
0
```

!!! warning
    Verifica que el código de salida del comando sea `0` (cero) utilizando `echo $?` inmediatamente después de ejecutar `pre-commit` manualmente


### Ejecución de `pre-commit`

El programa `pre-commit` se ejecuta cada vez que se llama `git commit`:

```console
$ git add ARCHIVO
$ git commit
check for added large files..............................................Passed
check for case conflicts.................................................Passed
check that executables have shebangs.................(no files to check)Skipped
check that scripts with shebangs are executable..........................Passed
check for merge conflicts................................................Passed
check for broken symlinks............................(no files to check)Skipped
check vcs permalinks.....................................................Passed
detect destroyed symlinks................................................Passed
detect aws credentials...................................................Passed
detect private key.......................................................Passed
fix end of files.........................................................Passed
file contents sorter.................................(no files to check)Skipped
fix utf-8 byte order marker..............................................Passed
forbid new submodules................................(no files to check)Skipped
mixed line ending........................................................Passed
trim trailing whitespace.................................................Passed
[main a1b2c3d4] example commit
 1 file changed, 7 insertions(+), 7 deletions(-)
```

!!! note
    - Realizarás estos pasos en la siguiente sección

!!! warning
    - Pregunta en el chat de Telegram si tienes alguna duda

!!! danger
    - Normalmente la solución a los errores de ejecución de los _hooks_ `pre-commit` es sencilla y los mensajes de error son bastante descriptivos



--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando tengas pre-commit instalado y configurado en tu repositorio local

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../clonar-fork
[arriba]: ../../workflow
[siguiente]: ../agregar-contenido

[pre-commit-config]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/blob/main/.pre-commit-config.yaml
