---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Características adicionales
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Características adicionales

--------------------------------------------------------------------------------

## PHP

### Versiones de PHP

Normalmente Debian tiene una sola versión de PHP.
Si se desea instalar una versión diferente es posible compilarla desde código fuente, pero este proceso puede ser bastante tedioso y propenso a errores.

Existe un repositorio llamado **Sury** que tiene más versiones de PHP que se pueden instalar en Debian.

| Sistema operativo | Versión de PHP predeterminada | Versiones de PHP disponibles en **Sury**
|:-----------------:|:-----------------------------:|:----------------------------------------:|
| Debian 11         | 7.4                           | 5.6 , 7.0 , 7.1 , 7.2 , 7.3 , 7.4 , 8.0 , 8.1 , 8.2
| Debian 10         | 7.3                           | 5.6 , 7.0 , 7.1 , 7.2 , 7.3 , 7.4 , 8.0 , 8.1 , 8.2

Si se pide instalar "la versión de PHP incluida con Debian", entonces no es necesario instalar el repositorio **Sury**.

El repositorio **Sury** se puede instalar en una máquina Debian siguiendo las instruciones del sitio web:

- <https://deb.sury.org/>
- <https://packages.sury.org/php/>
- <https://github.com/oerdnj/deb.sury.org/wiki/Frequently-Asked-Questions#how-to-enable-the-debsuryorg-repository>

### Configuración de PHP

Existen dos tipos de configuración de PHP:

- <https://cwiki.apache.org/confluence/display/HTTPD/php>
- <https://www.php.net/manual/en/install.unix.debian.php>

#### `mod_php`

En este tipo de configuración el motor del intérprete de PHP se carga como módulo de Apache HTTPD.

La comunicación de Apache HTTPD con PHP es **directa** porque el código de `mod_php` se carga en la imagen ejecutable de Apache.

- <https://www.php.net/manual/en/install.unix.apache2.php>
- <https://www.php.net/manual/en/book.apache.php>
- <https://www.php.net/manual/en/security.apache.php>

#### PHP-FPM

En este método de configuración el motoro de PHP se ejecuta como un demonio externo que utiliza la tecnología FastCGI.

La comunicación entre PHP-FPM y Apache HTTPD se realiza a través de un _socket de dominio UNIX_.
Es necesario que el usuario asociado a los demonios de Apache HTTPD y PHP-FPM pueda leer y escribir en el archivo de _socket_ para que la comunicación sea exitosa.
Muchas veces esto se logra agregando al usuario de Apache HTTPD al grupo de PHP-FPM y viceversa.

<!-- - <https://php-fpm.org/> -->
- <https://www.php.net/manual/en/install.fpm.php>

### Caché de PHP

Existen varios plugins que aplican diversas técnicas para acelerar el motor de PHP.

- <https://en.wikipedia.org/wiki/List_of_PHP_accelerators>

#### APC / APCu

APC permite que se guarden los _opcodes_ en caché para acelerar el procesamiento del intérprete de PHP.

- <https://pecl.php.net/package/APCu>
- <https://www.php.net/manual/en/book.apcu.php>
- <https://www.php.net/manual/en/apcu.installation.php>

#### OpCache

OPcache permite guardar el _bytecode_ en memoria compartida para acelerar el procesamiento del intérprete de PHP.

- <https://pecl.php.net/package/ZendOpcache>
- <https://www.php.net/manual/en/book.opcache.php>
- <https://www.php.net/manual/en/opcache.installation.php>

--------------------------------------------------------------------------------

## MySQL y MariaDB

Instalar el paquete `mariadb-server` si se pidió "la versión que trae Debian de manera predeterminada"

- https://packages.debian.org/buster/mariadb-server

Repositorio de MySQL community

- <https://dev.mysql.com/downloads/>
- <https://dev.mysql.com/downloads/repo/apt/>

Repositorio de MariaDB.org

- <https://mariadb.org/download/?t=repo-config&d=Debian+10+%22buster%22&v=10.11&r_m=rackspace>

### PHPMyAdmin

PHPMyAdmin es una interfaz de administración web para servidores MySQL y/o MariaDB

- <https://www.phpmyadmin.net/>
- <https://docs.phpmyadmin.net/>

!!! warning
    - Para este proyecto se tendrá que configurar la autenticación `htpasswd` en TODO el VirtualHost que atiende el dominio `phpmyadmin.example.com`
    - El sitio tiene que responder ÚNICAMENTE por HTTPS, configurar la redirección de HTTP a HTTPS

### Replicación MySQL

- <https://dev.mysql.com/doc/refman/5.7/en/replication.html>
- <https://dev.mysql.com/doc/mysql-replication-excerpt/5.7/en/replication.html>
- <https://dev.mysql.com/doc/refman/8.0/en/replication.html>
- <https://dev.mysql.com/doc/mysql-replication-excerpt/8.0/en/replication.html>

--------------------------------------------------------------------------------

## Panel de control web

### Webmin

Webmin es un panel de control web que se puede instalar en Linux para realizar las configuraciones del servidor.
El panel de control necesita que se abra un puerto adicional en el servidor para poder acceder a la sección administrativa.

- <https://webmin.com/>

### Virtualmin

Virtualmin es una extension de Webmin que permite la administración del servidor web

- <https://webmin.com/virtualmin/>
- <https://www.virtualmin.com/>

--------------------------------------------------------------------------------

## Autenticación en Apache HTTPD

Existen dos tipos de autenticación para Apache HTTPD: `htpasswd` y `htdigest`

- <https://httpd.apache.org/docs/2.4/howto/auth.html>

--------------------------------------------------------------------------------

## Características de WordPress

- Multi-sitio
    - <https://wordpress.org/documentation/article/create-a-network/>
    - <https://wordpress.org/documentation/article/multisite-network-administration/>
- [Aplicación móvil](https://wordpress.org/mobile/)
    - [Android](http://play.google.com/store/apps/details?id=org.wordpress.android)
    - [iOS](https://itunes.apple.com/app/apple-store/id335703880?pt=299112&ct=wordpress.org&mt=8)

--------------------------------------------------------------------------------

## Plugins de WordPress

- WP Offload Media Lite (S3)
- Plugin para Memcache
- Plugin para Redis
- WP Super Cache
- [`wp2static`](https://wp2static.com/) <!-- https://github.com/WP2Static/wp2static -->
- Simply Static
- CAPTCHA
- 2FA

--------------------------------------------------------------------------------

### Herramientas externas

- `wp-cli`
    - <https://wp-cli.org/>
    - <https://make.wordpress.org/cli/>
    - <https://github.com/wp-cli/wp-cli>
    - <https://developer.wordpress.org/cli/commands/>

--------------------------------------------------------------------------------

## Envío de bitácoras a `syslog`

Configurar el envío de bitácoras de Apache HTTPD a `syslog`

- <https://httpd.apache.org/docs/2.4/logs.html>

Después configurar la redirección de TODAS las bitácoras de `syslog` hacia un servicio externo:

- Loggly
    - <https://documentation.solarwinds.com/en/success_center/loggly/content/admin/rsyslog-manual-configuration.htm>
    - <https://www.loggly.com/use-cases/rsyslog-manual-configuration-and-troubleshooting/>
- DataDog
    - <https://docs.datadoghq.com/integrations/rsyslog/?tab=ubuntuanddebian>
- LogTail
    - <https://betterstack.com/docs/logs/rsyslog-setup/>

--------------------------------------------------------------------------------

## APM

Configurar el agente de [NewRelic](https://newrelic.com/) para que haga introspección y analice el rendimiento de PHP.

- <https://docs.newrelic.com/docs/apm/agents/php-agent/getting-started/introduction-new-relic-php/>
- <https://github.com/newrelic/newrelic-php-agent>

--------------------------------------------------------------------------------

## Monitoreo externo

Habilitar el monitoreo con alguna herramienta externa para que envíe correos cuando el servicio esté abajo.

- Pingdom
- Down Detector

Configurar esto únicamente para el VirtualHost de WordPress.

--------------------------------------------------------------------------------

## CloudFlare

Habilitar la protección de CloudFlare para TODOS los sitios hospedados en el dominio `example.com`.

Es necesario configurar los _nameservers_ de CloudFlare en el panel de control del _registrar_ para establecer CloudFlare como el servidor **autoritativo** de DNS para el dominio `example.com`.

- <https://wordpress.com/plugins/cloudflare>
- <https://www.cloudflare.com/pg-lp/cloudflare-for-wordpress/>
- <https://developers.cloudflare.com/support/third-party-software/content-management-system-cms/using-the-cloudflare-plugin-with-wordpress/>
- <https://developers.cloudflare.com/support/third-party-software/content-management-system-cms/wordpress.com-and-cloudflare/>

--------------------------------------------------------------------------------

## Configuración de respaldos

!!! warning
    - Esta sección aplica para el equipo que le tocó hacer el respaldo

Automatizar el respaldo de la base de datos en "esquema" y "contenido"

- Script y configuración de la tarea programada para respaldar la base de datos <u>cada 12 horas</u> ([medio día y media noche][cron])

- Definir un **prefijo** para que indique la fecha (`YYYY-MM-DD-HH-MM`) o _marca de tiempo_ UNIX epoch (ver salida de `date '+%s'`)

- Definir si los archivos respaldo se guardan el prefijo en el nombre o si se crearán carpetas con el prefijo para contener los archivos de cada respaldo

- `recursos.tar.gz`: Crear un archivo TAR comprimido con `gzip` que tenga los archivos que hayan subido los usuarios de la aplicación que hayan elegido, cada aplicación tiene una carpeta que no forma parte del código donde guarda estos archivos

- `esquema.sql.gz`: Generar un archivo SQL comprimido con `gzip` con el **esquema** de la base de datos (definición de tablas)

- `datos.sql.gz`: Generar otro archivo SQL con los **datos** de todas las tablas de la base de datos

- Subir los archivos resultantes a Google Drive utilizando [`rclone`][rclone-gdrive].

    - Tener cuidado con los [permisos que se dan a la aplicación][google-drive-permissions] y seleccionar [el modo de acceso `drive.file`][rclone-gdrive-scopes] para dar acceso **ÚNICAMENTE a los archivos y directorios que `rclone` haya creado**.

    - Establecer un folder como la [raíz que puede ver `rclone`][rclone-gdrive-root-folder] para limitar aún mas el acceso a Google Drive.

[google-drive-permissions]: https://developers.google.com/drive/api/guides/api-specific-auth
[rclone-gdrive-scopes]: https://rclone.org/drive/#scopes
[rclone-gdrive-root-folder]: https://rclone.org/drive/#root-folder-id

```
$ rclone config

	...

Scope that rclone should use when requesting access from drive.
Choose a number from below, or type in your own value
 1 / Full access all files, excluding Application Data Folder.
   \ "drive"
 2 / Read-only access to file metadata and file contents.
   \ "drive.readonly"
   / Access to files created by rclone only.	⬅	⬅	⬅	⬅	⬅	⬅	⬅
 3 | These are visible in the drive website.	⬅	⬅	⬅	⬅	⬅	⬅	⬅
   | File authorization is revoked when the user deauthorizes the app.	⬅	⬅
   \ "drive.file"	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅
   / Allows read and write access to the Application Data folder.
 4 | This is not visible in the drive website.
   \ "drive.appfolder"
   / Allows read-only access to file metadata but
 5 | does not allow any access to read or download file content.
   \ "drive.metadata.readonly"

scope> 3	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅	⬅

	...

ID of the root folder - leave blank normally.  Fill in to access "Computers" folders. (see docs).
root_folder_id> xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx	⬅	⬅	⬅	⬅	⬅	⬅

	...
```

--------------------------------------------------------------------------------

## Verificación del caché en RAM

!!! warning
    - Esta sección aplica para el equipo que le tocó implementar el caché en RAM

Es posible verificar el estado de los servicios MemCache y Redis utilizando los _scripts_ de _shell_:

### MemCache

???+ details "MemCache"
    Puedes probar el servicio de MemCache utilizando [el _script_ de _shell_ `test-memcache.sh`][test-memcache]

    <pre><code>
    root@example:~# chmod -c +x test-memcache.sh
    root@example:~# ./test-memcache.sh
    	...
    </code></pre>

### Redis

???+ details "Redis"
    Puedes probar el servicio de Redis utilizando [el _script_ de _shell_ `test-redis.sh`][test-redis]

    <pre><code>
    root@example:~# chmod -c +x test-redis.sh
    root@example:~# ./test-redis.sh
    	...
    </code></pre>

--------------------------------------------------------------------------------

[apache-userdir]: https://httpd.apache.org/docs/2.4/howto/public_html.html
[apache-redirect-https]: https://cwiki.apache.org/confluence/plugins/servlet/mobile?contentId=115522478#content/view/115522444
[apache-rewrite-https]: https://cwiki.apache.org/confluence/plugins/servlet/mobile?contentId=115522478#content/view/115522478
[api-telegram]: https://core.telegram.org/
[api-twitter]: https://developer.twitter.com/en/docs/twitter-api
[aws-ec2-ami]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/creating-an-ami-ebs.html
[aws-ec2-ami-share]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/sharingamis-explicit.html
[hsts]: https://https.cio.gov/hsts/
[x-robots-tag]: https://developers.google.com/search/docs/advanced/robots/robots_meta_tag
[apache-auth-digest]: https://httpd.apache.org/docs/2.4/howto/auth.html
[cron]: https://opensource.com/article/17/11/how-use-cron-linux
[lineamientos-entrega]: https://redes-ciencias-unam.gitlab.io/workflow/

[flujo-de-trabajo]: https://redes-ciencias-unam.gitlab.io/workflow/
[repo-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests

[proyecto-web]: https://tinyurl.com/Redes-2023-2-Proyecto-Web
[lista-redes]: https://tinyurl.com/Lista-Redes-2023-2
[carpeta-drive]: https://tinyurl.com/ProyectoWeb-Redes-2023-2

[playlist-https]: https://www.youtube.com/playlist?list=PLN1TFzSBXi3QGCMqARFoO1ePBX1P38erB
[video-protocolo-dns]: https://www.youtube.com/watch?v=r4PntflJs9E
[video-configuracion-ssh]: https://youtu.be/Hnu7BHBDcoM&t=1390
[video-configuracion-apache-debian]: https://youtu.be/XbQ_dBuERdM
[video-directivas-apache]: https://youtu.be/3JkQs3KcjxQ
[video-virtualhosts-apache-etc-hosts]: https://youtu.be/ZnqSNXIr-h4
[video-virtualhosts-apache-registros-dns]: https://youtu.be/JYo5rc4mhf0
[video-certificados-ssl-x509]: https://youtu.be/rXqkJi_FTuQ
[video-certificados-ssl-virtualhost-https-apache]: https://youtu.be/66dOHHD6L5I
[video-letsencrypt-certbot]: https://youtu.be/kpiChLT5JPs

[youtube-video-aws-summit-2020-cdmx-seguridad]: https://youtu.be/d3jnbtaLb24&list=PL2yQDdvlhXf_h40vMoMoh2SBa05geKLDq&index=10&
[youtube-video-aws-reinvent-2022-security]: https://www.youtube.com/watch?v=uFrj0jHN848&list=PL2yQDdvlhXf8bvQJuSP1DQ8vu75jdttlM&index=1&

[apache-docs]: https://httpd.apache.org/docs/2.4/
[apache-docs-config-sections]: https://httpd.apache.org/docs/2.4/sections.html
[apache-docs-security]: https://httpd.apache.org/docs/2.4/misc/security_tips.html
[apache-docs-server-wide]: https://httpd.apache.org/docs/2.4/server-wide.html
[apache-docs-url-rewrite]: https://httpd.apache.org/docs/2.4/rewrite/
[apache-docs-virtualhost]: https://httpd.apache.org/docs/2.4/vhosts/
[apache-docs-ssl]: https://httpd.apache.org/docs/2.4/ssl/
[apache-docs-htaccess]: https://httpd.apache.org/docs/2.4/howto/htaccess.html

[certbot-instructions-debian-10-buster]: https://certbot.eff.org/instructions?ws=apache&os=debianbuster

[rclone-gdrive]: https://rclone.org/drive/
[rclone-authorize]: https://rclone.org/commands/rclone_authorize/

[test-memcache]: files/test-memcache.sh
[test-redis]: files/test-redis.sh
