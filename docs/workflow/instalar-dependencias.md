---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Instalar dependencias de desarrollo
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Instalar dependencias de desarrollo

El contenido de este repositorio se convierte de archivos fuente escritos en Markdown hacia un sitio estático HTML que está hospedado en GitLab pages.
Esto es posible gracias al _software_ `mkdocs` que se puede ejecutar en el _pipeline_ de CI/CD de GitLab o de manera local utilizando el `Makefile` que viene incluido en el sitio.

El repositorio también tiene un archivo de configuración del _software_ `pre-commit` que nos ayuda a evitar los errores comunes cuando se envían archivos al repositorio de `git`.

## Instalación `git`

Necesitas tener la herramienta `git` instalada para hacer uso de los repositorios.

```console
# apt install git
```

!!! note
    - Si utilizas **macOS**, puedes instalar mediante [`brew install git`][brew-git]
    - Si utilizas Windows, puedes instalar via [`choco install git`][choco-git] o [`winget install git`][winget]

Después de instalar `git` en el equipo, se debe realizar la configuración de la herramienta utilizando el comando `git config` [como está descrito en la documentación][git-customize].

### Configuración de la identidad de usuario en `git`

El programa `git` necesita conocer el nombre y dirección de correo de la persona que crea los _commits_.

```console
$ git config --global user.name  "Andrés Hernández"
$ git config --global user.email "andres.hernandez@comunidad.unam.mx"
```

Verificar que la identidad del usuario se haya guardado en el archivo `~/.gitconfig`.

```console
$ cat ~/.gitconfig
[user]
	name = Andres Hernandez
	email = andres.hernandez@comunidad.unam.mx
```

### Manejo de espacios en blanco en `git`

```
$ git config --global core.whitespace blank-at-eol,blank-at-eof,space-before-tab
```

### Configuración de caracteres de fin de línea en `git`

GIT puede corregir los caracteres de fin de línea que utiliza Windows (`CR-LF`) para que sean compatibles con los de otros sistemas operativos más avanzados como GNU/Linux, macOS y BSD (`LF`).

#### GNU/Linux, macOS y BSD

Si utilizas alguno de estos sistemas operativos, entonces basta con activar la opción para convertir `CR-LF` en `LF` en los archivos de entrada.

```
$ git config --global core.autocrlf input
```

#### Windows

Cuando utilizas Windows, es necesario activar la conversión de `CR-LF` en `LF` en todos los casos.

```
$ git config --global core.autocrlf true
```


## Instalación de `make`

Es necesario instalar `make` para construir el sitio de manera local.

```console
# apt install build-essential make tree
```

## Instalación de dependencias de Python

Los programas `pre-commit` y `mkdocs` están escritos en Python y son necesarias las dependencias de desarrollo para el manejador de paquetes de Python (`pip3`) para poder instalarlos en el sistema.

```console
# apt install python3-dev python3-pip
```

Verifica que tengas `pip3` instalado y actualizado.

```console
$ which pip3
/usr/bin/pip3

$ pip3 install --user --upgrade pip
Requirement already satisfied: pip in /usr/lib/python3/dist-packages (20.3.4)
Collecting pip
  Downloading pip-23.0-py3-none-any.whl (2.1 MB)
Installing collected packages: pip
  WARNING: The scripts pip, pip3 and pip3.9 are installed in '/home/tonejito/.local/bin' which is not on PATH.
  Consider adding this directory to PATH or, if you prefer to suppress this warning, use --no-warn-script-location.
Successfully installed pip-23.0
```

!!! danger
    - Asegúrate de ejecutar `pip3` y no `pip`
    - **Nunca debes ejecutar `pip3` con `sudo`**

Agrega una ruta para alterar la variable de entorno `PATH` para tu usuario.

```
$ echo 'export PATH=${HOME}/.local/bin:${PATH}' >> ~/.bashrc
```

!!! note
    - Revisa que sólo tengas **una** vez la línea de `export PATH` en tu archivo `~/.bashrc`

!!! warning
    Debes de realizar alguna de las siguientes acciones después de modificar el archivo `~/.bashrc`:

    - Ejecuta `source ~/.bashrc` en la terminal actual para incluir los cambios
    - Abre otra ventana de la terminal para que se lea el nuevo contenido del archivo `~/.bashrc`
    - Cierra y abre tu sesión para que reconozca los cambios
    - Reinicia tu máquina

## Instalación de `pre-commit`

Instala `pre-commit` en tu perfil de usuario.

```console
$ pip3 install --user pre-commit
Collecting pre-commit
	...
Installing collected packages: ... pre-commit
	...
Successfully installed ... pre-commit-2.20.0 ...
```

!!! danger
    - Asegúrate de ejecutar `pip3` y no `pip`
    - **Nunca debes ejecutar `pip3` con `sudo`**

Verifica que el _shell_ reconozca la ubicación de `pre-commit`.

```console
$ which pre-commit
/home/tonejito/.local/bin/pre-commit
```

## Instalación de `mkdocs`

Descarga el archivo `requirements.txt` de la página del curso.

```console
$ wget -c -nv 'https://Redes-Ciencias-UNAM.gitlab.io/requirements.txt'
```

Se instala `mkdocs` para ejecutar el equivalente del _pipeline_ de CI/CD de manera local.

```console
$ pip3 install --user --requirement requirements.txt
Collecting mkdocs>=1.1.2
	...
Installing collected packages: ... mkdocs ...
	...
Successfully installed ... mkdocs-1.3.1 ...
```

!!! danger
    - Asegúrate de ejecutar `pip3` y no `pip`
    - **Nunca debes ejecutar `pip3` con `sudo`**

Verifica que el _shell_ reconozca la ubicación de `mkdocs`.

```console
$ which mkdocs
/home/tonejito/.local/bin/mkdocs
```

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando tengas las herramientas instaladas.

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
|                             | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../../workflow
[arriba]: ../../workflow
[siguiente]: ../crear-fork

[git-customize]: https://www.git-scm.com/book/en/v2/Customizing-Git-Git-Configuration

[brew]: https://brew.sh/#install
[brew-git]: https://formulae.brew.sh/formula/git#default
[choco]: https://chocolatey.org/install
[choco-git]: https://community.chocolatey.org/packages?q=git
[winget]: https://docs.microsoft.com/en-us/windows/package-manager/winget/
[winget-git]: https://github.com/microsoft/winget-pkgs/tree/master/manifests/g/Git/Git
