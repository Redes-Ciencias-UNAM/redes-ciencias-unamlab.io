---
title: Tarea 3 - Instalación de Cisco Packet Tracer
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Tarea 3: Instalación de Cisco Packet Tracer

--------------------------------------------------------------------------------

## Objetivo

- El alumno instalará la herramienta Packet Tracer de Cisco para simular los dispositivos que forman parte de una red.

## Elementos de apoyo

- [Video de la instalación de Packet Tracer en Ubuntu 📼][video-instalacion-packet-tracer]
- [Video de la creación de una red en Packet Tracer 📼][video-packet-tracer]

<!--
- [Instalación de Packet Tracer en ArchLinux][arch-linux-packettracer-info]

[arch-linux-packettracer-info]: https://gist.github.com/jpyamamoto/6fd4db9c11ac448c366f09a71f7b1cea
[arch-linux-packettracer-wiki]: https://wiki.archlinux.org/title/PacketTracer
[arch-linux-packettracer-aur]: https://aur.archlinux.org/packages/packettracer
-->

## Restricciones

- Esta tarea debe ser entregada de manera **individual**

- La fecha límite de entrega es el **martes 14 de febrero de 2023** a las 23:59 horas

!!! note
    Crear una nueva rama `tarea-3` a partir de la rama de la actividad anterior (`tarea-2`):

    ```
    $ git checkout tarea-2
    $ git checkout -b tarea-3
    ```

## Procedimiento

- Crear una cuenta en el sitio **Skills for All**
    - <https://skillsforall.com/>

- Termina el curso **Getting Started with Cisco Packet Tracer**
    - <https://skillsforall.com/course/getting-started-cisco-packet-tracer>

- Instala Packet Tracer en tu **máquina física**
    - Las instrucciones para esta tarea [están documentadas en una página separada][packet-tracer-install].

- Crea una topología de red simple para una red casera
    - Elabora un "plano" de cómo es la distribución aproximada de las habitaciones
    - No es necesario que pongas fotos de tu casa, únicamente que delimites los espacios

- Explica la topología en el archivo `README.md`
    - Agrega las imágenes que sean necesarias

- Verifica la conectividad de los equipos

- Anexa el archivo PKT resultante en la carpeta `files`

## Entregables

- Archivo `README.md`

    - Explicación de la topología de red utilizada.

        - Simular la salida a Internet utilizando un modem conectado a `Cloud-PT`
        - Utilizar los equipos de red de tipo "Packet Tracer"
            - `Switch-PT`, `Router-PT`, `AccessPoint-PT`, etc.
        - Utilizar PC, laptops, TV, SmartPhone, Tablet e impresoras como clientes
            - Dar de alta por lo menos 4 clientes
            - Utilizar conexiones Ethernet para conectar los clientes al switch
            - Utilizar un AP y conectar los clientes inalámbricos a el
            - Asignar direcciones IP estáticas del segmento `192.168.X.*`
                - Revisa la sección [**extra**](#extra) si deseas utilizar DHCP

    - Listar las imágenes con su respectiva descripción

- Carpeta `img`

    - Imagen donde se muestre que completaste el curso de **Getting Started with Cisco Packet Tracer** en **Skills for All**

    - Ventana "acerca de" Packet Tracer ejecutándose en la **máquina física**

    - Imagen del plano de la distribución física de la red casera

    - Imagen de la topología de red implementada en Packet Tracer

    - Imagen de la configuración de red para todos los equipos

    - Imagen de las pruebas de conectividad entre los equipos

- Carpeta `files`

    - Archivo de la actividad en formato `PKT` (Packet Tracer)

## Extra

- Incluir un equipo `HomeRouter-PT-AC` o `WRT300N` en tu topología de red en lugar del **modem/router** y activa el servicio de DHCP
    - Listar en el `README.md` los pasos que tuviste que seguir para realizar esta configuración
    - Incluir imagenes donde se muestre el proceso de configuración

--------------------------------------------------------------------------------

[flujo-de-trabajo]: https://redes-ciencias-unam.gitlab.io/workflow/
[repo-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests

[video-instalacion-packet-tracer]: https://www.youtube.com/watch?v=TQYe7Rp13xw&list=PLN1TFzSBXi3QWbHwBEV3p4LxV5KceXu8d&index=19
[video-packet-tracer]: https://www.youtube.com/watch?v=yfEIis_3MZk&list=PLN1TFzSBXi3QWbHwBEV3p4LxV5KceXu8d&index=20

[packet-tracer-install]: ./install
