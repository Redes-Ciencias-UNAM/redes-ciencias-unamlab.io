---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Práctica 1 - Laboratorio virtual de redes
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Práctica 1: Laboratorio virtual de redes

--------------------------------------------------------------------------------

## Objetivo

- El alumno instalará un laboratorio virtual para el análisis de redes y la implementación de servicios de red en sistemas operativos GNU/Linux.

## Elementos de apoyo

### Debian / Ubuntu

- [Capturas de pantalla de la instalación de Debian 11 🖥️][debian-install]
- [Capturas de pantalla de la configuración de Debian 11 🖥️][debian-configure]

- [Video de instalación de Debian 11 📼][video-debian-install]
- [Video de instalación de utilerías de VirtualBox en Debian 11 📼][video-debian-guest-additions]

- [Imagen ISO `netinst` de Debian 11 💽][iso-netinst-debian-11]
- [Imagen ISO `desktop` de Ubuntu 22.04 LTS 💽][iso-desktop-ubuntu-22-04-lts]

### CentOS Stream / RockyLinux / AlmaLinux

- [Capturas de pantalla de la instalación de CentOS Stream 9 🖥️][centos-install]
- [Capturas de pantalla de la configuración de CentOS Stream 9 🖥️][centos-configure]

- [Video de instalación de CentOS Stream 9 📼][video-centos-install]
- [Video de instalación de utilerías de VirtualBox en CentOS Stream 9 📼][video-centos-guest-additions]

- [Imagen ISO `boot` de CentOS Stream 9 💽][iso-boot-centos-stream-9]
- [Imagen ISO `boot` de Rocky Linux 9 💽][iso-boot-rocky-linux-9]
- [Imagen ISO `minimal` de Rocky Linux 9 💽][iso-minimal-rocky-linux-9]
- [Imagen ISO `boot` de Alma Linux 9 💽][iso-boot-alma-linux-9]
- [Imagen ISO `minimal` de Alma Linux 9 💽][iso-minimal-alma-linux-9]

## Restricciones

- La fecha límite de entrega es el **martes 21 de febrero de 2023** a las 23:59 horas
- Esta actividad debe ser entregada de manera **individual** de acuerdo al [flujo de trabajo para la entrega de tareas y prácticas][flujo-de-trabajo]
    - Utilizar la carpeta `docs/practicas/practica-1/ApellidoApellidoNombreNombre` para entregar la actividad
    - Crear un _merge request_ en el [repositorio de tareas][repo-tareas] para entregar la actividad

## Entregables

- Archivo `README.md`

###### Máquina física

- Lista de módulos cargados en el kernel de la máquina física
    - Ejecuta este comando si tu máquina física tiene un sistema operativo **GNU/Linux**

        ```
        root@laptop:~# lsmod | egrep -i 'v(irtual)?box'
        	...
        ```

    - Ejecuta este comando si tu máquina física tiene un sistema operativo **macOS**

        ```
        root@laptop:~# kextstat | egrep -i 'v(irtual)?box'
        	...
        ```

    - Ejecuta este comando si tu máquina física tiene un sistema operativo **Windows** y corta la salida para mostrar únicamente los módulos de VirtualBox (`vbox*`)

        ```
        PS C:\> Get-WindowsDriver –Online -All
        ```

- Miembros del grupo `vboxusers`
    - Ejecuta este comando si tu máquina física tiene un sistema operativo **GNU/Linux**

```console
root@laptop:~# getent group vboxusers
	...

root@laptop:~# id USUARIO
	...
```

!!! note
    Reemplaza `USUARIO` por el nombre de tu usuario en la **máquina física**

- Ubicación de VirtualBox y VBoxManage
    - Ejecuta este comando si tu máquina física tiene un sistema operativo **GNU/Linux** o **macOS**

```console
tonejito@laptop:~$ which VirtualBox VBoxManage
	...

tonejito@laptop:~$ whereis VirtualBox VBoxManage
	...
```

- Salida de VirtualBox donde se listen las características de la **máquina física**

```console
tonejito@laptop:~$ VBoxManage list hostinfo
	...

tonejito@laptop:~$ VBoxManage list hostcpuids
	...

tonejito@laptop:~$ VBoxManage list hostdrives
	...

tonejito@laptop:~$ VBoxManage list hostfloppies
	...

tonejito@laptop:~$ VBoxManage list hostdvds
	...

tonejito@laptop:~$ VBoxManage list systemproperties
	...

tonejito@laptop:~$ VBoxManage list usbfilters
	...

tonejito@laptop:~$ VBoxManage list usbhost
	...

tonejito@laptop:~$ VBoxManage list webcams
	...

tonejito@laptop:~$ VBoxManage list extpacks
	...

tonejito@laptop:~$ VBoxManage list bridgedifs
	...

tonejito@laptop:~$ VBoxManage list hostonlyifs
	...

tonejito@laptop:~$ VBoxManage list hostonlynets
	...

tonejito@laptop:~$ VBoxManage list dhcpservers
	...

tonejito@laptop:~$ VBoxManage list floppies
	...

tonejito@laptop:~$ VBoxManage list dvds
	...

tonejito@laptop:~$ VBoxManage list hdds
	...
```

###### Máquinas virtuales

- Pantallas donde se muestre la información del sistema de las máquinas virtuales [Debian][debian-about] y [CentOS][centos-about]
    - Listar cada imágen con su respectiva descripción en el archivo `README.md`
    - Incluir la salida del comando `neofetch` o alguna herramienta similar **si la máquina virtual no tiene interfaz gráfica**.

```text
| ![](img/imagen.png)      |
|:------------------------:|
| Descripción de la imagen |
```

- Listar los identificadores de cada máquina virtual

```console
tonejito@laptop:~$ VBoxManage list vms --long
	...
```

- Listar la configuración interna de cada máquina virtual en VirtualBox
    - Repetir para cada una de las máquinas virtuales (Debian y CentOS)

```
tonejito@laptop:~$ NOMBRE_VM='Nombre de mi maquina virtual'
	...

tonejito@laptop:~$ VBoxManage showvminfo "${NOMBRE_VM}" --machinereadable
	...
```

- Agregar un bloque de _texto preformateado_ donde se liste la información del sistema para Debian y CentOS
    - Agregar esta información como texto en el archivo `README.md`, **no agregar capturas de pantalla**
    - Repetir para cada una de las máquinas virtuales (Debian y CentOS)

```console
root@virtual:~# uname -a
	...

root@virtual:~# cat /etc/os-release
	...

root@virtual:~# cat /etc/debian_version /etc/redhat-release
	...

root@virtual:~# hostname -I
	...

root@virtual:~# ip addr
	...

root@virtual:~# ip route show
	...

root@virtual:~# nmcli connection show
	...

root@virtual:~# cat /etc/resolv.conf
	...

root@virtual:~# netstat -ntulp
	...

root@virtual:~# ping -c 4 1.1.1.1
	...

root@virtual:~# ping6 -c 4 one.one.one.one.
	...

root@virtual:~# dig example.com.
	...

root@virtual:~# lsmod
	...

root@virtual:~# ps afx
	...
```

- Listar los privilegios que tiene el usuario normal
    - La llamada a `sudo -i` no debería pedir contraseña
    - Agregar esta información como texto en el archivo `README.md`, **no agregar capturas de pantalla**
    - Repetir para cada una de las máquinas virtuales (Debian y CentOS)

```console
redes@virtual:~$ getent passwd ${USER}
	...
redes@virtual:~$ id
	...
redes@virtual:~$ groups
	...
redes@virtual:~$ sudo -l
	...
redes@virtual:~$ sudo -i
	...
```

- Agregar un bloque de _texto preformateado_ donde se liste la ubicación de las herramientas que se instalaron en [Debian][debian-tools] y [CentOS][centos-tools]
    - Agregar esta información como texto en el archivo `README.md`, **no agregar capturas de pantalla**
    - Repetir para cada una de las máquinas virtuales (Debian y CentOS)

```console
redes@virtual:~$ which wireshark tcpdump nmap ...

	...

redes@virtual:~$ whereis wireshark tcpdump nmap ...

	...
```

## Procedimiento

### Instalar máquinas virtuales

- Instalar [VirtualBox][virtualbox] en el equipo físico junto con el _Extension Pack_

- Instalar una máquina virtual donde se administren los paquetes con `apt`
    - [Debian 11][debian] _"bullseye"_
    - [Ubuntu 22.04 LTS][ubuntu] _"jammy"_
    - Hay una serie de pantallas sobre la [instalación de Debian 11][debian-install]

- Instalar otra máquina virtual donde se administren los paquetes con `yum` o `dnf`
    - [CentOS Stream 9][centos]
    - [Rocky Linux 9][rocky]
    - [Alma Linux 9][alma]
    - Hay una serie de pantallas sobre la [instalación de CentOS Stream 9][centos-install]

- Se recomienda que las máquinas virtuales tengan los siguientes recursos:
    - 1 vCPU
    - 2 GB de RAM
    - 10 GB de disco

### Configuración de red

Cada una de las máquinas virtuales deben tener dos interfaces de red

- Interfaz 1: NAT
- Interfaz 2: Host-only (sólo anfitrión)

### Configuración de usuarios

- Habilitar `sudo` sin contraseña
- Instalar SSH para acceso remoto

### Configuración de software

- Instalar utlerías del sistema
- Instalar herramientas de desarrollo
- Instalar _software_ para análisis de redes

### Configuración extra de VirtualBox

- Configurar los siguientes elementos para las máquinas virtuales
    - [Debian 11][debian-configure]
    - [CentOS Stream 9][centos-configure]
- Instalar los _Guest Additions_ de VirtualBox
- Habilitar el portapapeles compartido
- Configurar una carpeta compartida para copiar archivos entre la máquina física y las máquinas virtuales

--------------------------------------------------------------------------------

[virtualbox]: https://www.virtualbox.org/wiki/Downloads
[debian]: https://debian.org/download
[ubuntu]: https://ubuntu.com/download/desktop/thank-you?version=20.04.3&architecture=amd64
[centos]: https://centos.org/download/
[rocky]: https://rockylinux.org/download
[alma]: https://almalinux.org/isos.html

[fedora-virt-ext]: https://docs.fedoraproject.org/en-US/Fedora/13/html/Virtualization_Guide/sect-Virtualization-Troubleshooting-Enabling_Intel_VT_and_AMD_V_virtualization_hardware_extensions_in_BIOS.html

[nixcraft-virt]: https://www.cyberciti.biz/faq/linux-xen-vmware-kvm-intel-vt-amd-v-support/

[video-debian-install]: https://www.youtube.com/watch?v=IKig3W6b3kg
[video-debian-guest-additions]: https://www.youtube.com/watch?v=yptpnm5XXYE

[video-centos-install]: https://www.youtube.com/watch?v=uQT7MHJTbfA
[video-centos-guest-additions]: https://www.youtube.com/watch?v=EPScixuHzm4

[flujo-de-trabajo]: https://redes-ciencias-unam.gitlab.io/workflow/
[repo-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests

[debian-install]: ./debian-install
[centos-install]: ./centos-install

[debian-configure]: ./debian-configure
[centos-configure]: ./centos-configure

[debian-tools]: ./debian-configure/#instalar-software
[centos-tools]: ./centos-configure/#instalar-software

[debian-about]: ./debian-configure/#informacion-del-sistema
[centos-about]: ./centos-configure/#informacion-del-sistema

[iso-netinst-debian-11]: https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso
[iso-desktop-ubuntu-22-04-lts]: https://releases.ubuntu.com/22.04.1/ubuntu-22.04.1-desktop-amd64.iso

[iso-boot-centos-stream-9]: https://mirror.rackspace.com/centos-stream/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-latest-x86_64-boot.iso
[iso-boot-rocky-linux-9]: https://download.rockylinux.org/pub/rocky/9/isos/x86_64/Rocky-9.1-x86_64-boot.iso
[iso-minimal-rocky-linux-9]: https://download.rockylinux.org/pub/rocky/9/isos/x86_64/Rocky-9.1-x86_64-minimal.iso

[iso-boot-alma-linux-9]: http://repo.almalinux.org/almalinux/9.1/isos/x86_64/AlmaLinux-9-latest-x86_64-boot.iso
[iso-minimal-alma-linux-9]: http://repo.almalinux.org/almalinux/9.1/isos/x86_64/AlmaLinux-9-latest-x86_64-minimal.iso
