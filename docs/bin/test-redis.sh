#!/bin/bash -e
#	= ^ . ^ =
# Revisa el servidor redis que se ejecuta localmente

# https://docs.redis.com/latest/rs/databases/connect/test-client-connectivity/
# https://redis.io/docs/manual/cli/
# https://redis.io/commands/

which date netstat nc redis-cli >/dev/null

# /etc/init.d/redis-server restart

SVC=redis
REDIS_HOST=localhost
REDIS_PORT=6379

DATE_EPOCH="$(date '+%s')"
DATE_MAIL="$(date --rfc-email)"
DATE_RFC="$(date --rfc-3339='seconds')"
DATE_ISO="$(date --iso-8601='minutes')"

CGI=${GATEWAY_INTERFACE:-NO}
if [ "${CGI}" != "NO" ]
then
  printf "%s\n\n" "Content-Type: text/plain"
else
  echo "NO CGI!"
fi

set -evuo pipefail

netstat -ntulp | egrep -i "^Proto|${SVC}|${REDIS_PORT}"

nc -vzW 1 ${REDIS_HOST} ${REDIS_PORT}

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} --raw HELLO 3
redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} --raw SELECT 0
redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} --raw TIME
redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} --raw ECHO REDIS
redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} --raw MODULE LIST

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} << EOF
SET _EPOCH "${DATE_EPOCH}"
GET _EPOCH
EOF

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} << EOF
SET _MAIL "${DATE_MAIL}"
GET _MAIL
EOF

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} << EOF
SET _RFC "${DATE_RFC}"
GET _RFC
EOF

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} << EOF
SET _ISO "${DATE_ISO}"
GET _ISO
EOF

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} -r 3 -i 1 --raw INCR _counter

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} --raw KEYS '_[A-z]*'

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} << EOF
DEL _EPOCH
DEL _MAIL
DEL _RFC
DEL _ISO
EOF

redis-cli -h ${REDIS_HOST} -p ${REDIS_PORT} --raw "FLUSHALL"
