#!/bin/bash -e
#	= ^ . ^ =
# Revisa el servidor memcache que se ejecuta localmente

# https://github.com/memcached/memcached/wiki/Protocols
# https://github.com/memcached/memcached/blob/master/doc/protocol.txt

which date wc netstat nc unix2dos >/dev/null

# /etc/init.d/memcached restart

SVC=memcache
MEMCACHE_HOST=localhost
MEMCACHE_PORT=11211

DATE_EPOCH="$(date '+%s')"
DATE_EPOCH_SIZE=$(printf "${DATE_EPOCH}" | wc -m)
DATE_MAIL="$(date --rfc-email)"
DATE_MAIL_SIZE=$(printf "${DATE_MAIL}" | wc -m)
DATE_RFC="$(date --rfc-3339='seconds')"
DATE_RFC_SIZE=$(printf "${DATE_RFC}" | wc -m)
DATE_ISO="$(date --iso-8601='minutes')"
DATE_ISO_SIZE=$(printf "${DATE_ISO}" | wc -m)

CGI=${GATEWAY_INTERFACE:-NO}
if [ "${CGI}" != "NO" ]
then
  printf "%s\n\n" "Content-Type: text/plain"
else
  echo "NO CGI!"
fi

set -evuo pipefail

netstat -ntulp | egrep -i "^Proto|${SVC}|${MEMCACHE_PORT}"

nc -vzW 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}

echo "mn" | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}

echo "version" | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}

cat << EOF | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}
stats
stats conns
EOF

# set KEY META_DATA EXPIRY_TIME LENGTH_IN_BYTES
cat << EOF | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}
set DATE_EPOCH 0 60 ${DATE_EPOCH_SIZE}
${DATE_EPOCH}
get DATE_EPOCH
EOF

cat << EOF | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}
set DATE_MAIL 0 60 ${DATE_MAIL_SIZE}
${DATE_MAIL}
get DATE_MAIL
EOF

cat << EOF | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}
set DATE_RFC 0 60 ${DATE_RFC_SIZE}
${DATE_RFC}
get DATE_RFC
EOF

cat << EOF | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}
set DATE_ISO 0 60 ${DATE_ISO_SIZE}
${DATE_ISO}
get DATE_ISO
EOF

echo "touch DATE_MAIL 120" | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}

echo "delete DATE_ISO" | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}

echo "gat 120 DATE_RFC" | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}

DATE_EPOCH="$(date '+%s')"
cat << EOF | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}
replace DATE_EPOCH 0 90 ${DATE_EPOCH_SIZE}
${DATE_EPOCH}
get DATE_EPOCH
EOF

cat << EOF | unix2dos | nc -W 1 ${MEMCACHE_HOST} ${MEMCACHE_PORT}
mn
flush_all
quit
EOF
