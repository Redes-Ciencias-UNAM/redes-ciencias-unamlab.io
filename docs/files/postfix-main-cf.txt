# See /usr/share/postfix/main.cf.dist for a commented, more complete version

# Debian specific:  Specifying a file name will cause the first
# line of that file to be used as the name.  The Debian default
# is /etc/mailname.
#myorigin = /etc/mailname

# Banner ESMTP (sin $mail_name)
smtpd_banner = $myhostname ESMTP
biff = no

# appending .domain is the MUA's job.
append_dot_mydomain = no

# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h

readme_directory = no

# See http://www.postfix.org/COMPATIBILITY_README.html -- default to 2 on
# fresh installs.
compatibility_level = 2

# TLS parameters
smtpd_tls_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
smtpd_tls_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
smtpd_use_tls=yes
smtpd_tls_session_cache_database = btree:${data_directory}/smtpd_scache
smtp_tls_session_cache_database = btree:${data_directory}/smtp_scache

# Use AWS SES as relayhost
# https://docs.aws.amazon.com/ses/latest/DeveloperGuide/postfix.html
smtp_sasl_auth_enable = yes
smtp_sasl_security_options = noanonymous
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_use_tls = yes
smtp_tls_security_level = encrypt
smtp_tls_note_starttls_offer = yes
# smtp_tls_CApath=/etc/ssl/certs
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt

# See /usr/share/doc/postfix/TLS_README.gz in the postfix-doc package for
# information on enabling SSL in the smtp client.

smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = TEST.redes.tonejito.cf
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
# El contenido del archivo /etc/mailname es "TEST.redes.tonejito.cf"
myorigin = /etc/mailname
mydestination = $myhostname, TEST.redes.tonejito.cf, example.com, , localhost.localdomain, localhost
# Servidor de correo AWS SES
relayhost = [email-smtp.us-east-2.amazonaws.com]:587
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128
mailbox_command = procmail -a "$EXTENSION"
mailbox_size_limit = 0
recipient_delimiter = +
# Escucha solo en localhost por IPv4 (127.0.0.1) e IPv6 (::1)
inet_interfaces = loopback-only
inet_protocols = all
