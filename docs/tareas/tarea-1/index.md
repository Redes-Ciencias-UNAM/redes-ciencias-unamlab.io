---
title: Tarea 1 - Flujo de trabajo en GitLab
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Tarea 1: Flujo de trabajo en GitLab

--------------------------------------------------------------------------------

## Objetivo

- El alumno configurará un repositorio _fork_ en GitLab para el envío de las actividades del curso

## Elementos de apoyo

- [Introducción al control de versiones con `git`][curso-git]
- [Flujo de trabajo para la entrega de actividades][flujo-de-trabajo]

## Restricciones

- Esta tarea debe ser entregada de manera **individual**

- La fecha límite de entrega es el **viernes 10 de febrero de 2023** a las 23:59 horas

<!--
!!! note
    Crear una nueva rama `tarea-1` a partir de la rama `entregas`:

    ```
    $ git checkout entregas
    $ git checkout -b tarea-1
    ```

    Todas las demás veces se creará la rama a partir de la actividad anterior
-->

## Procedimiento

- Seguir el [flujo de trabajo presentado en el sitio de tareas][flujo-de-trabajo]

## Entregables

- Archivo `README.md`

    - Carácterísticas de la computadora que utilizas para hacer tus actividades
    - Listar cada una de las imágenes con su respectiva descripción
    - **Ingresar un comentario sobre la actividad**
        - ¿Qué tan útil te pareció este ejercicio?
        - ¿Tuviste alguna complicación durante la elaboración de esta actividad?
        - ¿Consideras que esta actividad te será útil en tu vida profesional?

- Carpeta `img`

    - Capturas de pantalla donde se muestre lo siguiente:
        - Versión del sistema operativo
        - Tipo de procesador
        - Cantidad de memoria RAM instalada en el equipo
        - Cantidad de espacio en disco disponible en el equipo

--------------------------------------------------------------------------------

[curso-git]: https://tonejito.github.io/curso-git/
[flujo-de-trabajo]: ../../workflow
[repo-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests
