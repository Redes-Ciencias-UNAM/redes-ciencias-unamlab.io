---
title: Proyectos
---

# Proyectos

| Título                                                               | Ambiente       | Entrega
|:---------------------------------------------------------------------|:--------------:|:----------:|
| **Proyecto 1**: [Implementación de una VPN](proyecto-vpn)            | _Nube pública_ | _Por definir_

<!--
!!! danger
    - `¹`: La evaluación de este proyecto final corresponderá al **tercer examen parcial** del curso
    - `²`: La evaluación de este proyecto final corresponderá al **cuarto examen parcial** del curso
-->

--------------------------------------------------------------------------------

|                 ⇦            |        ⇧      |                  ⇨           |
|:-----------------------------|:-------------:|-----------------------------:|
| [Laboratorio][laboratorio]   | [Arriba](../) | [Examenes][examenes]         |

[arriba]: ../
[tareas]: ../tareas/
[laboratorio]: ../laboratorio/
[proyectos]: ../proyectos/
[examenes]: ../examenes/
