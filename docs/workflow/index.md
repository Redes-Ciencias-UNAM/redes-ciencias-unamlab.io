---
title: Flujo de trabajo para la entrega de actividades
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Flujo de trabajo para la entrega de actividades

| ![https://drive.google.com/open?id=1_fhDH4NX_2kvMWoRupcdpaOqAATsbw9s](img/000-workflow.png "")
|:--------:|
|

<!-- -->
!!! note
    Este contenido también está disponible en video:

    - [Control de versiones con GIT 📼](https://www.youtube.com/watch?v=sLSqGjY84Sg&list=PLN1TFzSBXi3QZ3NpSSIFFp3hNWlIdCq5w&index=7)
    - [Flujo de trabajo para entrega de actividades 📼](https://www.youtube.com/watch?v=JDl_KRr_UYg&list=PLN1TFzSBXi3QZ3NpSSIFFp3hNWlIdCq5w&index=8)
<!-- -->

El flujo de trabajo consiste en los siguientes pasos:

- [Instalar dependencias](instalar-dependencias)
- [Hacer _fork_ del repositorio principal de actividades](crear-fork)
- [Clonar el repositorio y crear rama de trabajo](clonar-fork)
- [Instalar pre-commit en el repositorio local](configurar-precommit)
- [Agregar contenido a la copia de trabajo y enviar los cambios](agregar-contenido)
- [Crear el _merge request_](crear-merge-request)

Revisa las secciones de ayuda si tienes algún problema:

- [Resolver conflictos en un merge request](conflicts)
- [Revisar fallos en el _pipeline_](debug)

--------------------------------------------------------------------------------

[repositorio-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes.git
[repositorio-vista-web]: https://redes-ciencias-unam.gitlab.io/2023-2/tareas-redes/
[gitlab-login]: https://gitlab.com/users/sign_in
[repositorio-personal]: https://gitlab.com/USUARIO/tareas-redes
