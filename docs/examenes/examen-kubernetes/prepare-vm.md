---
title: Preparación de la máquina virtual para k3s
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Preparación de la máquina virtual para `k3s`

--------------------------------------------------------------------------------

!!! danger
    - Las configuraciones aqui listadas son únicamente para un ambiente de pruebas
    - Ninguna de estas configuraciones se debe realizar en ambientes de producción

!!! warning
    - Estas configuraciones se realizan para que el cluster de k3s pueda ejecutarse en el equipo `B1S` que tiene únicamente **1 vCPU** y **512 MB de RAM**
    - No utilizar el tipo de instancia `B1S` para clusters de Kubernetes en ambientes de producción

<!--
https://docs.microsoft.com/en-us/azure/cost-management-billing/manage/create-free-services
https://docs.microsoft.com/en-us/azure/virtual-machines/sizes-b-series-burstable
https://azure.microsoft.com/en-us/blog/introducing-b-series-our-new-burstable-vm-size/
-->

## Revisar el uso de memoria RAM en el equipo

Revisar el uso de memoria RAM, debería ser significantemente menos que cuando se inició esta práctica

```console
root@example:~# free -m
               total        used        free      shared  buff/cache   available
Mem:             913         498         206           1         209         281
Swap:              0           0           0
```

--------------------------------------------------------------------------------

## Deshabilitar los servicios de red del proyecto anterior

!!! danger
    - Unicamente **deshabilita** los servicios de red
    - No vayas a **desinstalarlos**

### Deshabilitar servidor web

**Deshabilita** el demonio de HTTP instalado en la práctica anterior

```console
root@example:~# systemctl disable --now apache2 nginx
	...
```

### Deshabilitar base de datos

**Deshabilita** el servicio de base de datos instalado en la práctica anterior

```console
root@example:~# systemctl disable --now mariadb postgresql
	...
```

### Deshabilitar caché en RAM

**Deshabilita** el servicio de caché en RAM instalado en la práctica anterior

```console
root@example:~# systemctl disable --now memcached redis-server
	...
```

### Deshabilitar servicio de la aplicación

**Deshabilita** los servicios de SystemD instalados por tu aplicación en la práctica anterior

!!! note
    - Esto depende de cada aplicación

```console
root@example:~# systemctl disable --now mattermost
	...

root@example:~# systemctl disable --now openproject openproject-web openproject-worker
	...
```

--------------------------------------------------------------------------------

## Reemplazar el demonio de `syslog`

Se debe reemplazar el demonio de syslog para hacer que la máquina virtual consuma menos memoria RAM

En este caso se va a reemplazar `rsyslog` con `busybox-syslogd`

```console
root@example:~# apt remove rsyslog
	...

root@example:~# apt install busybox-syslogd
	...
```

--------------------------------------------------------------------------------

## Deshabilitar el servicio de actualizaciones automáticas

Otra manera de bajar el uso de memoria RAM es deshabilitar el servicio `unattended-upgrades`

```console
root@example:~# systemctl disable unattended-upgrades
```

--------------------------------------------------------------------------------

## Deshabilitar tareas programadas

El equipo ejecuta algunas tareas de manera periódica.
Estas tareas hacen que se incremente el uso de memoria RAM por lo que se pueden deshabilitar para el contexto de esta práctica

```console
root@example:~# mkdir -vp /etc/cron.daily.disabled
root@example:~# mv -v /etc/cron.daily /etc/cron.daily.disabled
	...
```

--------------------------------------------------------------------------------

## Reducir el uso de memoria de `JournalD`

El demonio `systemd-journald` guarda las bitácoras de estado de los servicios del sistema, se pueden deshabilitar algunas funciones para reducir el uso de memoria RAM.

Establecer los siguientes valores en el archivo de configuración `/etc/systemd/journald.conf`

```console
root@example:~# egrep -v '^\s*(#|$)' /etc/systemd/journald.conf
[Journal]
Storage=none
ForwardToSyslog=no
ForwardToKMsg=no
ForwardToConsole=no
ForwardToWall=no
ReadKMsg=no
Audit=no
```

--------------------------------------------------------------------------------

## Establecer un área de intercambio `SWAP`

[Desde la versión 1.22 de Kubernetes][kubernetes-blog-swap-alpha] es posible utilizar [memoria swap en los nodos del cluster][kubernetes-nodes-swap-alpha], esta característica está en fase `alpha` de desarrollo por lo que puede cambiar en cualquier versión futura

Esto ayuda mucho a nodos con poca memoria RAM, como la máquina virtual de tipo `B1S` en Azure

[kubernetes-blog-swap-alpha]: https://kubernetes.io/blog/2021/08/09/run-nodes-with-swap-alpha/
[kubernetes-nodes-swap-alpha]: https://kubernetes.io/docs/concepts/architecture/nodes/#swap-memory

### Habilitar política de SWAP en `sysctl`

Establecer la política `vm.swappiness` en el archivo de configuración `/etc/sysctl.d/local.conf`

```console
root@example:~# cat >> /etc/sysctl.d/local.conf << EOF
###################################################################
# https://www.kernel.org/doc/html/latest/admin-guide/sysctl/vm.html#swappiness
vm.swappiness = 1
EOF
```

Recargar la configuración de `sysctl` para probar que los cambios surtieron efecto

```console
root@example:~# sysctl -p

root@example:~# sysctl -p /etc/sysctl.d/local.conf
vm.swappiness = 1

root@example:~# cat /proc/sys/vm/swappiness
1
```

<!--
### Crear partición SWAP en el disco adicional

Listar los dispositivos de bloque y verificar que `/dev/sdb1` se encuentre montado en el directorio `/mnt`

!!! warning
    En caso de que la máquina virtual no tenga el disco `/dev/sdb`, se tendrá que crear un _swap-file_ en la ruta `/.swap` con dueño `root:root` y permisos `0600`

```console
root@example:~# lsblk
NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda       8:0    0   30G  0 disk
|-sda1    8:1    0 29.9G  0 part /
|-sda14   8:14   0    3M  0 part
\-sda15   8:15   0  124M  0 part /boot/efi
sdb       8:16   0    4G  0 disk
\-sdb1    8:17   0    4G  0 part /mnt
```

Desmontar el directorio `/mnt` para dejar de usar la partición `/dev/sdb1`

```console
root@example:~# umount /mnt
```

Editar el archivo `/etc/fstab` para deshabilitar el punto de montaje de la partición `/dev/sdb1`

- Comentar la siguiente línea al final del archivo `/etc/fstab`

```console
# /dev/disk/cloud/azure_resource-part1	/mnt	auto	defaults,nofail,comment=cloudconfig	0	2
```

Cambiar el tipo de partición a `Linux swap` con cfdisk

```console
root@example:~# cfdisk /dev/sdb
```

Usar las flechas izquierda y derecha para resaltar la opción `[ Type ]` y presionar `< Enter >`

```console
                                 Disk: /dev/sdb
                 Size: 4 GiB, 4294967296 bytes, 8388608 sectors
                       Label: dos, identifier: 0xd4c0b1f1

    Device       Boot     Start       End   Sectors   Size   Id Type
>>  /dev/sdb1              2048   8386559   8384512     4G    7 HPFS/NTFS/exFAT


 ┌────────────────────────────────────────────────────────────────────────────┐
 │ Partition type: HPFS/NTFS/exFAT (7)                                        │
 │Filesystem UUID: d9d2504c-e0bd-471f-9ece-3911f3616f49                       │
 │     Filesystem: ext4                                                       │
 └────────────────────────────────────────────────────────────────────────────┘
     [Bootable]  [ Delete ]  [ Resize ]  [  Quit  ]  [  Type  ]  [  Help  ]		⬅️ [ Type ]
     [  Write ]  [  Dump  ]

                           Change the partition type
```

Utilizar las flechas arriba y abajo para seleccionar `Linux swap` y presionar `< Enter >`

```console
                    ┌ Select partition type ────────────↑─┐
                    │ 5c Priam Edisk                      │
                    │ 61 SpeedStor                        │
                    │ 63 GNU HURD or SysV                 │
                    │ 64 Novell Netware 286               │
                    │ 65 Novell Netware 386               │
                    │ 70 DiskSecure Multi-Boot            │
                    │ 75 PC/IX                            │
                    │ 80 Old Minix                        │
                    │ 81 Minix / old Linux                │
                    │ 82 Linux swap / Solaris             │		⬅️ Linux swap
                    │ 83 Linux                            │
                    │ 84 OS/2 hidden or Intel hibernation │
                    │ 85 Linux extended                   │
                    │ 86 NTFS volume set                  │
                    │ 87 NTFS volume set                  │
                    │ 88 Linux plaintext                  │
                    │ 8e Linux LVM                        │
                    │ 93 Amoeba                           │
                    │ 94 Amoeba BBT                       │
                    │ 9f BSD/OS                           │
                    │ a0 IBM Thinkpad hibernation         │
                    └───────────────────────────────────↓─┘

```

Utilizar las flechas izquierda y derecha para seleccionar la opción `[ Write ]` y presionar `< Enter >`

```console
                                 Disk: /dev/sdb
                 Size: 4 GiB, 4294967296 bytes, 8388608 sectors
                       Label: dos, identifier: 0xd4c0b1f1

    Device       Boot    Start      End  Sectors  Size  Id Type
>>  /dev/sdb1             2048  8386559  8384512    4G  82 Linux swap / Solaris


 ┌────────────────────────────────────────────────────────────────────────────┐
 │ Partition type: Linux swap / Solaris (82)                                  │
 │Filesystem UUID: d9d2504c-e0bd-471f-9ece-3911f3616f49                       │
 │     Filesystem: ext4                                                       │
 └────────────────────────────────────────────────────────────────────────────┘
     [Bootable]  [ Delete ]  [ Resize ]  [  Quit  ]  [  Type  ]  [  Help  ]		⬅️ [ Write ]
     [  Write ]  [  Dump  ]

            Write partition table to disk (this might destroy data)
```

Escribir `yes` para confirmar el cambio y presionar `< Enter >`

```console
                                 Disk: /dev/sdb
                 Size: 4 GiB, 4294967296 bytes, 8388608 sectors
                       Label: dos, identifier: 0xd4c0b1f1

    Device       Boot    Start      End  Sectors  Size  Id Type
>>  /dev/sdb1             2048  8386559  8384512    4G  82 Linux swap / Solaris


 ┌────────────────────────────────────────────────────────────────────────────┐
 │ Partition type: Linux swap / Solaris (82)                                  │
 │Filesystem UUID: d9d2504c-e0bd-471f-9ece-3911f3616f49                       │
 │     Filesystem: ext4                                                       │
 └────────────────────────────────────────────────────────────────────────────┘
 Are you sure you want to write the partition table to disk? yes


             Type "yes" or "no", or press ESC to leave this dialog.
```

Utilizar las flechas izquierda y derecha para seleccionar la opción `[ Quit ]` y presionar `< Enter >`


```console
                                 Disk: /dev/sdb
                 Size: 4 GiB, 4294967296 bytes, 8388608 sectors
                       Label: dos, identifier: 0xd4c0b1f1

    Device       Boot    Start      End  Sectors  Size  Id Type
>>  /dev/sdb1             2048  8386559  8384512    4G  82 Linux swap / Solaris


 ┌────────────────────────────────────────────────────────────────────────────┐
 │ Partition type: Linux swap / Solaris (82)                                  │
 │Filesystem UUID: d9d2504c-e0bd-471f-9ece-3911f3616f49                       │
 │     Filesystem: ext4                                                       │
 └────────────────────────────────────────────────────────────────────────────┘
     [Bootable]  [ Delete ]  [ Resize ]  [  Quit  ]  [  Type  ]  [  Help  ]		⬅️ [ Quit ]
     [  Write ]  [  Dump  ]

                      Quit program without writing changes
```

Comprobar que la partición es de tipo `swap`

```console
root@example:~# fdisk -l /dev/sdb
Disk /dev/sdb: 4 GiB, 4294967296 bytes, 8388608 sectors
Disk model: Virtual Disk
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0xd4c0b1f1

Device     Boot Start     End Sectors Size Id Type
/dev/sdb1        2048 8386559 8384512   4G 82 Linux swap / Solaris	⬅️
```

Crear un espacio de SWAP en la partición `/dev/sdb1`

!!! danger
    Verificar que la partición en la que se crea el espacio de SWAP no esté montada y no esté siendo utilizada por el sistema operativo

```console
root@example:~# mkswap /dev/sdb1
mkswap: /dev/sdb1: warning: wiping old ext4 signature.
Setting up swapspace version 1, size = 4 GiB (4292866048 bytes)
no label, UUID=90c047f8-a87e-4ef6-85ee-c9521e1afc5e
```
-->

### Crear _swap-file_ en el archivo `/.swap`

Preparar _swap-file_ en el archivo `/.swap`

```console
root@example:~# touch /.swap

root@example:~# chmod -c 0600 /.swap
mode of '/.swap' changed from 0644 (rw-r--r--) to 0600 (rw-------)
```

Crear archivo _sparse_ para SWAP

```console
root@example:~# dd if=/dev/zero of=/.swap bs=1M count=1024 status=progress
1051721728 bytes (1.1 GB, 1003 MiB) copied, 17 s, 61.8 MB/s
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 18.6303 s, 57.6 MB/s

root@example:~# ls -lah /.swap
-rw------- 1 root root 1.0G May 22 12:56 /.swap
```

Crear área de intercambio _swap_ en el archivo `/.swap`

```console
root@example:~# mkswap -L swap /.swap
Setting up swapspace version 1, size = 1024 MiB (1073737728 bytes)
LABEL=swap, UUID=6298196b-9e0e-4440-b41b-367e0b671f91
```

Para más información sobre SWAP y _swap-files_

- <https://www.linux.com/training-tutorials/increase-your-available-swap-space-swap-file/>
- <https://www.linuxjournal.com/article/10678>
- <https://www.linuxjournal.com/video/emergency-swapfile-when-your-memory-fills>
- <https://wiki.debian.org/Swap>

<!--
https://wiki.archlinux.org/title/Swap#Swap_file
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/managing_storage_devices/getting-started-with-swap_managing-storage-devices#creating-a-swap-file_getting-started-with-swap
https://docs.oracle.com/en/learn/file_system_linux_8/#task-9-increase-swap-space
https://docs.oracle.com/en/operating-systems/oracle-linux/6/admin/swap-create-use.html
https://docs.rackspace.com/support/how-to/create-a-linux-swap-file/
https://docs.rackspace.com/support/how-to/create-remove-swap-file-in-ubuntu/
https://itsfoss.com/create-swap-file-linux/
https://www.tecmint.com/create-a-linux-swap-file/
https://tecadmin.net/linux-create-swap/
https://linuxize.com/post/create-a-linux-swap-file/
https://linoxide.com/how-to-create-linux-swap-file/
https://www.vultr.com/docs/setup-swap-file-on-linux/
-->

### Crear configuración de montaje de SWAP

Verificar que la línea donde se monta el directorio `/mnt` está conentada y establecer el punto de montaje para la partición SWAP en el archivo `/etc/fstab`

!!! warning
    En caso de utilizar un _swap-file_ reemplazar `/dev/sdb1` con `/.swap` en el primer campo de `/etc/fstab`

```console
root@example:~# cat /etc/fstab
# /etc/fstab: static file system information
UUID=c0db73f0-f7cd-43df-b16e-20ae1caca357 / ext4 rw,discard,errors=remount-ro,x-systemd.growfs 0 1
UUID=ECD7-6DBA /boot/efi vfat defaults 0 0
/dev/disk/cloud/azure_resource-part1	/mnt	auto	defaults,nofail,comment=cloudconfig	0	2

# swap
/.swap	none	swap	defaults	0	0	⬅️
```

### Habilitar SWAP

Habilitar de manera manual la partición SWAP para verifica que la configuración es correcta

```console
root@example:~# swapon -va
swapon: /.swap: found signature [pagesize=4096, signature=swap]
swapon: /.swap: pagesize=4096, swapsize=1073741824, devsize=1073741824
swapon /.swap
```

Revisar el uso de memoria RAM, debería ser significantemente menos que cuando se inició esta práctica

```console
root@example:~# free -m
               total        used        free      shared  buff/cache   available
Mem:             863         182          62           0         618         547
Swap:           1023           0        1023	⬅️
```

--------------------------------------------------------------------------------

## Verifica la configuración

Reinicia el equipo para verificar que los cambios sean persistentes

```console
root@example:~# reboot
```

!!! danger
    - Verifica que **TODAS** las configuraciones que hiciste estén presentes respués de reiniciar la máquina antes de continuar con [la siguiente sección][siguiente]

!!! note
    - Continúa en [la siguiente página][siguiente] si el uso de memoria RAM es menor o igual al mostrado anteriormente

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../docker
[siguiente]: ../k3s-install
