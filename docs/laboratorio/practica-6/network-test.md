---
title: Pruebas de conectividad
authors:
- Dante Erik Santiago Rodríguez Pérez
- Andrés Leonardo Hernández Bermúdez
---

# Pruebas de conectividad

## Diagrama de red

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-----------------------------:|
| ![](img/diagrama_red.png)
| 🟠  <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵    <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢   <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet

--------------------------------------------------------------------------------

## Verificación de configuracion de pfSense

Realiza la verificación de configuración de pfSense y guarda los resultados en la carpeta `files/pfsense/`

- ✅ Configuración inicial de pfSense en modo texto
- ✅ Configuración inicial de pfSense con la webUI
    - ✅ <span class="red">Contraseña `Redes-2023-2` para pfSense</span>
    - ✅ <span class="red">El nombre de host debe ser `pfSense-ABCD-EFGH-IJKL-MNOP-QRST.local`</span>
    - ✅ Interfaces de red WAN, LAN, OPT
- ✅ Respaldos de la configuración de pfSense
    - ✅ Respaldo **INICIAL** de pfsense (`pfsense-inicial.xml`)
    - ✅ Respaldo **FINAL** de pfsense (`pfsense-final.xml`)
- ✅ Reglas de tráfico
    - ✅ Interfaces de red WAN, LAN, OPT
    - ✅ NAT hacia Internet
    - ✅ Alias de puertos y equipos
    - ✅ Redirección de puertos
- ✅ Configuración DHCP en interfaz LAN
    - ✅ Direccion reservada en DHCP
- ✅ Estados de conexión de pfSense
- ✅ Bitácoras de pfSense

## Verificación de configuracion de red

Realiza la verificación de configuración de red y guarda los resultados en la carpeta `files/pfsense/`, `files/alpine/`, `files/debian/` o `files/centos/` según corresponda

- ✅ Configuración red WAN
    - ✅ Verificar que el cliente alpine tenga la dirección IP del segmento WAN
- ✅ Configuración red LAN
    - ✅ Verificar que el cliente Debian tenga la dirección IP reservada
- ✅ Configuración red DMZ
    - ✅ Verificar que el servidor CentOS tenga la dirección IP estática

## Verificación de servicios de red

Realiza las pruebas de conectividad y guarda los resultados en la carpeta `files/pfsense/`, `files/alpine/`, `files/debian/` o `files/centos/` según corresponda

- ✅ Pruebas de conectividad
    - ✅ Conectividad desde la red LAN hacia DMZ
    - ❌ Conectividad desde la red DMZ hacia LAN
    - ✅ Conectividad de pfSense hacia Internet
    - ✅ Conectividad entre pfSense y el cliente WAN (alpine)
    - ✅ Conectividad del cliente LAN (Debian) hacia Internet
    - ✅ Conectividad del serivdor DMZ (CentOS) hacia Internet
    - ✅ Conectividad del cliente WAN (alpine) al servidor DMZ (CentOS) utilizando la redirección de puertos
        - ✅ Servicio SSH
        - ✅ Servicios HTTP y HTTPS

## Capturas de tráfico de red

<a id="pcap" name="pcap"></a>

Realiza capturas de tráfico de red en formato PCAP de los siguientes servicios y guardarlas en la carpeta `files/pcap/`

| Servicio | Interfaces de red | Notas
|:--------:|:-----------------:|:------|
| ARP      | LAN , WAN , OPT   | Tráfico ARP de todas las redes
| DHCP     | LAN               | Tráfico DHCP de la red LAN
| PING     | LAN , WAN , OPT   | Tráfico ICMP entre las redes LAN y OPT
| PING     | LAN , WAN , OPT   | Petición de los equipos hacia Internet (LAN hacia `1.1.1.1` y DMZ hacia `8.8.8.8`)
| PING     | LAN , WAN , OPT   | Petición de pfSense hacia Internet `9.9.9.9`
| DNS      | LAN , WAN         | Consulta DNS a servidor local `192.168.42.1` y a servidor en Internet `208.67.222.222`
| SSH      | LAN , OPT         | Tráfico SSH del cliente LAN a servidor DMZ
| SSH      | OPT , WAN         | Tráfico SSH del cliente WAN al servidor DMZ via la redirección de puertos
| HTTP     | LAN , OPT         | Petición HTTP  del cliente LAN al servidor DMZ
| HTTP     | OPT , WAN         | Petición HTTP  del cliente WAN al servidor DMZ via la redirección de puertos
| HTTPS    | LAN , OPT         | Petición HTTPS del cliente LAN al servidor DMZ
| HTTPS    | OPT , WAN         | Petición HTTPS del cliente WAN al servidor DMZ via la redirección de puertos

--------------------------------------------------------------------------------

!!! note
    - Regresa a la [descripción de la práctica][siguiente] y elabora tu reporte cuando tengas configurados todos los servicios de red y hayas verificado su correcto funcionamiento

--------------------------------------------------------------------------------

|             ⇦             |         ⇧          |               ⇨               |
|:--------------------------|:------------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba][arriba] | [Página siguiente][siguiente]

[anterior]: ../pfsense-install
[arriba]: ../#procedimiento
[siguiente]: ../#entregables
