#!/bin/bash
set -exuo pipefail

SENDER=admin@APLICACION.redes.tonejito.cf
RECIPIENT=NOMBRE@ciencias.unam.mx
SMTP_SERVER=email-smtp.us-east-2.amazonaws.com:587
SMTP_USER=AKIAXXXXXXXXXXXXXXXX

exec swaks \
  --from ${SENDER} \
  --to ${RECIPIENT} \
  --server ${SMTP_SERVER} \
  -tls --auth PLAIN \
  --auth-user ${SMTP_USER} \
  --header-X-Test "TEST email" \
;
