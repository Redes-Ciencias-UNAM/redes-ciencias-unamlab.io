---
title: Configuración de Postfix con AWS SES para envío de correo
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Configuración de Postfix con AWS SES para envío de correo

## Reconfigurar `debconf`

Reconfigurar el paquete `debconf` para anotar las opciones predeterminadas de configuración de los nuevos paquetes que se instalen en el sistema operativo.

```
root@example:~# dpkg-reconfigure -p low debconf
```

Seleccionar la interfaz `Dialog`

```

 ┌──────────────────────────┤ Configuring debconf ├──────────────────────────┐
 │ Packages that use debconf for configuration share a common look and       │
 │ feel. You can select the type of user interface they use.                 │
 │                                                                           │
 │ The dialog frontend is a full-screen, character based interface, while    │
 │ the readline frontend uses a more traditional plain text interface, and   │
 │ both the gnome and kde frontends are modern X interfaces, fitting the     │
 │ respective desktops (but may be used in any X environment). The editor    │
 │ frontend lets you configure things using your favorite text editor. The   │
 │ noninteractive frontend never asks you any questions.                     │
 │                                                                           │
 │ Interface to use:                                                         │
 │                                                                           │
 │                             Dialog             ↑                          │	⬅	⬅	⬅	Dialog
 │                             Readline           ▮                          │
 │                             Gnome              ↓                          │
 │                                                                           │
 │                                                                           │
 │                    <OK>                        <Cancel>                   │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

En esta sección se explican las diferentes prioridades de configuración para los paquetes del sistema operativo

```

 ┌──────────────────────────┤ Configuring debconf ├──────────────────────────┐
 │                                                                           │
 │ Debconf prioritizes the questions it asks you. Pick the lowest priority   │
 │ of question you want to see:                                              │
 │   - 'critical' only prompts you if the system might break.                │
 │     Pick it if you are a newbie, or in a hurry.                           │
 │   - 'high' is for rather important questions                              │
 │   - 'medium' is for normal questions                                      │
 │   - 'low' is for control freaks who want to see everything                │
 │                                                                           │
 │                                                                           │
 │ Note that no matter what level you pick here, you will be able to see     │
 │ every question if you reconfigure a package with dpkg-reconfigure.        │
 │                                                                           │
 │                                  <OK>                                     │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Cambiar la seleción del **valor predeterminado** `critical` a `low`

!!! note
    Para regresar a la <u>configuración original</u> seguir los pasos y seleccionar el **valor predeterminado** `critical`

```

                ┌───────────┤ Configuring debconf ├────────────┐
                │ Ignore questions with a priority less than:  │
                │                                              │
                │                  critical                    │
                │                  high                        │
                │                  medium                      │
                │                  low                         │	⬅	⬅	⬅	low
                │                                              │
                │                                              │
                │          <OK>              <Cancel>          │	⬅	⬅	⬅	OK
                │                                              │
                └──────────────────────────────────────────────┘

```

## Desinstalar `exim`

Desinstalar `exim` en caso de que se encuentre instalado.

```
# apt purge exim4
```

## Instalar `postfix`

Instalar `postfix` en el equipo

```
# apt install postfix sasl2-bin
```

## Configurar `postfix`

!!! warning
    Es posible realizar de nuevo la configuración de Postfix si existe algún problema ejecutando el siguiente comando

    - `# dpkg-reconfigure -p low postfix`

### Asistente de configuración `debconf`

Configurar como **Internet with smarthost**

```

  ┌────────────────────────┤ Postfix Configuration ├────────────────────────┐
  │                                                                         │
  │ Please select the mail server configuration type that best meets your   │
  │ needs.                                                                  │
  │                                                                         │
  │  No configuration:                                                      │
  │   Should be chosen to leave the current configuration unchanged.        │
  │  Internet site:                                                         │
  │   Mail is sent and received directly using SMTP.                        │
  │  Internet with smarthost:                                               │
  │   Mail is received directly using SMTP or by running a utility such     │
  │   as fetchmail. Outgoing mail is sent using a smarthost.                │
  │  Satellite system:                                                      │
  │   All mail is sent to another machine, called a 'smarthost', for        │
  │ delivery.                                                               │
  │  Local only:                                                            │
  │   The only delivered mail is the mail for local users. There is no      │
  │                                                                         │
  │                                 <OK>                                    │	⬅	⬅	⬅	OK
  │                                                                         │
  └─────────────────────────────────────────────────────────────────────────┘

```

Seleccionar **Internet with smarthost**

```

                    ┌──────┤ Postfix Configuration ├───────┐
                    │ General type of mail configuration:  │
                    │                                      │
                    │       No configuration               │
                    │       Internet Site                  │
                    │       Internet with smarthost        │	⬅	⬅	⬅
                    │       Satellite system               │
                    │       Local only                     │
                    │                                      │
                    │                                      │
                    │       <OK>           <Cancel>        │	⬅	⬅	⬅	OK
                    │                                      │
                    └──────────────────────────────────────┘

```

Escribir el nombre de host:

- `APLICACION.redes.tonejito.cf`

!!! note
    - Reemplazar `APLICACION` por el nombre de tu aplicación (`wordpress`, `redmine`, etc.)
    - Se utilizará este nombre de dominio para los correos que **salgan** de tu servidor
    - El nombre de dominio `redes.tonejito.cf` ya fue validado correctamente en AWS SES
    - Esto permite que se utilicen subdominios y cualquier dirección bajo estos como remitente de los correos que envíe el servidor

```

 ┌─────────────────────────┤ Postfix Configuration ├─────────────────────────┐
 │ The "mail name" is the domain name used to "qualify" _ALL_ mail           │
 │ addresses without a domain name. This includes mail to and from <root>:   │
 │ please do not make your machine send out mail from root@example.org       │
 │ unless root@example.org has told you to.                                  │
 │                                                                           │
 │ This name will also be used by other programs. It should be the single,   │
 │ fully qualified domain name (FQDN).                                       │
 │                                                                           │
 │ Thus, if a mail address on the local host is foo@example.org, the         │
 │ correct value for this option would be example.org.                       │
 │                                                                           │
 │ System mail name:                                                         │
 │                                                                           │
 │ APLICACION.redes.tonejito.cf ____________________________________________ │	⬅	⬅	⬅
 │                                                                           │
 │                    <OK>                        <Cancel>                   │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Especificar el host del relay de correo de AWS SES

!!! note
    - Estos datos se proveerán al equipo

```

 ┌─────────────────────────┤ Postfix Configuration ├─────────────────────────┐
 │ Please specify a domain, host, host:port, [address] or [address]:port.    │
 │ Use the form [destination] to turn off MX lookups. Leave this blank for   │
 │ no relay host.                                                            │
 │                                                                           │
 │ Do not specify more than one host.                                        │
 │                                                                           │
 │ The relayhost parameter specifies the default host to send mail to when   │
 │ no entry is matched in the optional transport(5) table. When no relay     │
 │ host is given, mail is routed directly to the destination.                │
 │                                                                           │
 │ SMTP relay host (blank for none):                                         │
 │                                                                           │
 │ [email-smtp.us-east-2.amazonaws.com]:587 ________________________________ │	⬅	⬅	⬅
 │                                                                           │
 │                    <OK>                        <Cancel>                   │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Seleccionar un destinatario para los correos que se envían a `postmaster` y `root`

!!! note
    - Se recomienda que sea una cuenta local, como el usuario `redes`

```

 ┌─────────────────────────┤ Postfix Configuration ├─────────────────────────┐
 │ Mail for the 'postmaster', 'root', and other system accounts needs to be  │
 │ redirected to the user account of the actual system administrator.        │
 │                                                                           │
 │ If this value is left empty, such mail will be saved in                   │
 │ /var/mail/nobody, which is not recommended.                               │
 │                                                                           │
 │ Mail is not delivered to external delivery agents as root.                │
 │                                                                           │
 │ If you already have a /etc/aliases file and it does not have an entry     │
 │ for root, then you should add this entry.  Leave this blank to not add    │
 │ one.                                                                      │
 │                                                                           │
 │ Root and postmaster mail recipient:                                       │
 │                                                                           │
 │ redes ___________________________________________________________________ │	⬅	⬅	⬅
 │                                                                           │
 │                    <OK>                        <Cancel>                   │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Especificar el destino de los correos recibidos por este equipo.

!!! warning
    - Reemplazar `APLICACION` por el nombre de tu aplicación (`wordpress`, `redmine`, etc.)
    - Reemplazar `example.com` con tu nombre de dominio

```

  ┌────────────────────────┤ Postfix Configuration ├─────────────────────────┐
  │ Please give a comma-separated list of domains for which this machine     │
  │ should consider itself the final destination. If this is a mail domain   │
  │ gateway, you probably want to include the top-level domain.              │
  │                                                                          │
  │ Other destinations to accept mail for (blank for none):                  │
  │                                                                          │
  │ APLICACION.redes.tonejito.cf, example.com, , localhost _________________ │	⬅	⬅	⬅
  │                                                                          │
  │                   <OK>                       <Cancel>                    │	⬅	⬅	⬅	OK
  │                                                                          │
  └──────────────────────────────────────────────────────────────────────────┘

```

Seleccionar la opción `No`

```

 ┌─────────────────────────┤ Postfix Configuration ├─────────────────────────┐
 │                                                                           │
 │ If synchronous updates are forced, then mail is processed more slowly.    │
 │ If not forced, then there is a remote chance of losing some mail if the   │
 │ system crashes at an inopportune time, and you are not using a journaled  │
 │ filesystem (such as ext3).                                                │
 │                                                                           │
 │ Force synchronous updates on mail queue?                                  │
 │                                                                           │
 │                    <Yes>                       <No>                       │	⬅	⬅	⬅	No
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Dejar la configuración predeterminada y dar clic en la opción `OK`

```

 ┌─────────────────────────┤ Postfix Configuration ├─────────────────────────┐
 │ Please specify the network blocks for which this host should relay mail.  │
 │ The default is just the local host, which is needed by some mail user     │
 │ agents. The default includes local host for both IPv4 and IPv6. If just   │
 │ connecting via one IP version, the unused value(s) may be removed.        │
 │                                                                           │
 │ If this host is a smarthost for a block of machines, you need to specify  │
 │ the netblocks here, or mail will be rejected rather than relayed.         │
 │                                                                           │
 │ To use the postfix default (which is based on the connected subnets),     │
 │ leave this blank.                                                         │
 │                                                                           │
 │ Local networks:                                                           │
 │                                                                           │
 │ 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 ____________________________ │	⬅	⬅	⬅
 │                                                                           │
 │                    <OK>                        <Cancel>                   │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Seleccionar la opción `Yes`

```

  ┌────────────────────────┤ Postfix Configuration ├────────────────────────┐
  │                                                                         │
  │ Please choose whether you want to use procmail to deliver local mail.   │
  │                                                                         │
  │ Note that if you use procmail to deliver mail system-wide, you should   │
  │ set up an alias that forwards mail for root to a real user.             │
  │                                                                         │
  │ Use procmail for local delivery?                                        │
  │                                                                         │
  │                    <Yes>                       <No>                     │	⬅	⬅	⬅	Yes
  │                                                                         │
  └─────────────────────────────────────────────────────────────────────────┘

```

Dejar la configuración predeterminada y dar clic en la opción `OK`

```

 ┌─────────────────────────┤ Postfix Configuration ├─────────────────────────┐
 │ Please specify the limit that Postfix should place on mailbox files to    │
 │ prevent runaway software errors. A value of zero (0) means no limit. The  │
 │ upstream default is 51200000.                                             │
 │                                                                           │
 │ Mailbox size limit (bytes):                                               │
 │                                                                           │
 │ 0 _______________________________________________________________________ │	⬅	⬅	⬅	0
 │                                                                           │
 │                    <OK>                        <Cancel>                   │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Dejar la configuración predeterminada y dar clic en la opción `OK`

```

 ┌─────────────────────────┤ Postfix Configuration ├─────────────────────────┐
 │ Please choose the character that will be used to define a local address   │
 │ extension.                                                                │
 │                                                                           │
 │ To not use address extensions, leave the string blank.                    │
 │                                                                           │
 │ Local address extension character:                                        │
 │                                                                           │
 │ + _______________________________________________________________________ │	⬅	⬅	⬅	+
 │                                                                           │
 │                    <OK>                        <Cancel>                   │	⬅	⬅	⬅	OK
 │                                                                           │
 └───────────────────────────────────────────────────────────────────────────┘

```

Seleccionar la opción `all` y dar clic en la opción `OK`

```

  ┌────────────────────────┤ Postfix Configuration ├─────────────────────────┐
  │ By default, whichever Internet protocols are enabled on the system at    │
  │ installation time will be used. You may override this default with any   │
  │ of the following:                                                        │
  │                                                                          │
  │  all : use both IPv4 and IPv6 addresses;                                 │
  │  ipv6: listen only on IPv6 addresses;                                    │
  │  ipv4: listen only on IPv4 addresses.                                    │
  │                                                                          │
  │ Internet protocols to use:                                               │
  │                                                                          │
  │                                  all                                     │	⬅	⬅	⬅	all
  │                                  ipv6                                    │
  │                                  ipv4                                    │
  │                                                                          │
  │                                                                          │
  │                   <OK>                       <Cancel>                    │	⬅	⬅	⬅	OK
  │                                                                          │
  └──────────────────────────────────────────────────────────────────────────┘

```

### Editar archivos de configuración

#### `/etc/mailname`

Editar el archivo `/etc/mailname` y anotar el nombre de dominio para los correos que se originen en este servidor.

```
APLICACION.redes.tonejito.cf
```

!!! note
    - Reemplazar `APLICACION` por el nombre de tu aplicación (`wordpress`, `redmine`, etc.)
    - Se utilizará este nombre de dominio para los correos que **salgan** de tu servidor
    - El nombre de dominio `redes.tonejito.cf` ya fue validado correctamente en AWS SES
    - Esto permite que se utilicen subdominios y cualquier dirección bajo estos como remitente de los correos que envíe el servidor

#### `/etc/postfix/main.cf`

Editar el archivo `/etc/postfix/main.cf` y anotar las siguientes opciones.

???+ details "Ejemplo de archivo de configuración de Postfix"
    - [`/etc/postfix/main.cf`](files/postfix-main-cf.txt)

```
	...
smtpd_banner = $myhostname ESMTP
	...
# Use AWS SES as relayhost
# https://docs.aws.amazon.com/ses/latest/DeveloperGuide/postfix.html
smtp_sasl_auth_enable = yes
smtp_sasl_security_options = noanonymous
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_use_tls = yes
smtp_tls_security_level = encrypt
smtp_tls_note_starttls_offer = yes
# smtp_tls_CApath=/etc/ssl/certs
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
	...
myhostname = APLICACION.redes.tonejito.cf
	...
myorigin = /etc/mailname
	...
mydestination = $myhostname, APLICACION.redes.tonejito.cf, example.com, , localhost.localdomain, localhost
	...
relayhost = [email-smtp.us-east-2.amazonaws.com]:587
	...
inet_interfaces = loopback-only
inet_protocols = all
	...
```

Editar el archivo `/etc/postfix/sasl_passwd` y agregar los siguientes datos:

| Elemento                        | Valor                                         |
|--------------------------------:|:----------------------------------------------|
| Dirección y puerto del servidor | `[email-smtp.us-east-2.amazonaws.com]:587`
| Usuario SMTP                    | `AKIAXXXXXXXXXXXXXXXX`
| Contraseña SMTP                 | `ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ`

!!! danger
    Las credenciales de SMTP se proporcionarán por un mecanismo alterno

```
# [host]:port	username:password
[email-smtp.us-east-2.amazonaws.com]:587	AKIAXXXXXXXXXXXXXXXX:ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
```

Procesa el archivo `sasl_passwd` para pbtener su contraparte binaria `sasl_passwd.db`

```
# postmap hash:/etc/postfix/sasl_passwd
```

Ajusta el dueño y grupo de los archivos `sasl_passwd*`

```
# chown -c root:staff /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
changed ownership of '/etc/postfix/sasl_passwd'    from root:root to root:staff
changed ownership of '/etc/postfix/sasl_passwd.db' from root:root to root:staff
```

Ajusta los permisos de los archivos `sasl_passwd*`

```
# chmod -c 0600 /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
mode of '/etc/postfix/sasl_passwd'    changed from 0644 (rw-r--r--) to 0600 (rw-------)
mode of '/etc/postfix/sasl_passwd.db' changed from 0644 (rw-r--r--) to 0600 (rw-------)
```

Lista los archivos `sasl_passwd*` para verificar el dueño, grupo y permisos

```
# ls -la /etc/postfix/sasl_passwd /etc/postfix/sasl_passwd.db
-rw------- 1 root staff   140 Nov 30 01:20 /etc/postfix/sasl_passwd
-rw------- 1 root staff 12288 Nov 30 01:30 /etc/postfix/sasl_passwd.db
```

Reinicia el servicio `postfix` para aplicar los cambios

```
root@example:~# systemctl restart postfix

root@tonejito:~# systemctl status postfix
● postfix.service - Postfix Mail Transport Agent
     Loaded: loaded (/lib/systemd/system/postfix.service; enabled; vendor preset: enabled)
     Active: active (exited) since Wed 2022-11-30 01:16:37 CST; 1min 2s ago
    Process: 902026 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 902026 (code=exited, status=0/SUCCESS)
        CPU: 1ms

Nov 30 02:16:37 example.com systemd[1]: Starting Postfix Mail Transport Agent...
Nov 30 02:16:37 example.com systemd[1]: Finished Postfix Mail Transport Agent.
```

Verifica que Postfix se encuentre en ejecución y escuchando en el puerto 25 de la interfaz loopback

!!! warning
    - Es muy importante que Postfix únicamente escuche en `localhost` (`127.0.0.1` y/o `::1`) porque este equipo únicamente va a **enviar** correo y no se va a configurar la recepción de correo electrónico

```
root@example:~# ps ax | egrep -i 'postfix|master'
 899176 ?        Ss     0:00 /usr/lib/postfix/sbin/master -w

root@example:~# netstat -ntulp | egrep '^Proto|master'
Proto Recv-Q Send-Q  Local Address   Foreign Address   State    PID/Program name
tcp        0      0  127.0.0.1:25    0.0.0.0:*         LISTEN   899176/master
tcp6       0      0  ::1:25          :::*              LISTEN   899176/master
```

## Pruebas de envío de correo electrónico

### Enviar correo electrónico de prueba desde la línea de comandos

Abre otra terminal y revisa la bitácora del servicio de correo electrónico

```
redes@example:~$ tail -n 0 -f /var/log/mail.log

	...
```

Regresa a la terminal principal e instala el _software_ para probar el envío de correo electrónico

```
root@example:~# apt -qqy install bsd-mailx
```

Envía un mensaje de prueba a tu dirección de correo `@ciencias`

- Identifica el _queue-id_ de tu mensaje de correo electrónico (`2C2AD602EC` en este caso)

```
root@tonejito:~# echo "TEST" | mail -s 'Prueba' -- andres.hernandez@ciencias.unam.mx ; mailq ;
-Queue ID-  --Size-- ----Arrival Time---- -Sender/Recipient-------
2C2AD602EC*     427 Wed Nov 30 02:16:56  root@APLICACION.redes.tonejito.cf
                                         andres.hernandez@ciencias.unam.mx

-- 0 Kbytes in 1 Request.
```

Este ID de cola (_queue-id_) permite ubicar los mensaje de bitácora relacionados con el mensaje en cuestión.

```
Nov 30 02:16:56 tonejito postfix/pickup[902024]: 2C2AD602EC: uid=0 from=<root>
Nov 30 02:16:56 tonejito postfix/cleanup[902047]: 2C2AD602EC: message-id=<20221130081656.2C2AD602EC@APLICACION.redes.tonejito.cf>
Nov 30 02:16:56 tonejito postfix/qmgr[902025]: 2C2AD602EC: from=<root@APLICACION.redes.tonejito.cf>, size=427, nrcpt=1 (queue active)
Nov 30 02:16:57 tonejito postfix/smtp[902051]: 2C2AD602EC: to=<andres.hernandez@ciencias.unam.mx>, relay=email-smtp.us-east-2.amazonaws.com[18.217.139.135]:587, delay=1, delays=0.03/0.06/0.53/0.4, dsn=2.0.0, status=sent (250 OK 010f0184c79c408b-4afd24b6-78df-47f3-b0a6-f387b63e04a5-000000)
Nov 30 02:16:57 tonejito postfix/qmgr[902025]: 2C2AD602EC: removed
```

Si la bitácora muestra que AWS SES aceptó el mensaje (`status=sent 250 OK`), entonces revisa tu buzón de correo para ver si te llegó el mensaje.

| [Correo recibido](files/correo.eml)
|:--------:|
| ![](img/correo-recibido.png)
| ![](img/correo-recibido-id.png)
| ![](img/correo-recibido-metadata.png)

### Prueba de envío de correo con `swaks`

Puedes realizar una prueba de envío con `swaks` [como viene descrito en la página de SMTP][smtp-test-swaks] para verificar que tus credenciales son correctas

- Puedes probar con el script [`swaks-test.sh`](files/swaks-test.sh)

```
% ./swaks-test.sh

exec swaks \
  --from root@TEST.redes.tonejito.cf \
  --to andres.hernandez@ciencias.unam.mx \
  --server email-smtp.us-east-2.amazonaws.com:587 \
  -tls --auth PLAIN \
  --auth-user AKIAXXXXXXXXXXXXXXXX \
  --header-X-Test "TEST email" \
;
Password: ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ
=== Trying email-smtp.us-east-2.amazonaws.com:587...
=== Connected to email-smtp.us-east-2.amazonaws.com.
<-  220 email-smtp.amazonaws.com ESMTP SimpleEmailService-d-16HSGQV1K DmY1yqN8DRlisdWKObIL
 -> EHLO vf-0s.local
<-  250-email-smtp.amazonaws.com
<-  250-8BITMIME
<-  250-STARTTLS
<-  250-AUTH PLAIN LOGIN
<-  250 Ok
 -> STARTTLS
<-  220 Ready to start TLS
=== TLS started with cipher TLSv1.2:ECDHE-RSA-AES256-GCM-SHA384:256
=== TLS no local certificate set
=== TLS peer DN="/CN=email-smtp.us-east-2.amazonaws.com"
 ~> EHLO vf-0s.local
<~  250-email-smtp.amazonaws.com
<~  250-8BITMIME
<~  250-STARTTLS
<~  250-AUTH PLAIN LOGIN
<~  250 Ok
 ~> AUTH PLAIN XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
<~  235 Authentication successful.
 ~> MAIL FROM:<root@TEST.redes.tonejito.cf>
<~  250 Ok
 ~> RCPT TO:<andres.hernandez@ciencias.unam.mx>
<~  250 Ok
```

| [Correo recibido](files/swaks.eml)
|:--------:|
| ![](img/swaks-mail.png)
| ![](img/swaks-mail-id.png)
| ![](img/swaks-mail-metadata.png)

--------------------------------------------------------------------------------

## Mensajes de error en Postfix


<!--

```
root@example:~# echo "Prueba" | mail -s 'TEST' -- andres.hernandez@ciencias.unam.mx
```

```
root@example:~# mailq
-Queue ID-  --Size-- ----Arrival Time---- -Sender/Recipient-------
AB42E60251*     394 Wed Nov 30 01:48:43  root@example.com
                                         andres.hernandez@ciencias.unam.mx

-- 0 Kbytes in 1 Request.
```
-->

Este tipo de mensajes de bitácora indica que hay un error con el remitente o destinatario porque la identidad (dirección de correo o nombre de dominio) no está verificado con AWS SES.

- `dsn=5.0.0, status=bounced (host email-smtp.us-west-2.amazonaws.com[52.39.182.212] said: 554 Message rejected: Email address is not verified. The following identities failed the check in region US-WEST-2:`

El servicio de correo de AWS SES envía un mensaje de correo al administrador `root` para informar la situación:

- Correo hacia `root`, recibido por el usuario `admin`
    - `sender non-delivery notification`

```
admin@tonejito:~$ tail -n 0 -f /var/log/mail.log

	...

Nov 30 01:52:05 tonejito postfix/pickup[899943]: 4B547602EC: uid=0 from=<root>
Nov 30 01:52:05 tonejito postfix/cleanup[899970]: 4B547602EC: message-id=<20221130075205.4B547602EC@example.com>
Nov 30 01:52:05 tonejito postfix/qmgr[899944]: 4B547602EC: from=<root@example.com>, size=394, nrcpt=1 (queue active)
Nov 30 01:52:05 tonejito postfix/smtp[899972]: 4B547602EC: to=<andres.hernandez@ciencias.unam.mx>, relay=email-smtp.us-west-2.amazonaws.com[52.39.182.212]:587, delay=0.35, delays=0.02/0.03/0.16/0.15, dsn=5.0.0, status=bounced (host email-smtp.us-west-2.amazonaws.com[52.39.182.212] said: 554 Message rejected: Email address is not verified. The following identities failed the check in region US-WEST-2: root@example.com, root <root@example.com>, andres.hernandez@ciencias.unam.mx (in reply to end of DATA command))
Nov 30 01:52:05 tonejito postfix/cleanup[899970]: A12BA6084D: message-id=<20221130075205.A12BA6084D@example.com>
Nov 30 01:52:05 tonejito postfix/bounce[899974]: 4B547602EC: sender non-delivery notification: A12BA6084D
Nov 30 01:52:05 tonejito postfix/qmgr[899944]: A12BA6084D: from=<>, size=2763, nrcpt=1 (queue active)
Nov 30 01:52:05 tonejito postfix/qmgr[899944]: 4B547602EC: removed
Nov 30 01:52:05 tonejito postfix/local[899975]: A12BA6084D: to=<admin@example.com>, orig_to=<root@example.com>, relay=local, delay=0.08, delays=0/0.01/0/0.07, dsn=2.0.0, status=sent (delivered to command: procmail -a "$EXTENSION")
Nov 30 01:52:05 tonejito postfix/qmgr[899944]: A12BA6084D: removed
```

<!--
```
root@example:~# echo "Prueba" | mail -s 'TEST' -- andres.hernandez@ciencias.unam.mx ; mailq ;
-Queue ID-  --Size-- ----Arrival Time---- -Sender/Recipient-------
0D87A602EC*     412 Wed Nov 30 01:56:43  root@redes.tonejito.cf
                                         andres.hernandez@ciencias.unam.mx

-- 0 Kbytes in 1 Request.
```

```
Nov 30 01:56:43 tonejito postfix/pickup[900422]: 0D87A602EC: uid=0 from=<root>
Nov 30 01:56:43 tonejito postfix/cleanup[900436]: 0D87A602EC: message-id=<20221130075643.0D87A602EC@redes.tonejito.cf>
Nov 30 01:56:43 tonejito postfix/qmgr[900423]: 0D87A602EC: from=<root@redes.tonejito.cf>, size=412, nrcpt=1 (queue active)
Nov 30 01:56:43 tonejito postfix/smtp[900440]: 0D87A602EC: to=<andres.hernandez@ciencias.unam.mx>, relay=email-smtp.us-west-2.amazonaws.com[52.11.44.63]:587, delay=0.41, delays=0.02/0.07/0.16/0.17, dsn=5.0.0, status=bounced (host email-smtp.us-west-2.amazonaws.com[52.11.44.63] said: 554 Message rejected: Email address is not verified. The following identities failed the check in region US-WEST-2: root <root@redes.tonejito.cf>, root@redes.tonejito.cf, andres.hernandez@ciencias.unam.mx (in reply to end of DATA command))
Nov 30 01:56:43 tonejito postfix/cleanup[900436]: 71E656084D: message-id=<20221130075643.71E656084D@redes.tonejito.cf>
Nov 30 01:56:43 tonejito postfix/bounce[900442]: 0D87A602EC: sender non-delivery notification: 71E656084D
Nov 30 01:56:43 tonejito postfix/qmgr[900423]: 71E656084D: from=<>, size=2881, nrcpt=1 (queue active)
Nov 30 01:56:43 tonejito postfix/qmgr[900423]: 0D87A602EC: removed
Nov 30 01:56:43 tonejito postfix/local[900443]: 71E656084D: to=<admin@redes.tonejito.cf>, orig_to=<root@redes.tonejito.cf>, relay=local, delay=0.12, delays=0/0.11/0/0.01, dsn=2.0.0, status=sent (delivered to command: procmail -a "$EXTENSION")
Nov 30 01:56:43 tonejito postfix/qmgr[900423]: 71E656084D: removed
```

-->

<!--

```
root@example:~# echo "Prueba" | mail -s 'TEST' -- andres.hernandez@ciencias.unam.mx ; mailq ;
-Queue ID-  --Size-- ----Arrival Time---- -Sender/Recipient-------
C227B602EC*     412 Wed Nov 30 02:08:29  root@redes.tonejito.cf
                                         andres.hernandez@ciencias.unam.mx

-- 0 Kbytes in 1 Request.
```

```
admin@example:~$ tail -n 0 -f /var/log/mail.log

	...

Nov 30 02:08:29 tonejito postfix/pickup[901298]: C227B602EC: uid=0 from=<root>
Nov 30 02:08:29 tonejito postfix/cleanup[901312]: C227B602EC: message-id=<20221130080829.C227B602EC@redes.tonejito.cf>
Nov 30 02:08:29 tonejito postfix/qmgr[901299]: C227B602EC: from=<root@redes.tonejito.cf>, size=412, nrcpt=1 (queue active)
Nov 30 02:08:31 tonejito postfix/smtp[901316]: C227B602EC: to=<andres.hernandez@ciencias.unam.mx>, relay=email-smtp.us-east-2.amazonaws.com[3.141.205.72]:587, delay=1.4, delays=0.02/0.31/0.59/0.46, dsn=2.0.0, status=sent (250 OK 010f0184c79487bd-cb175527-0a36-4338-9c6c-96adc49dce7e-000000)
Nov 30 02:08:31 tonejito postfix/qmgr[901299]: C227B602EC: removed
```
-->

--------------------------------------------------------------------------------

[smtp-test-swaks]: ../../../temas/smtp/#prueba-de-smtp-con-autenticacion

[aws-ses-postfix]: https://docs.aws.amazon.com/ses/latest/dg/postfix.html
