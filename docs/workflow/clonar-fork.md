---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Clonar fork en la máquina local
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Clonar _fork_ en la máquina local

## Clona el repositorio

- Accede a la URL del repositorio `tareas-redes` asociado a **tu cuenta de usuario**

```text
https://gitlab.com/USUARIO/tareas-redes.git
```

- Obten la URL de tu repositorio `tareas-redes`

| ![](img/008-Fork_successful-clone_URL.png)
|:------------------------------------------:|
| Da clic en el botón **clone** y copia la URL de `HTTPS` para clonar el repositorio

- Clona el repositorio en tu equipo con el comando `git clone`

```console
$ git clone https://gitlab.com/USUARIO/tareas-redes.git
Cloning into 'tareas-redes'...
remote: Enumerating objects: 46, done.
remote: Counting objects: 100% (46/46), done.
remote: Compressing objects: 100% (43/43), done.
remote: Total 46 (delta 2), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (46/46), 3.69 MiB | 1.0 MiB/s, done.
Resolving deltas: 100% (2/2), done.
```

- Lista el contenido del repositorio

```console
$ cd tareas-redes/
$ ls -lA
total 40
drwxr-xr-x 12 tonejito staff 384 Sep 28 01:02 .git
-rw-r--r--  1 tonejito staff 760 Sep 28 01:02 .gitignore
-rw-r--r--  1 tonejito staff 962 Sep 28 01:02 .gitlab-ci.yml
-rw-r--r--  1 tonejito staff 263 Sep 28 01:02 Makefile
lrwxr-xr-x  1 tonejito staff  14 Sep 28 01:02 README.md -> docs/README.md
drwxr-xr-x  5 tonejito staff 160 Sep 28 01:02 docs
-rw-r--r--  1 tonejito staff 954 Sep 28 01:02 mkdocs.yml
-rw-r--r--  1 tonejito staff 340 Sep 28 01:02 requirements.txt
```

- Lista el contenido de la carpeta `docs` utilizando el comando `tree` o utiliza el explorador de archivos para ver la estructura de carpetas

!!! warning
    Probablemente necesites instalar el programa `tree` en tu equipo

```
$ tree -a docs/
docs/
├── README.md -> ../README.md
├── tareas/
│   └── tarea-0
│       ├── README.md
│       └── ...
├── practicas/
│   └── practica-1
│       ├── README.md
│       └── ...
└── workflow/
    ├── README.md
    └── img/
        ├── .gitkeep
        ├── 000-workflow.png
        └── ...
```

--------------------------------------------------------------------------------

## Crear rama de trabajo

- Revisa que de manera inicial te encuentres en la rama `entregas`

```console
$ git branch
  entregados
* entregas
  main
```

!!! note
    Es posible que únicamente te aparezca la rama predeterminada llamada `entregas`

- Cámbiate a la rama `tarea-1` utilizando `git checkout`

```console
$ git checkout tarea-1
Switched to branch 'tarea-1'
```

!!! warning
    Para las demás actividades crea una rama que se llame como la actividad (`tarea-2`, `practica-1`, etc.)

    ```
    $ git checkout -b 'practica-1'
    ```

    No utilices **espacios**, **acentos**, **eñe**, **diéresis** o **caracteres especiales**

- Revisa que hayas cambiado a la rama
    - Debe tener el prefijo `*`

```console
$ git branch
* tarea-1
  entregados
  entregas
  main
```

!!! note
    Es posible que no se muestren todas las ramas (entregados y entregas)

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando ya tengas tu repositorio clonado y te hayas cambiado a la rama de trabajo.

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../crear-fork
[arriba]: ../../workflow
[siguiente]: ../configurar-precommit
