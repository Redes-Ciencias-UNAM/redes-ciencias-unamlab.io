---
title: Configuración de redes en VirtualBox
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Configuración de redes en VirtualBox

## Diagrama de red

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-----------------------------:|
| ![](img/diagrama_red.png)
| 🟠 <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵 <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢 <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet

Esta es la configuración de red que necesitas en esta práctica

| Red                                     | Tipo             | Nombre        | Dirección IP / Máscara | Servidor DHCP de VirtualBox
|:---------------------------------------:|:----------------:|:-------------:|:----------------------:|:-------------|
| <span class="green">**Red WAN**</span>  | Nat Network      | `NAT-Network` | `10.0.2.0/24`          | ☑️ **Habilitado** (_predeterminado_)
| <span class="blue">**Red LAN**</span>   | Host-only        | `vboxnet0`    | `192.168.42.0/24`      | 🔲 Deshabilitado
| <span class="orange">**Red DMZ**</span> | Internal network | `intnet`      | `172.16.1.0/24`        | _No soportado por VirtualBox_

--------------------------------------------------------------------------------

## 🟢 <span class="green">**Red WAN**</span> - NAT-Network

<!-- VirtualBox 6.x -->
<!--

Crea la red NAT-Network de VirtualBox para simular la red **WAN**

- Abre las preferencias de VirtualBox

| Menú principal
|:----------------------:|
| ![](img/virtualbox/virtualbox-menu-preferences.png)

- Ve a la pestaña de configuración de redes NAT

| Configuración de redes NAT
|:----------------------:|
| ![](img/virtualbox/virtualbox-nat-networks.png)

- Agrega una red NAT con el CIDR `10.0.2.0/24` y asegúrate de que el DHCP se encuentre activo

| Agregar red NAT
|:----------------------:|
| ![](img/virtualbox/virtualbox-nat-network-settings.png)
-->

<!-- VirtualBox 7.x -->

Crea la red NAT-Network de VirtualBox para simular la <span class="green">**red WAN**</span>

- Abre el **menú de herramientas** de VirtualBox y da clic en **Network**

| Menú de herramientas
|:----------------------:|
| ![](img/virtualbox/virtualbox-menu-tools.png)

- Selecciona la pestaña **NAT Networks** y da clic en el botón **Create ➕** para crear una nueva red NAT

| Crear red NAT
|:----:|
| ![](img/virtualbox/virtualbox-nat-network-create-1.png)

- Da clic en el botón **Properties ⚙️**, configura la red con los siguientes parámetros:

| Red                                | Nombre       | Prefijo IPv4  | Servidor DHCP
|:----------------------------------:|:------------:|:-------------:|:--------------------:|
| <span class="green">**WAN**</span> | `NatNetwork` | `10.0.2.0/24` | `☑️` **Habilitado**

| Parámetros de la red NAT
|:----:|
| ![](img/virtualbox/virtualbox-nat-network-create-2.png)

- Da clic en el botón **Apply ✔️** para guardar los cambios

- Lista las redes NAT que tienes configuradas en VirtualBox

```console
tonejito@laptop:~$ vboxmanage list natnets	⬅️
Name:         NatNetwork	✅
Network:      10.0.2.0/24	✅
Gateway:      10.0.2.1		✅
DHCP Server:  Yes			✅
IPv6:         No
IPv6 Prefix:  fd17:625c:f037:2::/64
IPv6 Default: No
```

--------------------------------------------------------------------------------

## 🔵 <span class="blue">**Red LAN**</span> - host-only

Permite que VirtualBox configure los segmentos adecuados en las redes host-only.

- Crea el directorio `/etc/vbox` en caso de que no exista

```
tonejito@laptop ~ % test -d /etc/vbox || mkdir -v /etc/vbox
mkdir: Created directory '/etc/vbox'
```

- Edita el archivo `/etc/vbox/networks.conf` con el siguiente contenido para habilitar los rangos de direcciones IP no homologadas del RFC 1918 en las redes _host-only_ de VirtualBox:

```
* 10.0.0.0/8 172.16.0.0/12 192.168.0.0/16
* 2001::/64
```

- Reinicia tu máquina física para aplicar la configuración de las redes de VirtualBox

```
tonejito@laptop ~ % reboot
```

<!-- VirtualBox 6.x -->
<!--

- Abre la consola de VirtualBox y lanza el administrador de redes

| Menú principal
|:----------------------:|
| ![](img/virtualbox/virtualbox-menu-host-network-manager.png)

- Configura las redes sólo anfitrión (_host-only_) para que queden de la siguiente manera

| Red     | Nombre     | Dirección IP / Máscara | Servidor DHCP |
|:-------:|:----------:|:----------------------:|:-------------:|
| **LAN** | `vboxnet0` | `192.168.56.1/24`      | Deshabilitado
| **DMZ** | `vboxnet1` | `172.16.1.10/24`       | Deshabilitado

| Administrador de redes _host-only_
|:----------------------:|
| ![](img/virtualbox/virtualbox-host-network-manager-config.png)
-->

<!-- VirtualBox 7.x -->

Crea la red _host-only_ de VirtualBox para simular la <span class="blue">**red LAN**</span>

- Abre el **menú de herramientas** de VirtualBox y da clic en **Network**

| Menú de herramientas
|:----------------------:|
| ![](img/virtualbox/virtualbox-menu-tools.png)

- Selecciona la pestaña **Host-Only Networks** y da clic en el botón **Create ➕** para crear una nueva red _host-only_

| Crear red _host-only_
|:----:|
| ![](img/virtualbox/virtualbox-hostonly-network-create-1.png)

- Da clic en el botón **Properties ⚙️**, configura la red con los siguientes parámetros:

| Red                                   | Nombre     | Dirección IP   | Máscara de subred | Servidor DHCP
|:-------------------------------------:|:----------:|:--------------:|:-----------------:|:-----------------------:|
| <span class="blue">**Red LAN**</span> | `vboxnet0` | `192.168.42.1` | `255.255.255.0`   | `🔲` **Deshabilitado**

| Parámetros de la red _host-only_
|:----:|
| ![](img/virtualbox/virtualbox-hostonly-network-create-2.png)
| ![](img/virtualbox/virtualbox-hostonly-network-create-3.png)

- Da clic en el botón **Apply ✔️** para guardar los cambios

- Lista las redes _host-only_ que tienes configuradas en VirtualBox

```console
tonejito@laptop:~$ vboxmanage list hostonlyifs	⬅️
Name:            vboxnet0		✅
GUID:            786f6276-656e-4074-8000-0a0027000000
DHCP:            Disabled		✅
IPAddress:       192.168.42.1	✅
NetworkMask:     255.255.255.0	✅
IPV6Address:     fe80::800:27ff:fe00:0
IPV6NetworkMaskPrefixLength: 64
HardwareAddress: 0a:00:27:00:00:00
MediumType:      Ethernet
Wireless:        No
Status:          Up				✅
VBoxNetworkName: HostInterfaceNetworking-vboxnet0
```

- Lista la configuración del adaptador de red `vboxnet0` y verifica que tenga la dirección IP `192.168.42.1`

```
tonejito@laptop ~ % ip addr show dev vboxnet0	⬅️
5: vboxnet0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 0a:00:27:00:00:00 brd ff:ff:ff:ff:ff:ff
    inet 192.168.42.1/24 brd 192.168.42.255 scope global vboxnet0	✅
       valid_lft forever preferred_lft forever
    inet6 fe80::800:27ff:fe00:0/64 scope link
       valid_lft forever preferred_lft forever

tonejito@laptop ~ % ip route show dev vboxnet0
192.168.42.0/24 dev vboxnet0 proto kernel scope link src 192.168.42.1	✅
```

--------------------------------------------------------------------------------

## 🟠 <span class="orange">**Red DMZ**</span> - internal-network

!!! warning
    - La red **Internal Network** `intnet` se crea cuando se asigna a alguna máquina virtual
    - La máquina física <u>no tiene conexión</u> a la red **Internal Network** `intnet`

| VirtualBox - Configuración red `intnet`
|:-----------------------------:|
| ![](img/centos/centos-settings-eth1-intnet.png)

- Lista las redes _internal network_ que tienes configuradas en VirtualBox

```console
tonejito@laptop:~$ vboxmanage list intnets	⬅️
Name:        intnet	✅
```

<!--
https://www.virtualbox.org/manual/ch06.html#network_nat_service
https://www.virtualbox.org/manual/ch06.html#network_hostonly
https://www.virtualbox.org/manual/ch06.html#network_internal
https://www.virtualbox.org/manual/ch06.html#network-manager
https://www.virtualbox.org/manual/ch08.html#vboxmanage-dhcpserver
-->

--------------------------------------------------------------------------------

## Interfaces de red en la máquina física

<!-- FIXME -->
Después de completar la configuración de las redes de VirtualBox, la máquina física tendrá dos adaptadores de red.
Cada adaptador de red está <u>directamente conectado</u> a las redes <span class="blue">**LAN**</span> y <span class="orange">**DMZ**</span>, por lo que la máquina física es capaz de comunicarse con los clientes de estas redes sin necesidad de ruteo

Después de completar la configuración de las redes de VirtualBox, la máquina física tendrá lo siguiente:

- Una red NAT Network que representa la <span class="green">**red WAN**</span>, **la máquina física no puede conectarse directamente a esta red**
- Un adaptador de red `vboxnet0` que está <u>directamente conectado</u> a la <span class="blue">**red LAN**</span>
- Una red **Internal Network** `intnet` que representa la <span class="orange">**red DMZ**</span>, **la máquina física no puede conectarse directamente a esta red**

Este es el resúmen de las conexiones de red y los parámetros de la configuración IP:

| Red                                     | Tipo             | Nombre        | Dirección IP / Máscara | Servidor DHCP de VirtualBox
|:---------------------------------------:|:----------------:|:-------------:|:----------------------:|:-------------:|
| <span class="green">**Red WAN**</span>  | Nat Network      | `NAT-Network` | `10.0.2.Y/24`          | `☑️` **Habilitado** <br/> (_predeterminado_)
| <span class="blue">**Red LAN**</span>   | Host-only        | `vboxnet0`    | `192.168.42.1/24`      | `🔲` Deshabilitado
| <span class="orange">**Red DMZ**</span> | Internal network | `intnet` `¹`  | (_ninguna_) `²`        | _No soportado por VirtualBox_

!!! warning
    - `¹`: La red **Internal Network** `intnet` se crea cuando se asigna a alguna máquina virtual
    - `²`: La máquina física no puede conectarse a la <span class="orange">**red DMZ**</span> implementada como **Internal Network** `intnet`, por esto no tiene dirección IP

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando tengas configuradas las redes de VirtualBox.

--------------------------------------------------------------------------------

|             ⇦             |         ⇧          |               ⇨               |
|:--------------------------|:------------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba][arriba] | [Página siguiente][siguiente]

[anterior]: ../
[arriba]: ../#procedimiento
[siguiente]: ../pfsense-install
