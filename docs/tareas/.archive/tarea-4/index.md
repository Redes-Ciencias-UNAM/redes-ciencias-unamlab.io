---
title: Tarea 4 - Captura de tráfico de red
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Tarea 4: Captura de tráfico de red

--------------------------------------------------------------------------------

## Objetivo

- El alumno realizará algunas capturas de tráfico de red donde identificará los protocolos asociados a cada capa del modelo OSI.

## Elementos de apoyo

Te invitamos a leer la documentación:

- [Página de manual de `dig`][dig-man]
- [Página de manual de `nslookup`][nslookup-man]
- [Página de manual de `tcpdump`][tcpdump-man]
- [Página de manual de `windump`][windump-man]
- [Manual de usuario de `nmap`][nmap-reference]
- [Guía del usuario de **WireShark**][wireshark-docs]
- [Preguntas frecuentes de **WireShark**][wireshark-faq]
- [Filtros de captura de **WireShark** y `tcpdump`][wireshark-capture-filters]
- [Filtros de despliegue de **WireShark**][wireshark-display-filters]
- [_Cheat-sheet_ de filtros de captura para **WireShark** y `tcpdump`][cheatsheet-capture-filters]
- [_Cheat-sheet_ de filtros de despliegue para **WireShark**][cheatsheet-display-filters]

## Restricciones

- La fecha límite de entrega es el **martes 28 de marzo de 2023** a las 23:59 horas
- Esta actividad debe ser entregada **en equipo** de acuerdo al [flujo de trabajo para la entrega de tareas y prácticas][flujo-de-trabajo]
    - Utilizar la carpeta `docs/tareas/tarea-5/Equipo-ABCD-EFGH-IJKL-MNOP` para entregar la práctica
        - Donde `Equipo-ABCD-EFGH-IJKL-MNOP` representa el nombre del equipo que debió anotarse previamente en la [lista del grupo][lista-redes]
    - Crear un _merge request_ en el [repositorio de tareas][repo-tareas] para entregar la actividad

<!--
!!! warning
    Se ajustó el periodo de elaboración de esta actividad y la fecha de entrega para compensar los días en los que la Facultad de Ciencias estuvo en paro activo.
-->

## Entregables

- Archivo `README.md`
    - Explicación de los comandos utilizados
    - Explicación de los filtros de captura utilizados
    - Filtro de captura para la capa de aplicación
    - Resultados de las pruebas de conectividad **en TEXTO**

- Carpeta `files`
    - Archivos de las capturas de tráfico de cada capa del modelo OSI en formato PCAP
        - Las capturas de tráfico no deben pesar más de `1 MB`
    - Explicación el tráfico encontrado en cada captura de tráfico de red

--------------------------------------------------------------------------------

## Procedimiento

- Verifica que las siguientes herramientas estén instaladas en tu máquina física.
    - Se recomienda instalar las dependencias en Windows utilizando [chocolatey]

| GNU/Linux Debian        | macOS                              | Windows
|:-----------------------:|:----------------------------------:|:-------------------------------------------:|
| `apt install wireshark` | `brew install wireshark-chmodbpf`¹ | `choco install wireshark`
| `apt install tcpdump`   | `brew install tcpdump`             | `choco install wireshark` <br/> ([`windump`][windump])
| `apt install nmap`      | `brew install nmap`                | `choco install nmap`

!!! note
    - `¹`: Notas sobre la [instalación de **WireShark** en macOS con `ChmodBPF`][wireshark-macos-chmodbpf]

### Solicitar una nueva dirección IP al servidor DHCP

#### Renovar dirección IP

Expira el _lease_ de la dirección de DHCP y solicita una nueva

- Reemplaza `${IFACE}` con el nombre de tu interfaz de red
- Reemplaza `${NAME}` con el nombre de tu conexión de red de Network Manager

| GNU/Linux                                   | macOS                                                               | Windows
|:-------------------------------------------:|:-------------------------------------------------------------------:|:-------------------:|
| `ifdown "${IFACE}"` <br/> `ifup "${IFACE}"` | `ipconfig set "${IFACE}" NONE` <br/> `ipconfig set "${IFACE}" DHCP` | `ipconfig /release` <br/> `ipconfig /renew`
| `nmcli connection down "${NAME}"` <br/> `nmcli connection up "${NAME}"`  |   |
| `dhclient -r "${IFACE}"` |  |

#### Dirección local

Obtén la dirección MAC e IP de tu interfaz de red

| GNU/Linux     | macOS         | Windows
|:-------------:|:-------------:|:---------------:|
| `ifconfig -a` | `ifconfig -a` | `ipconfig /all`
| `ip addr`     |               |

#### Dirección del ruteador

Obtén la dirección IPv4 y/o IPv6 del ruteador

| GNU/Linux       | macOS               | Windows
|:---------------:|:-------------------:|:---------------:|
| `netstat -rn`   | `netstat -rn`       | `netstat -rn`
| `route -n`      | `route get default` | `ipconfig /all`
| `ip route show` |                     |

#### Dirección del servidor DNS

Obtén la dirección IPv4 y/o IPv6 del servidor DNS

| GNU/Linux                                       | macOS                            | Windows
|:-----------------------------------------------:|:--------------------------------:|:------------------------------------------:|
| `cat /etc/resolv.conf`                          | `cat /etc/resolv.conf`           | `type %WINDIR%\System32\Drivers\etc\hosts`
| `nmcli connection show "${NAME}" | grep -i DNS` | `scutil --dns | grep nameserver` | PS: `Get-DnsServer` |

### Captura de tráfico

!!! note
    Deberás hacer una captura de tráfico nueva para las pruebas de cada una de las capas de red

Identifica la interfaz por la que te conectas a Internet, ya sea alámbrica o inalámbrica

| GNU/Linux     | macOS                                | Windows
|:-------------:|:------------------------------------:|:---------------:|
| `ifconfig -a` | `ifconfig -a`                        | `ipconfig /all`
| `ip addr`     | `networksetup -listallhardwareports` |

Inicia una captura de tráfico con `tcpdump` o **WireShark** en la máquina física

- En el caso de **WireShark** selecciona la interfaz de red adecuada
    - **WireShark** pide el filtro de captura cuando se selecciona la interfaz de red antes de iniciar la captura de tráfico
- Para `tcpdump` reemplaza `${IFACE}` con el nombre de tu interfaz de red
    - Asegúrate de incluir las comillas si el nombre de la interfaz de red contiene espacios (Windows)
    - El **filtro de captura**, debe ir encerrado entre comilas simples `'`

```console
% tcpdump -pevvvni "${IFACE}" -w captura.pcap 'FILTRO'
```

#### Limpiar la tabla ARP

!!! warning
    Para esta actividad **deberás limpiar la tabla ARP** cuando inicies una captura de tráfico de red

Limpia la tabla ARP de la maquina física después de iniciar la captura de tráfico

<!-- -->

[man-arp]: https://linux.die.net/man/8/arp

| GNU/Linux                                                             | macOS       | Windows
|:---------------------------------------------------------------------:|:-----------:|:------------------------------------:|
| `ip neighbour flush all`                                              | `arp -d -a` | `arp -d *`
| `arp -n | grep -V "^Address" | awk '{print "arp -d " $1}' | ${SHELL}` |             | `netsh interface IP delete arpcache`

<!-- -->

#### Mostrar la tabla ARP

Muestra la tabla ARP de la máquina física **al finalizar la captura de tráfico**

- Agrega la tabla ARP al final de tu reporte en el archivo `README.md`

<!-- -->

| GNU/Linux                | macOS       | Windows
|:------------------------:|:-----------:|:--------:|
| `arp -an`                | `arp -a -n` | `arp -a`
| `ip neighbour show`      |             |

<!-- -->

#### Limpiar el caché de DNS

!!! warning
    Para esta actividad **deberás limpiar el caché de DNS** cuando inicies una captura de tráfico de red

- Limpia el caché de DNS en la máquina física

| GNU/Linux                        | macOS                        | Windows
|:--------------------------------:|:----------------------------:|:--------------------:|
| `systemd-resolve --flush-caches` | `dscacheutil -flushcache`    | `ipconfig /flushDNS`
| `resolvectl flush-caches`        | `killall -HUP mDNSResponder` |
| `service dnsmasq restart`        |

[mac-dns-cache]: https://help.dreamhost.com/hc/en-us/articles/214981288-Flushing-your-DNS-cache-in-Mac-OS-X-and-Linux
[clear-dns-cache]: https://linuxize.com/post/how-to-clear-the-dns-cache/
[linux-dns-cache]: https://www.cyberciti.biz/faq/rhel-debian-ubuntu-flush-clear-dns-cache/

- Haz una consulta al DNS

| `dig`                                      | `nslookup`
|:------------------------------------------:|:-------------------------------------------:|
| `dig  [TIPO]  NOMBRE  @SERVIDOR`           | `nslookup  [-type=TIPO]  NOMBRE  SERVIDOR`
| `dig example.com. @one.one.one.one`        | `nslookup example.com. one.one.one.one`
| `dig AAAA example.com. @1.1.1.1`           | `nslookup -type=A example.com. 1.1.1.1`
| `dig A example.com. @2606:4700:4700::1111` | `nslookup -type=AAAA example.com. 2606:4700:4700::1111`

--------------------------------------------------------------------------------

### Pruebas de conectividad

#### Capa 2 - Enlace

???+ Filtro
     Utiliza este [_filtro de captura_][wireshark-capture-filters] para `tcpdump` o **WireShark**

    `'arp or (ether src host 00:00:00:00:00:00 or ether dst host ff:ff:ff:ff:ff:ff)'`

- Hacer un descubrimiento de la red con `nmap`
    - Reemplaza `192.168.X.0/24` con el segmento de red que tienes en la **máquina física**

```console
nmap -v -sP 192.168.X.0/24
```

- Separa la captura de red en: **tráfico ARP** y **tráfico broadcast**
    - ¿Qué tráfico de red aparece además de ARP?
    - Probablemente necesites aplicar algún [_filtro de despliegue_][wireshark-display-filters] de **WireShark** para separar la captura de tráfico
    - Anota el filtro de despliegue utilizado en tu reporte

!!! note
    Recuerda guardar tus capturas de tráfico en formato **PCAP** (que es diferente de PCAP-NG)

--------------------------------------------------------------------------------

#### Capa 3 - Red

???+ Filtro
     Utiliza este [_filtro de captura_][wireshark-capture-filters] para `tcpdump` o **WireShark**

    `'ip6 or icmp or ( host ( 0.0.0.0 or 255.255.255.255 ) or ip6 host :: or ip6 net ::ffff )'`

- Hacer pruebas de conectividad hacia el router

```console
% ping  -c 10 "${ROUTER}"

% ping6 -c 10 "${ROUTER}"
```

- Hacer pruebas de conectividad hacia Internet

<!--	www.unam.mx.	IN	CNAME	www.unam.mx.cdn.cloudflare.net.	-->

```console
% ping  -c 10 one.one.one.one        # 1.1.1.1  o  1.0.0.1

% ping6 -c 10 one.one.one.one        # 2606:4700:4700::1111  o  2606:4700:4700::1001

% ping  -c 10 www.fciencias.unam.mx  # Pagina de la FC (IPv4)  132.248.181.220

% ping6 -c 10 www.unam.mx            # Pagina de la UNAM (IPv6)  2606:4700::6811:752e  o  2606:4700::6811:762e
```

- Hacer el descubrimiento de la ruta hacia esos destinos

```console
% traceroute  -n one.one.one.one        # 1.1.1.1  o  1.0.0.1

% traceroute6 -n one.one.one.one        # 2606:4700:4700::1111  o 2606:4700:4700::1001

% traceroute  -n www.fciencias.unam.mx  # Pagina de la FC (IPv4)  132.248.181.220

% traceroute6 -n www.unam.mx            # Pagina de la UNAM (IPv6)  2606:4700::6811:752e  o  2606:4700::6811:762e
```

- Separa la captura de red en: **tráfico IPv4** y **tráfico IPv6**
    - ¿Existen paquetes que se muestren tanto en IPv4 como en IPv6?
    - Probablemente necesites aplicar algún [_filtro de despliegue_][wireshark-display-filters] de **WireShark** para separar la captura de tráfico
    - Anota el filtro de despliegue utilizado en tu reporte

!!! note
    Recuerda guardar tus capturas de tráfico en formato **PCAP** (que es diferente de PCAP-NG)

--------------------------------------------------------------------------------

#### Capa 4 - Transporte

???+ Filtro
     Utiliza este [_filtro de captura_][wireshark-capture-filters] para `tcpdump` o **WireShark**

    `'udp port ( 53 or 67 or 68 ) or tcp port ( 22 or 53 or 5353 or 1024 or 16384 or 32768 or 49152 or 65535 )'`

- Realiza pruebas de conexión exitosas y no exitosas

    - `22`, `53`, `5353`, `80`, `443`, `1024`, `16384`, `32768`, `49152`, `65535`

- Separa la captura de red en: **conexiones exitosas** y **conexiones no exitosas**
    - ¿Existen paquetes que se muestren tanto en las conexiones exitosas como en las no exitosas?
    - Probablemente necesites aplicar algún [_filtro de despliegue_][wireshark-display-filters] de **WireShark** para separar la captura de tráfico
    - Anota el filtro de despliegue utilizado en tu reporte

!!! note
    Recuerda guardar tus capturas de tráfico en formato **PCAP** (que es diferente de PCAP-NG)

--------------------------------------------------------------------------------

#### Capa 7 - Aplicación

!!! note
    - Crea un filtro para únicamente capturar el tráfico hacia los hosts y puertos indicados en los pasos siguientes.

<!--
???+ Filtro
     Utiliza este [_filtro de captura_][wireshark-capture-filters] para `tcpdump` o **WireShark**

    `'tcp port ( 80 or 443 ) and ( host ( 1.1.1.1 or 93.184.216.34 ) or ip6 host ( 2606:4700:4700::1111 or 2606:2800:220:1:248:1893:25c8:1946 ) )'`
-->

- Verifica si la conexión a internet de tu casa tiene conectividad IPv6

- Elige un dominio DNS que no esté bajo `unam.mx.`
    - Verifica si la página que elegiste tiene conectividad IPv6 y un registro DNS de tipo `AAAA`
    - Puede que sea necesario utilizar `www.example.com. ` en lugar de `example.com.`
    - Reemplaza `example.com.` con el dominio que elegiste

- Haz una resolución de DNS por IPv4 e IPv6 con `dig` o `nslookup` utilizando el servidor DNS publico proporcionado por `one.one.one.one`
    - Reemplaza `example.com.` con el dominio que elegiste

```console
% dig -4 A    one.one.one.one.
% dig -4 AAAA one.one.one.one.

% dig -6 A    one.one.one.one.
% dig -6 AAAA one.one.one.one.

% dig -4 A    example.com. @one.one.one.one.
% dig -4 AAAA example.com. @one.one.one.one.

% dig -6 A    example.com. @one.one.one.one.
% dig -6 AAAA example.com. @one.one.one.one.

% nslookup -type=A    example.com. 1.0.0.1
% nslookup -type=AAAA example.com. 1.0.0.1

% nslookup -type=A    example.com. 2606:4700:4700::1001
% nslookup -type=AAAA example.com. 2606:4700:4700::1001
```

- Realiza conexiones **HTTP** al puerto `80`
    - Reemplaza `example.com.` con el dominio que elegiste

```console
% curl -4vk#L 'http://1.1.1.1/' > /dev/null
% curl -4vk#L 'http://one.one.one.one/' > /dev/null

% curl -6vk#L 'http://[2606:4700:4700::1111]/' > /dev/null
% curl -6vk#L 'http://one.one.one.one/' > /dev/null

% curl -4vk#L 'http://example.com/' > /dev/null
% curl -6vk#L 'http://example.com/' > /dev/null
```

- Realiza conexiones **HTTPS** al puerto `443`
    - Reemplaza `example.com.` con el dominio que elegiste

```console
% curl -4vk#L 'https://1.1.1.1/' > /dev/null
% curl -4vk#L 'https://one.one.one.one/' > /dev/null

% curl -6vk#L 'https://[2606:4700:4700::1111]/' > /dev/null
% curl -6vk#L 'https://one.one.one.one/' > /dev/null

% curl -4vk#L 'https://example.com/' > /dev/null
% curl -6vk#L 'https://example.com/' > /dev/null
```

- Separa la captura de red en: **tráfico HTTP** y **tráfico HTTPS** junto con las peticiones **DNS** que se hagan para cada página
    - ¿Se puede ver todo el tráfico **HTTP** y **HTTPS** en las capturas?. Justifica tu respuesta.
    - Probablemente necesites aplicar algún [_filtro de despliegue_][wireshark-display-filters] de **WireShark** para separar la captura de tráfico
    - Anota el filtro de despliegue utilizado en tu reporte

!!! note
    Recuerda guardar tus capturas de tráfico en formato **PCAP** (que es diferente de PCAP-NG)

<!--
--------------------------------------------------------------------------------

- Hacer una prueba de conectividad hacia la dirección IP del gateway

```console
$ ping  -c 4 "${GATEWAY}"
$ ping6 -c 4 "${GATEWAY}"
```

- Hacer una prueba de conectividad hacia direcciones externas en Internet

```console
$ ping -c 10 1.1.1.1

$ ping -c 10 8.8.8.8

$ ping -c 10 9.9.9.9
```

- Intentar acceder un puerto no disponible

```console
$ nc -4vz ${GATEWAY} 12345
$ nc -6vz ${GATEWAY} 12345

$ nc -4vz ${GATEWAY} 65535
$ nc -6vz ${GATEWAY} 65535
```

- Acceder a un servicio HTTP con curl

```console
$ nc -4vz one.one.one.one 80
$ nc -6vz one.one.one.one 80

$ curl -4vk# http://one.one.one.one/
$ curl -6vk# http://one.one.one.one/

$ dig +short A    example.com.
93.184.216.34

$ dig +short AAAA example.com.
2606:2800:220:1:248:1893:25c8:1946

$ ping  -c 10 example.com.
$ ping6 -c 10 example.com.

$ nc -4vz example.com. 80
$ nc -6vz example.com. 80

$ curl -4vk# "http://example.com/"
$ curl -6vk# "http://example.com/"
```

- Acceder a un servicio HTTPS con curl

```console
$ nc -4vz one.one.one.one 443
$ nc -6vz one.one.one.one 443

$ curl -4vk# https://one.one.one.one/
$ curl -6vk# https://one.one.one.one/

$ nc -4vz example.com. 443
$ nc -6vz example.com. 443

$ curl -4vk# "https://example.com/"
$ curl -6vk# "https://example.com/"
```

- Acceder a una página HTTP en el navegador web

- Muestra la tabla ARP de la máquina física

-->
<!--
| GNU/Linux                | macOS       | Windows  |
|:------------------------:|:-----------:|:--------:|
| `arp -an`                | `arp -a -n` | `arp -a`
| `ip neighbour show`      |             |
-->

--------------------------------------------------------------------------------

[flujo-de-trabajo]: https://redes-ciencias-unam.gitlab.io/workflow/
[repo-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests

[lista-redes]: https://tinyurl.com/Lista-Redes-2023-2

[chocolatey]: https://chocolatey.org/install
[wireshark]: https://www.wireshark.org/
[tcpdump]: https://www.tcpdump.org/
[windump]: https://www.winpcap.org/windump/
[nmap]: https://nmap.org/download.html

[dig-man]: https://linux.die.net/man/1/dig
[nslookup-man]: https://ss64.com/bash/nslookup.html

[tcpdump-man]: https://www.tcpdump.org/manpages/tcpdump.1.html
[windump-man]: https://www.winpcap.org/windump/docs/manual.htm

[wireshark-macos-chmodbpf]: https://www.wireshark.org/docs/wsug_html_chunked/ChBuildInstallOSXInstall.html
[wireshark-faq]: https://www.wireshark.org/faq.html
[wireshark-docs]: https://www.wireshark.org/docs/wsug_html_chunked/
[wireshark-capture-filters]: https://wiki.wireshark.org/CaptureFilters
[wireshark-display-filters]: https://wiki.wireshark.org/DisplayFilters
[cheatsheet-capture-filters]: https://packetlife.net/media/library/12/tcpdump.pdf
[cheatsheet-display-filters]: https://packetlife.net/media/library/13/Wireshark_Display_Filters.pdf

[nmap-reference]: https://nmap.org/book/man.html

[dhclient-nixcraft]: https://www.cyberciti.biz/faq/howto-linux-renew-dhcp-client-ip-address/

[man-dhclient]: https://linux.die.net/man/8/dhclient

[man-arp]: https://linux.die.net/man/8/arp
