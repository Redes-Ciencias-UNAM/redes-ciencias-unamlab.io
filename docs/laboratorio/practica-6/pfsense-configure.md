---
title: Configuración del firewall pfSense
authors:
- Dante Erik Santiago Rodríguez Pérez
- Andrés Leonardo Hernández Bermúdez
---

# Configuración del _firewall_ pfSense

## Diagrama de red

<a id="diagrama" name="diagrama"> </a>

| Diagrama de interfaces de red en VirtualBox
|:-----------------------------:|
| ![](img/diagrama_red.png)
| 🟠  <span class="orange">**Red DMZ**</span> - Conexiones que se manejan en la red **internal network** de VirtualBox
| 🔵    <span class="blue">**Red LAN**</span> - Conexiones que se manejan en la red **host-only** de VirtualBox
| 🟢   <span class="green">**Red WAN**</span> - Conexiones que se manejan en la red **NAT Network** de VirtualBox
| 🔴 <span class="red">**Red Externa**</span> - Conexiones que salen desde el equipo físico hacia Internet

## Configuración de las interfaces de red

Permitir que las redes <span class="green">**WAN**</span>, <span class="blue">**LAN**</span> y <span class="orange">**DMZ**</span> acepten el tráfico de redes no homologadas (RFC 1918).

| Asignación de interfaces
|:--:|
| ![](img/pfsense/pfsense-042-webui-interfaces.png)

### <span class="green">**Red WAN**</span>

Entrar a la configuración de la interfaz <span class="green">**WAN**</span> `em0`

- Revisar que la configuración de IP sea por DHCP

| Configuración de interfaz <span class="green">**WAN**</span>
|:--:|
| ![](img/pfsense/pfsense-43-webui-interface-wan.png)

- Ir al final de la página y <u>desmarcar</u> la casilla **Block private networks and loopback addresses** para permitir que los segmentos no homologados (RFC1918) puedan comunicarse

| Permitir redes RFC1918
|:--:|
| ![](img/pfsense/pfsense-43a-webui-interface-wan-cont.png)

### <span class="blue">**Red LAN**</span>

Entrar a la configuración de la interfaz <span class="blue">**LAN**</span> `em1`

- Revisar que la configuración de IP estática y que tenga la dirección `192.168.42.254/24`

| Configuración de interfaz <span class="blue">**LAN**</span>
|:--:|
| ![](img/pfsense/pfsense-44-webui-interface-lan.png)

- Ir al final de la página y <u>desmarcar</u> la casilla **Block private networks and loopback addresses** para permitir que los segmentos no homologados (RFC1918) puedan comunicarse

| Permitir redes RFC1918
|:--:|
| ![](img/pfsense/pfsense-44a-webui-interface-lan-cont.png)

### <span class="orange">**Red DMZ**</span>

Entrar a la configuración de la interfaz <span class="orange">**DMZ**</span> `em2`

- Revisar que la configuración de IP estática y que tenga la dirección `172.16.1.254/24`

| Configuración de interfaz <span class="orange">**DMZ**</span>
|:--:|
| ![](img/pfsense/pfsense-45-webui-interface-opt.png)

- Ir al final de la página y <u>desmarcar</u> la casilla **Block private networks and loopback addresses** para permitir que los segmentos no homologados (RFC1918) puedan comunicarse

| Permitir redes RFC1918
|:--:|
| ![](img/pfsense/pfsense-45a-webui-interface-opt-cont.png)

--------------------------------------------------------------------------------

## Servicio DNS

- Dar clic en **Services** ⇨ **DNS Resolver**  para entrar a la configuración del servicio local de DNS

- Configurar el servicio con los siguientes parámetros:

| Elemento                    | Valor
|:---------------------------:|:--------:|
| Enable                      | ☑️ **Habilitado**
| Listen port                 | 53
| Network interfaces *        | <span class="blue">**LAN**</span> , <span class="orange">**OPT1**</span> , localhost
| Outgoing network interfaces | <span class="green">**WAN**</span>
| DNS Query Forwarding        | ☑️ **Habilitado**
| Static DHCP                 | ☑️ **Habilitado**

!!! note
    - Puedes utilizar la tecla `Ctrl` para seleccionar varias interfaces de red

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

--------------------------------------------------------------------------------

## Alias para servicios de la <span class="orange">**red DMZ**</span>

### Alias de IP

Generar alias de puertos para los servicios SSH, HTTP y HTTPS

- Dar clic en **Firewall** ⇨ **Aliases** ⇨ **IP** para entrar a la interfaz de _alias_

#### Alias de IP para servidor en DMZ

- Dar clic en **IP** y en el botón **Add ➕** para agregar una regla

- Llenar el formulario con los siguientes parámetros:

| Elemento    | Valor
|:-----------:|:--------:|
| Nombre      | DMZ_Servers
| Descripción | Servidores en la red DMZ
| Type        | Hosts
| IP          | 172.16.1.10
| Descripción | Servidor CentOS

- Da clic en el botón **Save 💾** para guardar la regla

- Da clic en el botón **Apply Changes ✔️** para guardar los cambios

<!--
Generar alias

- Generar Alias para los servidores WEB que tendrán acceso desde Internet
    - Firewall -> Alias -> IP
    - Hacer clic en el botón _add_ para agregar alias, considerar los siguientes parámetros :
        - Name: DMZ_WEB_ACCESO
        - Description : Dispositivos DMZ Acceso
        - Host: dirección IP y descripción
    - Al finalizar hacer clic en el botón _save_, posteriormente aplicar cambios con el botón _apply Changes_
-->

<!-- REPLACE -->
<!--
| Alias IP
|:----:|
| ![](img/pfsense/27.png)
-->

#### Resumen de alias de IP

- Dar clic en **Firewall** ⇨ **Aliases** ⇨ **IP** para entrar a la interfaz de _alias_

| Alias de IP
|:----:|
| ![](img/pfsense/pfsense-46-webui-firewall-alias-ip.png)

<!--	-------------------------------------------------------------------	-->

### Alias de puertos

Generar alias de puertos para los servicios SSH, HTTP y HTTPS

- Dar clic en **Firewall** ⇨ **Aliases** ⇨ **Ports** para entrar a la interfaz de _alias_

#### Alias para SSH

- Dar clic en **Ports** y en el botón **Add ➕** para agregar una regla

- Llenar el formulario con los siguientes parámetros:

| Elemento    | Valor
|:-----------:|:--------:|
| Nombre      | SSH_DMZ
| Descripción | Servicio SSH en red DMZ
| Type        | Ports
| Port        | 22
| Descripción | SSH

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

#### Alias para HTTP y HTTPS

- Dar clic en **Ports** y en el botón **Add ➕** para agregar una regla

- Llenar el formulario con los siguientes parámetros:

| Elemento    | Valor
|:-----------:|:--------:|
| Nombre      | WEB_DMZ
| Descripción | Servicios HTTP y HTTPS en red DMZ
| Type        | Ports
| Port        | 80
| Descripción | HTTP
| Port        | 443
| Descripción | HTTPS

!!! note
    - Da clic en el botón **Add Port ➕** para agregar un <u>segundo puerto</u> al formulario

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

<!-- REPLACE -->
<!--
| Alias Puertos
|:----:|
| ![](img/pfsense/26.png)
-->

#### Resumen de alias de puertos

| Alias de puertos
|:----:|
| ![](img/pfsense/pfsense-47-webui-firewall-alias-ports.png)

--------------------------------------------------------------------------------

## Port Forward

### Acceso al servicio **SSH** en la <span class="orange">**red DMZ**</span>

Crear una redirección de puertos para acceder a los servicios de la DMZ desde Internet

- Dar clic en **Firewall** ⇨ **NAT** para entrar a la interfaz de redirección de puertos

- Dar clic en **Port Forward** y en el botón **Add ➕** para agregar una regla "al final" de la lista

| Elemento               | Valor
|:----------------------:|:--------:|
| Interfaz               | <span class="green">**WAN**</span>
| Address Family         | IPv4
| Protocol               | TCP
| Destination            | <span class="green">**WAN address**</span>
| Destination port range | From port: **SSH** To port **SSH**
| Redirect target IP     | Single Host: DMZ_Servers
| Redirect target port   | **SSH**
| Description            | Redirección de puerto **SSH** de interfaz WAN a servidor DMZ

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

### Acceso al servicio **HTTP** en la <span class="orange">**red DMZ**</span>

Agregar otra regla para el servicio HTTP

- Dar clic en **Port Forward** y en el botón **Add ➕** para agregar una regla "al final" de la lista

| Elemento               | Valor
|:----------------------:|:--------:|
| Interfaz               | <span class="green">**WAN**</span>
| Address Family         | IPv4
| Protocol               | TCP
| Destination            | <span class="green">**WAN address**</span>
| Destination port range | From port: **HTTP** To port **HTTP**
| Redirect target IP     | Single Host: DMZ_Servers
| Redirect target port   | **HTTP**
| Description            | Redirección de puerto **HTTP** de interfaz WAN a servidor DMZ

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

### Acceso al servicio **HTTPS** en la <span class="orange">**red DMZ**</span>

Agregar otra regla para el servicio HTTPS

- Dar clic en **Port Forward** y en el botón **Add ➕** para agregar una regla "al final" de la lista

| Elemento               | Valor
|:----------------------:|:--------:|
| Interfaz               | <span class="green">**WAN**</span>
| Address Family         | IPv4
| Protocol               | TCP
| Destination            | <span class="green">**WAN address**</span>
| Destination port range | From port: **HTTPS** To port **HTTPS**
| Redirect target IP     | Single Host: DMZ_Servers
| Redirect target port   | **HTTPS**
| Description            | Redirección de puerto **HTTPS** de interfaz WAN a servidor DMZ

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

<!--
- Configuración de Port forwarding para acceso a la DMZ
    - Firewall->NAT->Port Forward
    - Hacer clic en el botón _add_ para agregar nuevas reglas, considerar los siguientes parámetros :
        - Interface: <span class="green">**WAN**</span>
        - Address Family: IPv4
        - Protocol: TCP
        - Destination : <span class="green">**WAN address**</span>
        - Destination port range: http
        - Redirect target IP: 172.16.1.101
        - Redirect target port: http
        - Description: Port forwarding DMZ Web Server
    - Al finalizar hacer clic en el botón _save_, posteriormente aplicar cambios con el botón _apply Changes_
    - Replicar configuración de Port Forwarding para los servicios _https_ y _ssh_
-->
<!--
| Port Forwarding servicio http
|:----:|
| ![](img/pfsense/29.png)
-->

<!-- Agregar una regla en la interfaz WAN para permitir la entrada de los puertos redirigidos -->

<!--
Revisar que las reglas de redirección de puertos estén aplicadas en la interfaz WAN

- Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="green">**WAN**</span> para entrar a la interfaz de reglas del _firewall_

| Regla en interfaz <span class="green">**WAN**</span> para Port Forwarding http
|:----:|
| ![](img/pfsense/30.png)
-->

### Resumen de _port forward_

Revisar que las reglas de redirección de puertos estén aplicadas en la interfaz WAN

<!-- - Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="green">**WAN**</span> para entrar a la interfaz de reglas del _firewall_ -->
- Dar clic en **Firewall** ⇨ **NAT** ⇨ **Port Forward** para entrar a la interfaz de redirección de puertos

| Reglas de _port forwarding_
|:----:|
| ![](img/pfsense/pfsense-50-webui-firewall-nat-port-forward.png)

--------------------------------------------------------------------------------

## Reglas de _firewall_ para la <span class="green">**red WAN**</span>

### Permitir ICMP en la interfaz <span class="green">**WAN**</span>

- Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="green">**WAN**</span> para entrar a la interfaz de reglas del _firewall_

- Da clic en el botón **Add ⤵️** para agregar una regla "al final" de la lista

- Llenar el formulario con los siguientes parámetros:

| Elemento        | Valor
|:---------------:|:--------:|
| Action          | ✅ Pass
| Interface       | <span class="green">**WAN**</span>
| Address Familiy | IPv4
| Protocol        | ICMP
| ICMP Subtypes   | Any
| Source          | **Any**
| Destination     | <span class="green">**WAN address**</span>
| Log             | ☑️ **Habilitado**
| Description     | Permite ICMP en la interfaz <span class="green">**WAN**</span>

### Resumen de reglas en la interfaz <span class="green">**WAN**</span>

- Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="green">**WAN**</span> para entrar a la interfaz de reglas del _firewall_

| Reglas de _firewall_ en la interfaz <span class="green">**WAN**</span>
|:----:|
| ![](img/pfsense/pfsense-55-webui-firewall-rules-wan.png)

!!! note
    - Las reglas que tienen una descipción "**NAT**" fueron creadas cuando se configuró el _port forwarding_ de la interfaz <span class="green">**WAN**</span> a los servicios de la <span class="orange">**red DMZ**</span> en el equipo CentOS

--------------------------------------------------------------------------------

## Reglas de _firewall_ para la <span class="blue">**red LAN**</span>

### Permitir ICMP en la interfaz <span class="blue">**LAN**</span>

- Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="blue">**LAN**</span> para entrar a la interfaz de reglas del _firewall_

- Da clic en el botón **Add ⤵️** para agregar una regla "al final" de la lista

- Llenar el formulario con los siguientes parámetros:

| Elemento        | Valor
|:---------------:|:--------:|
| Action          | ✅ Pass
| Interface       | <span class="blue">**LAN**</span>
| Address Familiy | IPv4
| Protocol        | ICMP
| ICMP Subtypes   | Any
| Source          | **Any**
| Destination     | <span class="blue">**LAN address**</span>
| Log             | ☑️ **Habilitado**
| Description     | Permite ICMP en la interfaz <span class="blue">**LAN**</span>

### Resumen de reglas en la interfaz <span class="blue">**LAN**</span>

| Reglas de _firewall_ en la interfaz <span class="blue">**LAN**</span>
|:----:|
| ![](img/pfsense/pfsense-56-webui-firewall-rules-lan.png)

--------------------------------------------------------------------------------

<!-- FIXME -->

## Reglas de _firewall_ para la <span class="orange">**red DMZ**</span>

### Permitir ICMP en la interfaz <span class="orange">**DMZ**</span>

- Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="orange">**DMZ**</span> para entrar a la interfaz de reglas del _firewall_

- Da clic en el botón **Add ⤵️** para agregar una regla "al final" de la lista

- Llenar el formulario con los siguientes parámetros:

| Elemento        | Valor
|:---------------:|:--------:|
| Action          | ✅ Pass
| Interface       | <span class="orange">**OPT1**</span>
| Address Familiy | IPv4
| Protocol        | ICMP
| ICMP Subtypes   | Any
| Source          | **Any**
| Destination     | <span class="orange">**OPT1 address**</span>
| Log             | ☑️ **Habilitado**
| Description     | Permite ICMP en la interfaz <span class="orange">**OPT1**</span>

### Bloquear tráfico de red <span class="orange">**DMZ**</span> a <span class="blue">**LAN**</span>

Crear una regla en la interfaz <span class="orange">**DMZ**</span> para bloquear el tráfico de la <span class="orange">**red DMZ**</span> a la <span class="blue">**red LAN**</span>

- Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="orange">**OPT1**</span> para entrar a la interfaz de reglas del _firewall_

- Da clic en el botón **Add ⤵️** para agregar una regla "al final" de la lista

- Llenar el formulario con los siguientes parámetros:

| Elemento        | Valor
|:---------------:|:--------:|
| Action          | ❌ Block
| Interface       | <span class="orange">**OPT1**</span>
| Address Familiy | IPv4
| Protocol        | ANY
| Source          | <span class="orange">**OPT1 net**</span>
| Destination     | <span class="blue">**LAN net**</span>
| Log             | ☑️ **Habilitado**
| Description     | Bloquea la comunicación de la <span class="orange">**red DMZ**</span> a la <span class="blue">**red LAN**</span>

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

<!--
| Reglas <span class="orange">**DMZ**</span>
|:----:|
| ![](img/pfsense/25.png)
-->

### Permitir el acceso de la <span class="orange">**red DMZ**</span> a <span class="red">**Internet**</span>

Crear una regla en la interfaz DMZ para dar acceso a los servicios desde internet.

- Dar clic en **Firewall** ⇨ **Rules** ⇨ <span class="orange">**OPT1**</span> para entrar a la interfaz de reglas del _firewall_

- Da clic en el botón **Add ⤵️** para agregar una regla "al final" de la lista

- Llenar el formulario con los siguientes parámetros:

| Elemento        | Valor
|:---------------:|:--------:|
| Action          | ✅ Pass
| Interface       | <span class="orange">**OPT1**</span>
| Address Familiy | IPv4
| Protocol        | Any
| Source          | <span class="orange">**OPT1 net**</span>
| Destination     | **Any**
| Log             | 🔲 **Deshabilitado**
| Description     | Permite la salida a Internet desde la <span class="orange">**DMZ**</span>

- Da clic en el botón **Save 💾** para guardar los ajustes

- Da clic en el botón **Apply Changes ✔️** para aplicar los cambios

<!--
| Regla Acceso DMZ a Internet
|:----:|
| ![](img/pfsense/28.png)
-->

### Resumen de reglas en la interfaz <span class="orange">**DMZ**</span>

| Reglas de _firewall_ en la interfaz <span class="orange">**DMZ**</span>
|:----:|
| ![](img/pfsense/pfsense-57-webui-firewall-rules-dmz.png)

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando hayas terminado la configuración del _firewall_ pfSense

--------------------------------------------------------------------------------

|             ⇦             |         ⇧          |               ⇨               |
|:--------------------------|:------------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba][arriba] | [Página siguiente][siguiente]

[anterior]: ../pfsense-install
[arriba]: ../#procedimiento
[siguiente]: ../alpine-install
