---
title: Implementación de sitios web en Kubernetes
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Implementación de sitios web en **Kubernetes**

--------------------------------------------------------------------------------

## Sitio web _default_

La primer tarea es configurar un sitio web "_default_" que maneje todo el tráfico, esto se conoce como un sitio _catch-all_

- En la práctica anterior, esto se configuró en el _VirtualHost_ `_default_` de Apache HTTPD

### Crea la página de índice

Crea un archivo llamado `index.html` con el siguiente contenido

- Reemplaza `Equipo-AAAA-BBBB-CCCC-DDDD` con el nombre de tu equipo
- Reemplaza `example.com` con tu nombre de dominio

!!! note
    Puedes utilizar otra página `index.html`, siempre y cuando **no utilice recursos externos** y muestre la información solicitada

```html
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Examen 4 - Redes 2023-2</title>
    <link href="data:image/x-icon;base64," rel="icon" type="image/x-icon" />
  </head>
  <body>
    <h1>Hola</h1>
    <div>
      <p>Esta es la página del <code>Equipo-AAAA-BBBB-CCCC-DDDD</code></p>
      <p>Nuestro dominio DNS es <code>example.com</code></p>
      <ul>
        <li><em>Esta página se está sirviendo desde el cluster de <b>Kubernetes</b> en <code>k3s.example.com</code></em></li>
      </ul>
    </div>
    <hr/>
    <code>Redes de Computadoras</code>
  </body>
</html>
```

Crea un _configmap_ para guardar el contenido del archivo `index.html`

- Este _configmap_ se pasará a un servidor web para mostrar la página cuando se visita la URL del servidor
- Reemplaza `index-equipo-aaaa-bbbb-cccc-dddd` con el nombre de tu equipo (**en minúsculas**)

```console
usuario@laptop ~ % kubectl create configmap index-equipo-aaaa-bbbb-cccc-dddd --from-file=index.html
configmap/index-equipo-aaaa-bbbb-cccc-dddd created
```

### Crea el _deployment_ del servidor web

Ejecuta el siguiente comando para crear un _deployment_ del servidor web `nginx` y especifica que escucha conexiones en el puerto `80`

```console
usuario@laptop ~ % kubectl create deployment root-nginx --image=nginx --port=80
```

Verifica que el _pod_ de `nginx` se encuentre en estado de ejecución

```console
usuario@laptop ~ % kubectl get pods -l app=root-nginx
NAME                          READY   STATUS    RESTARTS   AGE
root-nginx-7b6b9d5b79-c9z99   1/1     Running   0          60s
```

### Asigna el volúmen de la página de índice al _deployment_ del servidor web

Edita el _deployment_ `root-nginx` para agregar las líneas donde se monta el _configmap_ que contiene el archivo `index.html`

```console
usuario@laptop ~ % kubectl edit deployment/root-nginx
	...
```

Agrega las líneas correspondientes a `volumeMounts` y `volumes` en las secciones adecuadas

- Reemplaza `index-equipo-aaaa-bbbb-cccc-dddd` con el nombre de tu equipo (**en minúsculas**)

!!! warning
    - Verifica que la identación de las líneas sea correcta y que insertes las secciones en el lugar adecuado
    - YAML es un lenguaje que maneja la identación utlizando **DOS ESPACIOS**

```yaml
---
apiVersion: apps/v1
kind: Deployment
...
spec:
...
  template:
...
    spec:  # Agrega la sección "volumes" entre "spec" y "containers"
      volumes:
      - name: index-equipo-aaaa-bbbb-cccc-dddd
        configMap:
          name: index-equipo-aaaa-bbbb-cccc-dddd
      containers:  # Esta línea es donde empieza la sección "containers"
      - name: nginx
...
        terminationMessagePolicy: File  # Agrega la sección "volumeMounts" después de esta línea
        volumeMounts:
        - name: index-equipo-aaaa-bbbb-cccc-dddd
          mountPath: /usr/share/nginx/html/index.html
          subPath: index.html
```

Si tienes éxito, aparecerá un mensaje indicando que el _deployment_ fue editado, en caso contrario `kubectl` te regresará al editor e insertará un comentario en el archivo tratando de explicar el error

```console
usuario@laptop ~ % kubectl edit deployment/root-nginx
deployment.apps/root-nginx edited
```

Espera unos segundos mientras se lanza otro _pod_ con la nueva configuración

```console
usuario@laptop ~ % kubectl get pods -l app=root-nginx
NAME                          READY   STATUS    RESTARTS   AGE
root-nginx-6556db7b58-7dj7l   1/1     Running   0          30s
```

!!! note
    - El identificador del _pod_ debe ser diferente porque se creó uno nuevo con la configuración actualizada del _deployment_
    - Utiliza los siguientes comandos para forzar la creación de un nuevo _pod_ en caso de que no se cree uno automáticamente

    <pre><code>
    usuario@laptop ~ % kubectl scale deployment/root-nginx --replicas 0
    usuario@laptop ~ % kubectl scale deployment/root-nginx --replicas 1
    </code></pre>

### Visualiza la página de índice utilizando _port-forward_

Haz una redirección de puertos de Kubernetes para visualizar la página de índice que acabas de configurar

- Esto funciona al poner el puerto `8080` del equipo local en escucha y redireccionar las peticiones al puerto `80` del _pod_ en el cluster

```console
usuario@laptop ~ % kubectl port-forward deployment/root-nginx 8080:80
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
	...
```

!!! note
    - Deja este comando corriendo

Abre otra terminal y revisa que el puerto este abierto en la máquina local

```
usuario@laptop ~ % nc -vz localhost 8080
Connection to localhost port 8080 [tcp/http-alt] succeeded!
```

Revisa que el puerto responda utilizando `curl`

```
usuario@laptop ~ % curl -vk# 'http://localhost:8080/'
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.87.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.25.0
< Date: Fri, 26 May 2023 07:36:54 GMT
< Content-Type: text/html
< Content-Length: 592
< Last-Modified: Fri, 26 May 2023 07:30:23 GMT
< Connection: keep-alive
< ETag: "6470600f-250"
< Accept-Ranges: bytes
<
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Examen 4 - Redes 2023-2</title>
    <link href="data:image/x-icon;base64," rel="icon" type="image/x-icon" />
  </head>
  <body>
    <h1>Hola</h1>
    <div>
      <p>Esta es la página del <code>Equipo-AAAA-BBBB-CCCC-DDDD</code></p>
      <p>Nuestro dominio DNS es <code>example.com</code></p>
      <ul>
        <li><em>Esta página se está sirviendo desde el cluster de <b>Kubernetes</b> en <code>k3s.example.com</code></em></li>
      </ul>
    </div>
    <hr/>
    <code>Redes de Computadoras</code>
  </body>
</html>
```

Accede con un navegador al puerto `8080` en el equipo local

| Página en Kubernetes mostrada a través de _port-forward_
|:-----------------------------------------------------------:|
| ![](img/kubernetes-port-forward-root-nginx-index-custom.png)

Presiona `Ctrl+C` para salir de `kubectl port-forward`

```console
usuario@laptop ~ % kubectl port-forward deployment/root-nginx 8080:80
Forwarding from 127.0.0.1:8080 -> 80
Forwarding from [::1]:8080 -> 80
Handling connection for 8080
	...
^C
```

--------------------------------------------------------------------------------

## Sitio de la página de documentación del _kernel_ Linux

Crea un _deployment_ donde se ejecute la imágen de contenedor que contiene el sitio web de la documentación del _kernel_ Linux

- Reemplaza `docker.io/tonejito/nginx:linux-doc` con el [nombre de la imagen de contenedor que creaste][docker-crear-imagenes-de-contenedor] para el sitio web de la documentación del _kernel_ Linux

```console
usuario@laptop ~ % CONTAINER_IMAGE="docker.io/tonejito/nginx:linux-doc"
usuario@laptop ~ % kubectl create deployment linux-doc --image="${CONTAINER_IMAGE}" --port=80
deployment.apps/linux-doc created
```

Verifica que el _pod_ asociado al _deployment_ `linux-doc` se está ejecutando

```console
usuario@laptop ~ % kubectl get pods -l app=linux-doc
NAME                         READY   STATUS    RESTARTS   AGE
linux-doc-5565869db8-shs58   1/1     Running   0          30s
```

Verifica que puedas acceder al sitio web con `kubectl port-forward`

```console
usuario@laptop ~ % kubectl port-forward deployment/linux-doc 8081:80
Forwarding from 127.0.0.1:8081 -> 80
Forwarding from [::1]:8081 -> 80
Handling connection for 8081
	...
```

!!! note
    - Deja este comando corriendo

Abre otra terminal y revisa que el puerto este abierto en la máquina local

```console
usuario@laptop ~ % nc -vz localhost 8081
Connection to localhost port 8081 [tcp/sunproxyadmin] succeeded!
```

Revisa que el puerto responda utilizando `curl`

```console
usuario@laptop ~ % curl -vk# 'http://localhost:8081/' | egrep '</?title>'
*   Trying 127.0.0.1:8081...
* Connected to localhost (127.0.0.1) port 8081 (#0)
> GET / HTTP/1.1
> Host: localhost:8081
> User-Agent: curl/7.87.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.22.0
< Date: Fri, 26 May 2023 07:42:45 GMT
< Content-Type: text/html
< Content-Length: 145412
< Last-Modified: Fri, 29 Apr 2022 09:36:50 GMT
< Connection: keep-alive
< ETag: "626bb1b2-23804"
< Accept-Ranges: bytes
<
{ [32768 bytes data]
######################################################################### 100.0%
*Connection #0 to host localhost left intact

	<title>The Linux Kernel documentation &mdash; The Linux Kernel  documentation</title>
```

Accede con un navegador al puerto `8081` en el equipo local

| Página en Kubernetes mostrada a través de _port-forward_
|:--------------------------------------------------------:|
| ![](img/kubernetes-port-forward-linux-doc.png)

Presiona `Ctrl+C` para salir de `kubectl port-forward`

```console
usuario@laptop ~ % kubectl port-forward deployment/linux-doc 8081:80
Forwarding from 127.0.0.1:8081 -> 80
Forwarding from [::1]:8081 -> 80
Handling connection for 8081
	...
^C
```

--------------------------------------------------------------------------------

## Sitio de la página de tareas de la materia

Crea un _deployment_ donde se ejecute la imágen de contenedor que contiene el sitio web de tareas de la materia

- Reemplaza `docker.io/tonejito/nginx:tareas-redes` con el [nombre de la imagen de contenedor que creaste][docker-crear-imagenes-de-contenedor] para el sitio web de tareas de la materia

```console
usuario@laptop ~ % CONTAINER_IMAGE="docker.io/tonejito/nginx:tareas-redes"
usuario@laptop ~ % kubectl create deployment tareas-redes --image="${CONTAINER_IMAGE}" --port=80
deployment.apps/tareas-redes created
```

Verifica que el _pod_ asociado al _deployment_ `tareas-redes` se está ejecutando

```console
usuario@laptop ~ % kubectl get pods -l app=tareas-redes
NAME                            READY   STATUS    RESTARTS   AGE
tareas-redes-6cb9c496cd-2gdq5   1/1     Running   0          30s
```

Verifica que puedas acceder al sitio web con `kubectl port-forward`

```console
usuario@laptop ~ % kubectl port-forward deployment/tareas-redes 8082:80
Forwarding from 127.0.0.1:8082 -> 80
Forwarding from [::1]:8082 -> 80
Handling connection for 8082
	...
```

!!! note
    - Deja este comando corriendo

Abre otra terminal y revisa que el puerto este abierto en la máquina local

```console
usuario@laptop ~ % nc -vz localhost 8082
Connection to localhost port 8082 [tcp/sunproxyadmin] succeeded!
```

Revisa que el puerto responda utilizando `curl`

```console
usuario@laptop ~ % curl -vk# 'http://localhost:8082/' | egrep '</?title>'
*   Trying 127.0.0.1:8082...
* Connected to localhost (127.0.0.1) port 8082 (#0)
> GET / HTTP/1.1
> Host: localhost:8082
> User-Agent: curl/7.87.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.22.0
< Date: Fri, 26 May 2023 07:49:37 GMT
< Content-Type: text/html
< Content-Length: 12443
< Last-Modified: Tue, 31 May 2022 01:47:02 GMT
< Connection: keep-alive
< ETag: "62957396-309b"
< Accept-Ranges: bytes
<
{ [12443 bytes data]
######################################################################### 100.0%
*Connection #0 to host localhost left intact

	<title>Redes - Tareas 2023-2</title>
```

Accede con un navegador al puerto `8082` en el equipo local

| Página en Kubernetes mostrada a través de _port-forward_
|:--------------------------------------------------------:|
| ![](img/kubernetes-port-forward-tareas-redes.png)

Presiona `Ctrl+C` para salir de `kubectl port-forward`

```console
usuario@laptop ~ % kubectl port-forward deployment/linux-doc 8082:80
Forwarding from 127.0.0.1:8082 -> 80
Forwarding from [::1]:8082 -> 80
Handling connection for 8082
	...
^C
```


--------------------------------------------------------------------------------

## Verifica la configuración

Revisa que los _deployments_ tengan estado **READY** `1/1` y que el estado de los _pods_ sea `Running`

```console
usuario@laptop ~ % kubectl get deployments,pods
NAME                           READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/root-nginx     1/1     1            1           22m
deployment.apps/linux-doc      1/1     1            1           11m
deployment.apps/tareas-redes   1/1     1            1           6m

NAME                                READY   STATUS    RESTARTS   AGE
pod/root-nginx-6556db7b58-7dj7l     1/1     Running   0          20m
pod/linux-doc-5565869db8-shs58      1/1     Running   0          10m
pod/tareas-redes-6cb9c496cd-2gdq5   1/1     Running   0          5m
```

!!! danger
    - Verifica que **TODAS** las configuraciones que hiciste estén presentes respués de reiniciar la máquina antes de continuar con [la siguiente sección][siguiente]

!!! note
    - Continúa en [la siguiente página][siguiente] si los _deployments_ y _pods_ se están ejecutando y pudiste ver las páginas utilizando `kubectl port-forward` en el equipo local

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../k8s-ingress-nginx
[siguiente]: ../k8s-ingress-resource

[docker-crear-imagenes-de-contenedor]: ../docker#crear-imagenes-de-contenedor
