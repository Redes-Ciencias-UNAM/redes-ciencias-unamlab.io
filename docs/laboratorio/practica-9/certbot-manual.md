---
title: Trámite del certificado SSL desde otro equipo
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Trámite del certificado SSL desde otro equipo con `certbot`

!!! warning
    - Asegúrate de haber instalado Apache HTTPD y [configurado todo lo necesario hasta este punto][tramita-certificado-ssl]

## Trámite manual de certificado

!!! note
    - Máquina local

```console
root@laptop ~ # certbot certonly --manual --domain 'example.com' --domain '*.example.com'
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator manual, Installer None
Enter email address (used for urgent renewal and security notices)
 (Enter 'c' to cancel): linux@tonejito.org

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.3-September-21-2022.pdf. You must
agree in order to register with the ACME server. Do you agree?
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: Y

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing, once your first certificate is successfully issued, to
share your email address with the Electronic Frontier Foundation, a founding
partner of the Let's Encrypt project and the non-profit organization that
develops Certbot? We'd like to send you email about our work encrypting the web,
EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: N
Account registered.
Requesting a certificate for example.com and *.example.com
Performing the following challenges:
dns-01 challenge for example.com
http-01 challenge for example.com
```

### Validar el dominio por DNS

!!! note
    - Máquina local

```text
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please deploy a DNS TXT record under the name
_acme-challenge.example.com with the following value:

fedcba9876543210fedcba9876543210fedcba9876543210

Before continuing, verify the record is deployed.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Press Enter to Continue
```

### Crear y validar el registro para validación DNS

!!! note
    - Máquina local
    - Instancia en la nube

```console
root@example:~# dig TXT _acme-challenge.example.com.

; <<>> DiG 9.9.5-9+deb8u17-Debian <<>> TXT _acme-challenge.example.com.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 4541
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;_acme-challenge.example.com.	IN	TXT

;; ANSWER SECTION:
_acme-challenge.example.com. 300 IN	TXT	"fedcba9876543210fedcba9876543210fedcba9876543210"

;; Query time: 31 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Tue Nov 22 12:26:45 EST 2022
;; MSG SIZE  rcvd: 114
```

### Validar el dominio por HTTP

!!! note
    - Máquina local

```text
	...
Press Enter to Continue		⬅	⬅	⬅	⬅	⬅	⬅

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Create a file containing just this data:

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.0123456789abcdef0123456789abcdef0123456789abcdef

And make it available on your web server at this URL:

http://example.com/.well-known/acme-challenge/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

(This must be set up in addition to the previous challenges; do not remove,
replace, or undo the previous challenge tasks yet.)

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Press Enter to Continue
```

### Crear y validar el archivo para validación HTTP

!!! note
    - Instancia en la nube

```console
root@example:~# mkdir -vp /var/www/html//.well-known/acme-challenge
mkdir: created directory ‘/var/www/html//.well-known’
mkdir: created directory ‘/var/www/html//.well-known/acme-challenge’

root@example:~# FILE_NAME='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
root@example:~# FILE_CONTENT='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.0123456789abcdef0123456789abcdef0123456789abcdef'

root@example:~# echo "${CONTENT}" > "/var/www/html//.well-known/acme-challenge/${FILE_NAME}"
root@example:~# curl -vk# "http://localhost//.well-known/acme-challenge/${FILE_NAME}"
* Hostname was NOT found in DNS cache
*   Trying ::1...
* Connected to localhost (::1) port 80 (#0)
> GET //.well-known/acme-challenge/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx HTTP/1.1
> User-Agent: curl/7.38.0
> Host: localhost
> Accept: */*
>
< HTTP/1.1 200 OK
< Date: Tue, 22 Nov 2022 17:26:30 GMT
* Server Apache is not blacklisted
< Server: Apache
< Last-Modified: Tue, 22 Nov 2022 17:26:15 GMT
< ETag: "58-5ee1278205a02"
< Accept-Ranges: bytes
< Content-Length: 88
< X-Content-Type-Options: nosniff
< X-Frame-Options: sameorigin
< X-Robots-Tag: none, noindex, nofollow, noarchive, nosnippet, notranslate, noimageindex
<
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.0123456789abcdef0123456789abcdef0123456789abcdef
* Connection #0 to host localhost left intact
```

!!! note
    - Máquina local

```console
tonejito@laptop ~ % FILE_NAME='xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
tonejito@laptop ~ % curl -vk# "http://example.com//.well-known/acme-challenge/${FILE_NAME}"
*   Trying 1.2.3.4:80...
* Connected to example.com (1.2.3.4) port 80 (#0)
> GET //.well-known/acme-challenge/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx HTTP/1.1
> Host: example.com
> User-Agent: curl/7.79.1
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Date: Tue, 22 Nov 2022 17:29:10 GMT
< Server: Apache
< Last-Modified: Tue, 22 Nov 2022 17:26:15 GMT
< ETag: "58-5ee1278205a02"
< Accept-Ranges: bytes
< Content-Length: 88
< X-Content-Type-Options: nosniff
< X-Frame-Options: sameorigin
< X-Robots-Tag: none, noindex, nofollow, noarchive, nosnippet, notranslate, noimageindex
<
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.0123456789abcdef0123456789abcdef0123456789abcdef
* Connection #0 to host example.com left intact
```

### Finalizar el trámite del certificado SSL

!!! note
    - Máquina local

```text
	...
Press Enter to Continue		⬅	⬅	⬅	⬅	⬅	⬅

Waiting for verification...
Cleaning up challenges
IMPORTANT NOTES:
- Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/example.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/example.com/privkey.pem
   Your certificate will expire on 2023-02-20. To obtain a new or
   tweaked version of this certificate in the future, simply run
   certbot again. To non-interactively renew *all* of your
   certificates, run "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

## Revisar archivos resultantes

!!! note
    - Máquina local

```console
root@laptop ~ # tree /etc/letsencrypt/
/etc/letsencrypt/
|-- accounts
|   \-- acme-v02.api.letsencrypt.org
|       \-- directory
|           \-- 0123456789abcdef0123456789abcdef
|               |-- meta.json
|               |-- private_key.json
|               \-- regr.json
|-- archive
|   \-- example.com
|       |-- cert1.pem
|       |-- chain1.pem
|       |-- fullchain1.pem
|       \-- privkey1.pem
|-- cli.ini
|-- csr
|   \-- 0000_csr-certbot.pem
|-- keys
|   \-- 0000_key-certbot.pem
|-- live
|   |-- README
|   \-- example.com
|       |-- README
|       |-- cert.pem -> ../../archive/example.com/cert1.pem
|       |-- chain.pem -> ../../archive/example.com/chain1.pem
|       |-- fullchain.pem -> ../../archive/example.com/fullchain1.pem
|       \-- privkey.pem -> ../../archive/example.com/privkey1.pem
|-- renewal
|   \-- example.com.conf
\-- renewal-hooks
    |-- deploy
    |-- post
    \-- pre

15 directories, 17 files
```

Verifica que los archivos PEM del certificado y la llave se reconozcan correctamente

```console
root@laptop ~ # file /etc/letsencrypt/live/example.com/*
/etc/letsencrypt/live/example.com/README:        ASCII text
/etc/letsencrypt/live/example.com/cert.pem:      symbolic link to ../../archive/example.com/cert1.pem
/etc/letsencrypt/live/example.com/chain.pem:     symbolic link to ../../archive/example.com/chain1.pem
/etc/letsencrypt/live/example.com/fullchain.pem: symbolic link to ../../archive/example.com/fullchain1.pem
/etc/letsencrypt/live/example.com/privkey.pem:   symbolic link to ../../archive/example.com/privkey1.pem

root@laptop ~ # file  --dereference /etc/letsencrypt/live/example.com/*
/etc/letsencrypt/live/example.com/README:        ASCII text
/etc/letsencrypt/live/example.com/cert.pem:      PEM certificate
/etc/letsencrypt/live/example.com/chain.pem:     PEM certificate
/etc/letsencrypt/live/example.com/fullchain.pem: PEM certificate
/etc/letsencrypt/live/example.com/privkey.pem:   ASCII text

root@laptop ~ # openssl x509 -subject -issuer -dates -noout -in /etc/letsencrypt/live/example.com/cert.pem
subject=CN = example.com	⬅	⬅	⬅	⬅	⬅	⬅
issuer=C = US, O = Let's Encrypt, CN = R3
notBefore=Nov 22 16:27:14 2022 GMT
notAfter=Feb 20 16:27:13 2023 GMT
```

## Copiar los certificados a la máquina remota

!!! note
    - Máquina local

```console
root@laptop ~ # tar -cvvf ~/letsencrypt.tar /etc/letsencrypt/
tar: Removing leading `/' from member names
drwxr-xr-x root/root         0 2022-11-22 17:27 /etc/letsencrypt/
drwx------ root/root         0 2022-11-22 17:27 /etc/letsencrypt/archive/
drwxr-xr-x root/root         0 2022-11-22 17:27 /etc/letsencrypt/archive/example.com/
	...

root@laptop ~ # scp letsencrypt.tar example.com:~/
tonejito@example.com's password:
letsencrypt.tar                                      100%   50KB 1.1KB/s   00:00
```

## Instalar los certificados en la máquina remota

!!! note
    - Instancia en la nube


Renombra el directorio `/etc/letsencrypt`

```console
root@example:~# test -d /etc/letsencrypt && mv -v /etc/letsencrypt /etc/letsencrypt.old
‘/etc/letsencrypt’ -> ‘/etc/letsencrypt.old’
```

Descomprime el archivo `letsencrypt.tar`

```console
root@example:~# mkdir -vp /etc/letsencrypt
mkdir: created directory ‘/etc/letsencrypt’

root@example:~# tar -xvvpf ~tonejito/letsencrypt.tar -C /
drwxr-xr-x root/root         0 2022-11-22 12:27 etc/letsencrypt/
drwx------ root/root         0 2022-11-22 12:27 etc/letsencrypt/archive/
drwxr-xr-x root/root         0 2022-11-22 12:27 etc/letsencrypt/archive/example.com/
	...
```

Verifica que los archivos PEM del certificado y la llave se reconozcan correctamente en la máquina remota

```console
root@example:~# tree /etc/letsencrypt/
/etc/letsencrypt/
...

root@example:~# file /etc/letsencrypt/live/example.com/*
/etc/letsencrypt/live/example.com/README:        ASCII text
/etc/letsencrypt/live/example.com/cert.pem:      symbolic link to ../../archive/example.com/cert1.pem
/etc/letsencrypt/live/example.com/chain.pem:     symbolic link to ../../archive/example.com/chain1.pem
/etc/letsencrypt/live/example.com/fullchain.pem: symbolic link to ../../archive/example.com/fullchain1.pem
/etc/letsencrypt/live/example.com/privkey.pem:   symbolic link to ../../archive/example.com/privkey1.pem

root@example:~# file  --dereference /etc/letsencrypt/live/example.com/*
/etc/letsencrypt/live/example.com/README:        ASCII text
/etc/letsencrypt/live/example.com/cert.pem:      PEM certificate
/etc/letsencrypt/live/example.com/chain.pem:     PEM certificate
/etc/letsencrypt/live/example.com/fullchain.pem: PEM certificate
/etc/letsencrypt/live/example.com/privkey.pem:   ASCII text

root@example:~# openssl x509 -subject -issuer -dates -noout -in /etc/letsencrypt/live/example.com/cert.pem
subject=CN = example.com	⬅	⬅	⬅	⬅	⬅	⬅
issuer=C = US, O = Let's Encrypt, CN = R3
notBefore=Nov 22 16:27:14 2022 GMT
notAfter=Feb 20 16:27:13 2023 GMT
```

!!! note
    Continúa con la [configuración de la redirección del tráfico a HTTPS][https-redir]

--------------------------------------------------------------------------------

[tramita-certificado-ssl]: ../ssl-lets-encrypt/#tramita-el-certificado-ssl-con-lets-encrypt
[https-redir]: ../ssl-lets-encrypt/#redirige-el-trafico-http-a-https
