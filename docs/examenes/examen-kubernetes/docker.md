---
title: Creación de imágenes de contenedor con Docker
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Creación de imágenes de contenedor con Docker

--------------------------------------------------------------------------------

## Cuenta en Docker Hub

Crea una cuenta en Docker Hub

- <https://hub.docker.com/signup>

!!! note
    - Todos los integrantes del equipo deben crear una cuenta en DockerHub
    - Anota **todos** los nombres de usuario (Docker ID) en tu reporte

| Página de registro de Docker Hub
|:----:|
| ![](img/docker-signup.png)

## Instalación de Docker Desktop

Instala Docker Desktop en tu computadora personal

- <https://www.docker.com/products/docker-desktop/>
- <https://docs.docker.com/desktop/>
- <https://www.docker.com/blog/getting-started-with-docker-desktop/>

!!! note
    En GNU/Linux opcionalmente puedes instalar el demonio de docker (sin interfaz gráfica)
    Para esto sigue las instrucciones de **Docker Engine** para tu distribución de GNU/Linux

    - <https://docs.docker.com/engine/install/debian/>

## Prueba de un contenedor

Ejecuta el contenedor `hello-world` para verificar que Docker está instalado correctamente

```
usuario@laptop ~ % docker run -it hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
7050e35b49f5: Pull complete
Digest: sha256:80f31da1ac7b312ba29d65080fddf797dd76acfb870e677f390d5acba9741b17
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (arm64v8)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

## Descargar una imagen de contenedor

Descarga una imagen de contenedor del _registry_ `docker.io`

!!! note
    Normalmente este paso se puede omitir porque la imagen se descarga cuando el contenedor se ejecuta o se construye

```
usuario@laptop ~ % docker pull nginx:1.23-alpine
1.23-alpine: Pulling from library/nginx
c41833b44d91: Pull complete
2c2c9b85ac58: Pull complete
40f94fa36194: Pull complete
ae26f20697dc: Pull complete
e4fa283fba0e: Pull complete
4c53b6cdc37b: Pull complete
7bcac465295e: Pull complete
Digest: sha256:02ffd439b71d9ea9408e449b568f65c0bbbb94bebd8750f1d80231ab6496008e
Status: Downloaded newer image for nginx:1.23-alpine
docker.io/library/nginx:1.23-alpine
```

<!--
## Borrar una imagen de contenedor

```
usuario@laptop ~ % docker rmi nginx:1.23-alpine
Untagged: nginx:1.23-alpine
Untagged: nginx@sha256:02ffd439b71d9ea9408e449b568f65c0bbbb94bebd8750f1d80231ab6496008e
Deleted: sha256:510900496a6c312a512d8f4ba0c69586e0fbd540955d65869b6010174362c313
Deleted: sha256:caf1fa269ac346a280e6ca27d7f9963844504a20073729e6210f03d2b3956677
Deleted: sha256:812f6132ce68fa42fd32a3156689d4373d98f9bf6544312eff0b2f6cd60a2f94
Deleted: sha256:9196bd565f77e73b561ed7f40512727ed022a61e86754d0d56fdbb43415447d5
Deleted: sha256:badd2812d19c75201b060164850edcc911ad9a213a778ef0ce4565d15576b8b1
Deleted: sha256:decb9051a18cbc7ff4fa934bbdfb958128d72839cf150f570ec1910ad81a0180
Deleted: sha256:f6132fcbfc40e72bb25863ab28132422abae4f49cfb7c49822f314848819aac7
Deleted: sha256:26cbea5cba74143fbe6f584f5fc5321543155aedc4a434fcaa63b643877b5a74

```

```
usuario@laptop ~ % docker rmi nginx:1.23-alpine
Error response from daemon: conflict: unable to remove repository reference "nginx:1.23-alpine" (must force) - container 2f4fb7860deb is using its referenced image 510900496a6c
```

```
usuario@laptop ~ % docker kill 2f4fb7860deb
2f4fb7860deb

usuario@laptop ~ % docker rm 2f4fb7860deb
2f4fb7860deb

usuario@laptop ~ % docker rmi nginx:1.23-alpine
Untagged: nginx:1.23-alpine
Untagged: nginx@sha256:...
Deleted: sha256:...
	...
```
-->

## Ejecutar un contenedor con un servidor web

Ejecuta la imagen de contenedor que acabas de descargar

- La opción `-it` indica que se muestre en pantalla la salida del proceso que se ejecuta en contenedor
- La opción `-p 8080:80` indica que se redireccionará el puerto `8080` del equipo físico al puerto `80` del contenedor

!!! note
    Usualmente se puede escribir el nombre corto del contenedor (`nginx:1.23-alpine`), pero es recomendable escribir la ruta completa de la imagen para evitar ambigüedades entre _registries_

```
usuario@laptop ~ % docker run -it -p 8080:80 docker.io/library/nginx:1.23-alpine
Unable to find image 'nginx:1.23-alpine' locally
1.23-alpine: Pulling from library/nginx
c41833b44d91: Pull complete
2c2c9b85ac58: Pull complete
40f94fa36194: Pull complete
ae26f20697dc: Pull complete
e4fa283fba0e: Pull complete
4c53b6cdc37b: Pull complete
7bcac465295e: Pull complete
Digest: sha256:02ffd439b71d9ea9408e449b568f65c0bbbb94bebd8750f1d80231ab6496008e
Status: Downloaded newer image for nginx:1.23-alpine
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: info: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: info: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
2023/05/22 04:32:59 [notice] 1#1: using the "epoll" event method
2023/05/22 04:32:59 [notice] 1#1: nginx/1.23.4
2023/05/22 04:32:59 [notice] 1#1: built by gcc 12.2.1 20220924 (Alpine 12.2.1_git20220924-r4)
2023/05/22 04:32:59 [notice] 1#1: OS: Linux 5.15.49-linuxkit
2023/05/22 04:32:59 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
2023/05/22 04:32:59 [notice] 1#1: start worker processes
2023/05/22 04:32:59 [notice] 1#1: start worker process 30
2023/05/22 04:32:59 [notice] 1#1: start worker process 31
2023/05/22 04:32:59 [notice] 1#1: start worker process 32
2023/05/22 04:32:59 [notice] 1#1: start worker process 33
	...
```

### Acceder al contenedor del servidor web

Abre otra terminal y lista los contenedores que se están ejecutando

- Visualiza la columna `PORTS` y verifica que indique que el puerto `8080` del equipo físico se redirige al puerto `80` del contenedor
    - `0.0.0.0:8080->80/tcp` para las conexiones IPv4
    - `:::8080->80/tcp` para las conexiones IPv6

```
usuario@laptop ~ % docker ps
CONTAINER ID   IMAGE               COMMAND                 CREATED         STATUS         PORTS                  NAMES
68a8a97ddda5   nginx:1.23-alpine   "/docker-entrypoint…"   5 seconds ago   Up 4 seconds   0.0.0.0:8080->80/tcp   raccoon_city
```

<!--
```
usuario@laptop ~ % curl -v# 'http://localhost:8080/'
*   Trying 127.0.0.1:8080...
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET / HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.87.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Server: nginx/1.23.4
< Date: Mon, 22 May 2023 04:33:23 GMT
< Content-Type: text/html
< Content-Length: 615
< Last-Modified: Tue, 28 Mar 2023 17:09:24 GMT
< Connection: keep-alive
< ETag: "64231f44-267"
< Accept-Ranges: bytes
<
<!DOCTYPE html>
<html>
  <head>
    <title>Welcome to nginx!</title>
    <style>
    html { color-scheme: light dark; }
    body { width: 35em; margin: 0 auto;
    font-family: Tahoma, Verdana, Arial, sans-serif; }
    </style>
  </head>
  <body>
    <h1>Welcome to nginx!</h1>
    <p>
      If you see this page, the nginx web server is successfully installed and working.
      Further configuration is required.
    </p>
    <p>
      For online documentation and support please refer to <a href="http://nginx.org/">nginx.org</a>.
      <br/>
      Commercial support is available at <a href="http://nginx.com/">nginx.com</a>.
    </p>
    <p><em>Thank you for using nginx.</em></p>
  </body>
</html>
* Connection #0 to host localhost left intact
```
-->

Accede a la URL del contenedor para verificar que el servidor web responde

```
usuario@laptop ~ % curl -fsSL http://localhost:8080/ | egrep '</?title>'
    <title>Welcome to nginx!</title>
```

Abre un navegador web y accede a la URL del contenedor

- <http://localhost:8080/>

| Página web del contenedor `nginx`
|:----:|
| ![](img/docker-container-nginx.png)

--------------------------------------------------------------------------------

## Crear imagenes de contenedor

### Iniciar sesión en el _registry_ Docker Hub

Utiliza el siguiente comando para iniciar sesión en Docker Hub y que puedas enviar las imagenes de contenedor que construyas al _registry_.

```console
usuario@laptop ~ % docker login docker.io
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: tonejito
Password:
WARNING! Your password will be stored unencrypted in /home/usuario/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store

Login Succeeded
```

!!! warning
    - Tu contraseña se guardará en _texto plano_ en el archivo `~/.docker/config.json`.
    - Es **RECOMENDABLE** que configures un mecanismo para que se guarde de manera cifrada en tu equipo
        - <https://docs.docker.com/engine/reference/commandline/login/#credentials-store>

### Sintaxis de `Dockerfile`

Esta es la sintaxis de un `Dockerfile` que utiliza [**BuildKit**][docker-buildkit] y algunas características modernas como _heredoc_ en el comando `COPY`

[docker-buildkit]: https://docs.docker.com/develop/develop-images/build_enhancements/

Cada acción se explica en un comentario

!!! warning
    - Asegúrate de especificar la primer línea `# syntax=docker/dockerfile:1.4` (con el símbolo `#`) para utilizar las características avanzadas de BuildKit
    - En caso de que tu versión de Docker no soporte BuildKit, utiliza la otra opción en el `Dockerfile`

```
# syntax=docker/dockerfile:1.4
# La primer línea permite utilizar características avanzadas de BuildKit

# Especifica qué contenedor se utilizará como base para la nueva imagen
FROM	docker.io/library/nginx:1.23-alpine AS nginx

# Declara una variable de entorno para utilizarla después en algún comando
ENV	NGINX_ROOT=/usr/share/nginx/html

# Crea la página de índice para NGINX
# Es necesaria la primer línea (syntax=docker/...) para hacer uso de esta variante de COPY
COPY	<<EOF ${NGINX_ROOT}/index.html
<html>
  <head>
    <title>Hello world!</title>
  </head>
  <body>
    <h1>It works!</h1>
  </body>
</html>
EOF
```

<!--
```
usuario@laptop ~ % curl -fsSL http://localhost:8080/
<html>
  <head>
    <title>Hello world!</title>
  </head>
  <body>
    <h1>It works!</h1>
  </body>
</html>
```
-->

- Revisa el contenido de los siguientes archivos `Dockerfile`
- Modificalos haciendo comentarios línea por línea explicando las acciones realizadas
- Anexa los archivos modificados a tu reporte `files/linux-doc/Dockerfile` y `files/tareas-redes/Dockerfile`

    - [`Dockerfile` para el contenedor `linux-doc`][dockerfile-linux-doc]
        - Tal vez necesites el archivo [`apt-conf-99-local.conf`][apt-conf-local]
    - [`Dockerfile` para el contenedor `tareas-redes`][dockerfile-tareas-redes]
        - Modifica las variables de entorno `GIT_PROJECT`, `GIT_REPO` y `GIT_BRANCH` de tal manera que `GITLAB_URL` apunte al repositorio donde tu equipo envía las tareas de la materia

[dockerfile-hello-nginx]: ../files/hello-nginx/Dockerfile
[dockerfile-linux-doc]: ../files/linux-doc/Dockerfile
[apt-conf-local]: ./files/linux-doc/apt-conf-99-local.conf
[dockerfile-tareas-redes]: ../files/tareas-redes/Dockerfile

### Construir imagenes de contenedor

Utiliza `docker build` para construir la imagen del contenedor, verifica que el `TAG` corresponda a tu nombre de usuario y el contenedor `linux-doc` o `tareas-redes` según sea el caso

!!! note
    - Se recomienda utilizar la opción `--progress plain` para capturar toda la salida de `docker build`
    - Guarda esta salida en los archivos `files/linux-doc/build-output.txt` y `files/tareas-redes/build-output.txt`
    - Anexa estos archivos a tu reporte

!!! warning
    - Necesitas construir las imagenes de contenedor en un equipo con arquitectura `x86_64` (también conocida como `amd64`)
    - Las imágenes no podrán ser ejecutadas en el cluster de Kubernetes en Azure si se construyen en una computadora con procesador `ARM` porque la máquina virtual de Azure tiene arquitectura `x86_64`

```
usuario@laptop ~ % TAG=tonejito/hello-nginx
usuario@laptop ~ % docker build --progress plain -t "${TAG}" ./
#1 [internal] load .dockerignore
#1 transferring context: 2B done
#1 DONE 0.0s

#2 [internal] load build definition from Dockerfile
#2 transferring dockerfile: 308B done
#2 DONE 0.0s

#3 resolve image config for docker.io/docker/dockerfile:1.4
#3 DONE 0.6s

#4 docker-image://docker.io/docker/dockerfile:1.4@sha256:9ba7531bd80fb0a858632727cf7a112fbfd19b17e94c4e84ced81e24ef1a0dbc
#4 CACHED

#5 [internal] load metadata for docker.io/library/nginx:1.23-alpine
#5 DONE 0.0s

#6 [internal] preparing inline document
#6 CACHED

#7 [1/2] FROM docker.io/library/nginx:1.23-alpine
#7 DONE 0.0s

#8 [2/2] COPY	<<EOF /usr/share/nginx/html/index.html
#8 DONE 0.0s

#9 exporting to image
#9 exporting layers 0.0s done
#9 writing image sha256:7bc863031bee63b0aaf1c6ce9aa641213bc5988cefc784aa520e648b3617d9dc done
#9 naming to docker.io/tonejito/hello-nginx done
#9 DONE 0.0s
```

### Lista las imagenes de contenedor

```
usuario@laptop ~ % docker images
REPOSITORY             TAG           IMAGE ID       CREATED             SIZE
tonejito/tareas-redes  latest        93a5bb20db56   5 minutes ago        41.9 MB	⬅️
tonejito/linux-doc     latest        ce93e6541728   8 minutes ago       188.0 MB	⬅️
tonejito/hello-nginx   latest        7bc863031bee   11 minutes ago       40.6 MB	⬅️
tonejito/hello-httpd   latest        8c14047e1c63   13 minutes ago       68.4 MB
debian                 11            af5aac46f628   2 weeks ago         118.0 MB
python                 3.11-alpine   f03ab8864405   11 days ago          57.4 MB
nginx                  1.23-alpine   510900496a6c   7 weeks ago          40.6 MB
httpd                  2.4-alpine    65e63686adc8   10 days ago          68.4 MB
```


### Enviar las imagenes de contenedor al _registry_

Utiliza `docker push` para enviar tus imagenes de contenedor `linux-doc` y `tareas-redes` al _registry_ Docker Hub

!!! note
    - Guarda esta salida en los archivos `files/linux-doc/push-output.txt` y `files/tareas-redes/push-output.txt`
    - Anexa estos archivos a tu reporte

```
usuario@laptop ~ % TAG=tonejito/hello-nginx
usuario@laptop ~ % docker push "${TAG}"
Using default tag: latest
The push refers to repository [docker.io/tonejito/hello-nginx]
e747e9c4bf58: Pushed
363722710bd8: Mounted from library/nginx
fbebe8ba7bed: Mounted from library/nginx
363858148796: Mounted from library/nginx
7e1b91127bea: Mounted from library/nginx
2749f4c7cb99: Mounted from library/nginx
09353074bdde: Mounted from library/nginx
26cbea5cba74: Mounted from library/nginx
latest: digest: sha256:658165388b9c111924dc7b82fabd028f07c3fbee1a7e009741a11fa7b0779d33 size: 1988
```

--------------------------------------------------------------------------------

## Verifica los cambios realizados

Revisa que tus imagenes de contenedor `linux-doc` y `tareas-redes` aparezcan en tu cuenta de usuario en Docker Hub

- <https://hub.docker.com/u/USUARIO>

| Imagenes de contenedor en Docker Hub
|:------------------------------------:|
| ![](img/docker-hub-images.png)

!!! warning
    - Necesitas construir las imagenes de contenedor en un equipo con arquitectura `x86_64` (también conocida como `amd64`)
    - Las imágenes no podrán ser ejecutadas en el cluster de Kubernetes en Azure si se construyen en una computadora con procesador `ARM` porque la máquina virtual de Azure tiene arquitectura `x86_64`

!!! danger
    - Verifica que **TODAS** las imagenes de contenedor esten presentes antes de continuar con [la siguiente sección][siguiente]

!!! note
    - Continúa en [la siguiente página][siguiente] cuando hayas terminado de construir las imagenes de contenedor y las hayas subido a Docker Hub

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../
[siguiente]: ../prepare-vm

[docker-desktop]: https://www.docker.com/products/docker-desktop/
[docker-desktop-docs]: https://docs.docker.com/desktop/

[dockerfile-heredoc]: https://www.docker.com/blog/introduction-to-heredocs-in-dockerfiles/
[dockerfile-buildkit-syntax]: https://hub.docker.com/r/docker/dockerfile
[dockerfile-frontend-syntaxe]: https://github.com/moby/buildkit/blob/master/frontend/dockerfile/docs/syntax.md
