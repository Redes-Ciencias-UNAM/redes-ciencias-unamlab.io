---
title: Instalación de k3s en Debian 11
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Instalación de `k3s` en Debian 11

--------------------------------------------------------------------------------

## Verificar el _script_ de instalación de `k3s`

Abrir la página de [VirusTotal para escanear una URL][virustotal-scan-url], pegar la liga `https://get.k3s.io/` y presionar `<ENTER>` para validar el script de instalación

[virustotal-scan-url]: https://www.virustotal.com/gui/home/url

- <https://www.virustotal.com/gui/home/url>

!!! warning
    - **NUNCA** se deben ejecutar _scripts_ aleatorios de internet sin antes verificar su autenticidad utilizando funciones _hash_, firmas criptográficas o un escaneo de antivirus

    | VirusTotal - get.k3s.io
    |:----------------------------------:|
    | ![](img/virustotal-get_k3s_io.png)

## Descargar el _script_ de instalación de `k3s`

Guardar el _script_ de instalación y dar permisos de ejecución

```console
root@example:~# wget -c -nv -O ~/get-k3s-io.sh 'https://get.k3s.io/'
2023-05-22 13:04:24 URL:https://get.k3s.io/ [30463/30463] -> "/root/get-k3s-io.sh" [1]

root@example:~# chmod -c +x ./get-k3s-io.sh
mode of './get-k3s-io.sh' changed from 0644 (rw-r--r--) to 0755 (rwxr-xr-x)
```

## Ejecutar el _script_ de instalación de `k3s`

Se utilizan las siguientes variables de entorno para personalizar la instalación de `k3s` en el sistema operativo:

- `INSTALL_K3S_SKIP_START="true"` se utiliza para no iniciar `k3s` después de haberlo instalado, esto permite ingresar opciones adicionales para reducir el consumo de memoria RAM

- `INSTALL_K3S_EXEC` permite especificar argumentos adicionales a `k3s`
    - Deshabilitar componentes no necesarios para reducir el uso de memoria RAM
    - Especificar nombres adicionales para el certificado SSL del servidor API de Kubernetes
        - Reemplaza `k3s.example.com` con tu nombre de dominio (ej. `k3s.tonejito.cf`)
        - Reemplaza `20.58.191.222` con la dirección IP **pública** de tu máquina virtual

```console
root@example:~# export INSTALL_K3S_SKIP_START="true"
root@example:~# export INSTALL_K3S_EXEC="--tls-san='k3s.tonejito.cf' --tls-san='20.58.191.222' --disable-cloud-controller --disable=metrics-server --disable=servicelb --disable=traefik"
root@example:~# ~/get-k3s-io.sh
[INFO]  Finding release for channel stable
[INFO]  Using v1.26.4+k3s1 as release
[INFO]  Downloading hash https://github.com/k3s-io/k3s/releases/download/v1.26.4+k3s1/sha256sum-amd64.txt
[INFO]  Downloading binary https://github.com/k3s-io/k3s/releases/download/v1.26.4+k3s1/k3s
[INFO]  Verifying binary download
[INFO]  Installing k3s to /usr/local/bin/k3s
[INFO]  Finding available k3s-selinux versions
/root/get-k3s-io.sh: 407: [: k3s-selinux-1.2-2.el8.noarch.rpm: unexpected operator
[INFO]  Creating /usr/local/bin/kubectl symlink to k3s
[INFO]  Creating /usr/local/bin/crictl symlink to k3s
[INFO]  Creating /usr/local/bin/ctr symlink to k3s
[INFO]  Creating killall script /usr/local/bin/k3s-killall.sh
[INFO]  Creating uninstall script /usr/local/bin/k3s-uninstall.sh
[INFO]  env: Creating environment file /etc/systemd/system/k3s.service.env
[INFO]  systemd: Creating service file /etc/systemd/system/k3s.service
[INFO]  systemd: Enabling k3s unit
Created symlink /etc/systemd/system/multi-user.target.wants/k3s.service → /etc/systemd/system/k3s.service.
```

## Establecer variables de entorno para `k3s`

Establecer una variable de entorno para hacer que `k3s` ocupe aún menos memoria RAM en el equipo

<!--
```console
root@example:~# grep EnvironmentFile /etc/systemd/system/k3s.service
EnvironmentFile=-/etc/default/%N
EnvironmentFile=-/etc/sysconfig/%N
EnvironmentFile=-/etc/systemd/system/k3s.service.env
```
-->

```console
root@example:~# cat > /etc/systemd/system/k3s.service.env << EOF
# Aggressive garbage collector
GOGC=10
EOF
```

!!! danger
    - Esta configuración es únicamente para un ambiente de pruebas
    - Ninguna de estas configuraciones se debe realizar en ambientes de producción
    - Estas configuraciones se realizan para que el cluster de k3s pueda ejecutarse en el equipo `B1S` que tiene únicamente **1 vCPU** y **512 MB de RAM**
    - No utilizar el tipo de instancia `B1S` para clusters de Kubernetes en ambientes de producción

--------------------------------------------------------------------------------

## Iniciar el servicio de `k3s`

Reinicia el equipo para verificar que los cambios sean persistentes y que el servicio de `k3s` se inicie de manera automática

```console
root@example:~# reboot
```

Espera a que la máquina vuelva a estar en línea y verifica que el servicio de `k3s` se haya iniciado de manera correcta

```console
root@example:~# PAGER=cat systemctl status --full k3s
● k3s.service - Lightweight Kubernetes
     Loaded: loaded (/etc/systemd/system/k3s.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2023-05-22 13:07:47 CST; 27s ago
       Docs: https://k3s.io
   Main PID: 256 (k3s-server)
      Tasks: 48
     Memory: 522.8M
        CPU: 14.941s
     CGroup: /system.slice/k3s.service
             ├─ 256 /usr/local/bin/k3s server
             ├─ 512 containerd -c /var/lib/rancher/k3s/agent/etc/containerd/config.toml -a /run/k3s/containerd/containerd.sock --state /run/k3s/containerd --root /var/lib/rancher/k3s/agent/containerd
             ├─1024 /var/lib/rancher/k3s/data/.../bin/containerd-shim-runc-v2 -namespace k8s.io -id ... -address /run/k3s/containerd/containerd.sock
             ├─2048 /var/lib/rancher/k3s/data/.../bin/containerd-shim-runc-v2 -namespace k8s.io -id ... -address /run/k3s/containerd/containerd.sock
             └─4096 /var/lib/rancher/k3s/data/.../bin/unpigz -d -c
```

!!! warning
    Cuando `k3s` se haya iniciado el uso de memoria RAM crecerá **considerablemente**, esta fue la razón por la que se hicieron los ajustes [en la sección anterior][anterior]

```console
root@example:~# free -m
               total        used        free      shared  buff/cache   available
Mem:             863         416          67           0         379         315
Swap:           1023           0        1023
```

## Revisa que el puerto de `kube-apiserver` esté escuchando

El servidor del API de Kubernetes esucha en el puerto `6443`, mismo que se debe de abrir para poder administrar el cluster desde el equipo local

```console
root@example:~# netstat -ntulp | grep 6443
tcp6       0      0 :::6443              :::*        LISTEN      256/k3s server
```

--------------------------------------------------------------------------------

## Prepara el archivo `~/.kube/config` en el equipo remoto

Agrega al usuario `redes` al grupo `staff` y cambia los permisos del archivo `/etc/rancher/k3s/k3s.yaml`

```console
root@example:~# adduser redes staff
Adding user `redes' to group `staff' ...
Adding user redes to group staff
Done.

root@example:~# chown -c root:staff /etc/rancher/k3s/k3s.yaml
changed ownership of '/etc/rancher/k3s/k3s.yaml' from root:root to root:staff

root@example:~# chmod -c 0440 /etc/rancher/k3s/k3s.yaml
mode of '/etc/rancher/k3s/k3s.yaml' changed from 0600 (rw-------) to 0440 (r--r-----)
```

Haz una liga simbólica al archivo `k3s.yaml` en la ruta `~/.kube/config` con el usuario `root`

```console
root@example:~# mkdir -vp ~/.kube
mkdir: created directory '/root/.kube'

root@example:~# ln -vsf /etc/rancher/k3s/k3s.yaml ~/.kube/config
'/root/.kube/config' -> '/etc/rancher/k3s/k3s.yaml'

root@k3s:~# ls -la ~/.kube
total 8
drwxr-xr-x 2 root root 4096 May 22 13:11 .
drwx------ 4 root root 4096 May 22 13:11 ..
lrwxrwxrwx 1 root root   25 May 22 13:11 config -> /etc/rancher/k3s/k3s.yaml
```

Copia el archivo `k3s.yaml` en la ruta `~/.kube/config` con el usuario `redes` y ajusta los permisos

```console
root@example:~# su - redes

redes@example:~$ mkdir -vp ~/.kube
mkdir: created directory '/home/redes/.kube'

redes@example:~$ sudo cp -v /etc/rancher/k3s/k3s.yaml ~/.kube/config
'/etc/rancher/k3s/k3s.yaml' -> '/home/redes/.kube/config'

redes@example:~$ sudo chown -cR redes:staff ~/.kube
changed ownership of '/home/redes/.kube' from redes:users to redes:staff
changed ownership of '/home/redes/.kube/config' from root:root to redes:staff

redes@example:~$ chmod -c 0640 ~/.kube/config
mode of '/home/redes/.kube/config' changed from 0440 (r--r-----) to 0640 (rw-r-----)

redes@example:~$ ls -la ~/.kube
total 12
drwxr-xr-x 2 redes staff 4096 May 22 13:11 .
drwxr-xr-x 4 redes users 4096 May 22 13:11 ..
-rw-r----- 1 redes staff 2961 May 22 13:11 config
```

## Conectarse al cluster de Kubernetes desde el equipo remoto

Utilizar el programa `kubectl` que fué instalado por `k3s` para listar la información del cluster

```console
redes@example:~$ which kubectl
/usr/local/bin/kubectl

redes@example:~$ kubectl version --short
Flag --short has been deprecated, and will be removed in the future. The --short output will become the default.
Client Version: v1.26.4+k3s1
Kustomize Version: v4.5.7
Server Version: v1.26.4+k3s1

redes@example:~$ kubectl get nodes -o wide
NAME              STATUS   ROLES                  AGE     VERSION        INTERNAL-IP   EXTERNAL-IP   OS-IMAGE                         KERNEL-VERSION          CONTAINER-RUNTIME
k3s.tonejito.cf   Ready    control-plane,master   6m19s   v1.26.4+k3s1   10.0.0.4      <none>        Debian GNU/Linux 11 (bullseye)   5.10.0-23-cloud-amd64   containerd://1.6.19-k3s1
```

--------------------------------------------------------------------------------

## Abrir el puerto `6443` en el grupo de seguridad de Azure

Localiza el grupo de seguridad de la máquina virtual en el [portal de Azure][azure-portal] y abre el puerto `6443` al tráfico de Internet

- Utiliza el nombre `kube-api-server`

| Portal de Azure - Grupo de Seguridad
|:------------------------------------:|
| ![](img/azure-portal-vm-deploy-020-inbound-rules.png)

--------------------------------------------------------------------------------

## Instala `kubectl` en el equipo local

El programa `kubectl` se utiliza para crear recursos en el cluster de Kubernetes

- <https://kubernetes.io/docs/tasks/tools/>

Descarga el binario de `kubectl` para tu sistema operativo

- <https://kubernetes.io/releases/download/#kubectl>

```console
usuario@laptop ~ % KUBECTL_VERSION=v1.26.4
usuario@laptop ~ % curl -fsSLO https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl

usuario@laptop ~ % chmod -c +x kubectl
mode of 'kubectl' changed from 0644 (rw-r--r--) to 0755 (rwxr-xr-x)
```

Instala el programa `kubectl` en el equipo

```console
usuario@laptop ~ % sudo install --verbose -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
'kubectl' -> '/usr/local/bin/kubectl'

usuario@laptop ~ % rm -v ~/kubectl
removed '/home/usuario/kubectl'

usuario@laptop ~ % ls -la /usr/local/bin/kubectl
-rwxr-xr-x 1 root root 48037888 May 22 13:18 /usr/local/bin/kubectl

usuario@laptop ~ % which kubectl
/usr/local/bin/kubectl
```

--------------------------------------------------------------------------------

## Instala `krew` en el equipo local

!!! note
    - Este programa debe ser instalado en el **equipo local** y el **equipo remoto**

El programa `krew` es un manejador de _plugins_ para `kubectl`

- <https://krew.sigs.k8s.io/docs/user-guide/quickstart/>

Crea un directorio temporal y descarga el archivo `tar.gz` de `krew`

```console
redes@example:~$ KREW_VERSION=v0.4.3
redes@example:~$ KREW_TMP_DIR=/tmp/krew

redes@example:~$ mkdir -vp ${KREW_TMP_DIR}
mkdir: created directory '/tmp/krew'

redes@example:~$ wget -c -nv -O "${KREW_TMP_DIR}/krew-${KREW_VERSION}.tar.gz" \
  "https://github.com/kubernetes-sigs/krew/releases/download/${KREW_VERSION}/krew-linux_amd64.tar.gz"

redes@example:~$ ls -la "${KREW_TMP_DIR}/krew-${KREW_VERSION}.tar.gz"
-rw-r--r-- 1 redes users 4128657 Feb  4  2022 /tmp/krew/krew-v0.4.3.tar.gz
```

Extrae el archivo `tar.gz` de `krew` en el directorio temporal

```console
redes@example:~$ tar -xvvzf ${KREW_TMP_DIR}/krew-${KREW_VERSION}.tar.gz -C ${KREW_TMP_DIR}
-rw-r--r-- runner/docker    11358 1999-12-31 18:00 ./LICENSE
-rwxr-xr-x runner/docker 11836580 1999-12-31 18:00 ./krew-linux_amd64
```

<!--
Instala el programa `krew` en el directorio `/usr/local/bin`

```console
redes@example:~$ sudo install --owner root --group root --mode 0755 ${KREW_TMP_DIR}/krew-linux_amd64 /usr/local/bin/krew

redes@example:~$ which krew
/usr/local/bin/krew

redes@example:~$ ls -la /usr/local/bin/krew
-rwxr-xr-x 1 root root 12164162 Jun  8 17:44 /usr/local/bin/krew
```
-->

Instala el programa `krew` en la cuenta del usuario

```console
redes@example:~$ ls -la ${KREW_TMP_DIR}/krew-linux_amd64
-rwxr-xr-x 1 redes users 11836580 Dec 31  1999 /tmp/krew/krew-linux_amd64

redes@example:~$ ${KREW_TMP_DIR}/krew-linux_amd64 install krew
WARNING: To be able to run kubectl plugins, you need to add
the following to your ~/.bash_profile or ~/.bashrc:

    export PATH="${PATH}:${HOME}/.krew/bin"

and restart your shell.

Adding "default" plugin index from https://github.com/kubernetes-sigs/krew-index.git.
Updated the local copy of plugin index.
Installing plugin: krew
Installed plugin: krew
\
 | Use this plugin:
 | 	kubectl krew
 | Documentation:
 | 	https://krew.sigs.k8s.io/
 | Caveats:
 | \
 |  | krew is now installed! To start using kubectl plugins, you need to add
 |  | krew's installation directory to your PATH:
 |  |
 |  |   * macOS/Linux:
 |  |     - Add the following to your ~/.bashrc or ~/.zshrc:
 |  |         export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
 |  |     - Restart your shell.
 |  |
 |  |   * Windows: Add %USERPROFILE%\.krew\bin to your PATH environment variable
 |  |
 |  | To list krew commands and to get help, run:
 |  |   $ kubectl krew
 |  | For a full list of available plugins, run:
 |  |   $ kubectl krew search
 |  |
 |  | You can find documentation at
 |  |   https://krew.sigs.k8s.io/docs/user-guide/quickstart/.
 | /
/
```

Borra el directorio temporal donde se descargó el instalador de `krew`

```console
redes@example:~$ rm -vrf ${KREW_TMP_DIR}
removed '/tmp/krew/krew-v0.4.3.tar.gz'
removed '/tmp/krew/krew-linux_amd64'
removed '/tmp/krew/LICENSE'
removed directory '/tmp/krew'
```

Agrega esta línea al final del archivo `~/.bashrc`

- Después **cierra y abre tu sesión** para que el _shell_ tome los cambios

<!--
```console
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
```
-->
```console
export PATH="${PATH}:${HOME}/.krew/bin"
```

Verifica que `~/.krew/bin` aparezca en tu `PATH`

<!--
```console
redes@example:~$ echo ${PATH}
/home/redes/.krew/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
```
-->
```console
redes@example:~$ echo ${PATH}
/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/home/redes/.krew/bin
```

Confirma que tu _shell_ pueda ubicar el programa `kubectl-krew`

```console
redes@example:~$ which kubectl-krew
/home/redes/.krew/bin/kubectl-krew
```

Verifica que puedas ejecutar el comando `kubectl krew`

<!--
```console
redes@example:~$ kubectl krew
krew is the kubectl plugin manager.
You can invoke krew through kubectl: "kubectl krew [command]..."

Usage:
  kubectl krew [command]

Available Commands:
  completion  generate the autocompletion script for the specified shell
  help        Help about any command
  index       Manage custom plugin indexes
  info        Show information about an available plugin
  install     Install kubectl plugins
  list        List installed kubectl plugins
  search      Discover kubectl plugins
  uninstall   Uninstall plugins
  update      Update the local copy of the plugin index
  upgrade     Upgrade installed plugins to newer versions
  version     Show krew version and diagnostics

Flags:
  -h, --help      help for krew
  -v, --v Level   number for the log level verbosity

Use "kubectl krew [command] --help" for more information about a command.
```
-->

```console
redes@example:~$ kubectl krew version
OPTION            VALUE
GitTag            v0.4.3
GitCommit         dbfefa5
IndexURI          https://github.com/kubernetes-sigs/krew-index.git
BasePath          /home/redes/.krew
IndexPath         /home/redes/.krew/index/default
InstallPath       /home/redes/.krew/store
BinPath           /home/redes/.krew/bin
DetectedPlatform  linux/amd64
```

Actualiza la lista de _plugins_ de `krew`

```console
redes@example:~$ kubectl krew update
Updated the local copy of plugin index.
```

Instala el _plugin_ `neat` de `krew`

```console
redes@example:~$ kubectl krew install neat
Updated the local copy of plugin index.
Installing plugin: neat
Installed plugin: neat
\
 | Use this plugin:
 | 	kubectl neat
 | Documentation:
 | 	https://github.com/itaysk/kubectl-neat
/
WARNING: You installed plugin "neat" from the krew-index plugin repository.
   These plugins are not audited for security by the Krew maintainers.
   Run them at your own risk.
```

[kubectl-krew]: https://krew.sigs.k8s.io/
[krew-quickstart]: https://krew.sigs.k8s.io/docs/user-guide/quickstart/
[krew-install]: https://krew.sigs.k8s.io/docs/user-guide/setup/install/

--------------------------------------------------------------------------------

## Prepara el archivo `~/.kube/config` en el equipo local

Copia el archivo `~/.kube/config` al equipo local, este archivo tiene la información para conectarse al servidor de Kubernetes

- Reemplaza `20.58.191.222` con la dirección IP **pública** de tu máquina virtual

```console
usuario@laptop ~ % scp -i ~/.ssh/redes_azure.pem redes@20.58.191.222:~/.kube/config ~/Downloads/redes-kube-config.yaml
```

Edita el archivo `redes-kube-config.yaml` para poner el nombre DNS de tu máquina virtual

- Reemplaza `k3s.example.com` con tu nombre de dominio (ej. `k3s.tonejito.cf`)

```console
usuario@laptop ~ % sed -i'' -e 's/127.0.0.1/k3s.example.com/g' ~/Downloads/redes-kube-config.yaml
```

!!! note
    No hay espacio entre la letra `i` y las **comillas simples** `''` en el comando `sed`

Crea el directorio `~/.kube` en el equipo local

```
usuario@laptop ~ % mkdir -vp --mode 0700 ~/.kube
mkdir: created directory '/home/usuario/.kube'
```

Copia el archivo `~/Downloads/redes-kube-config.yaml` al directorio `~/.kube` en el equipo local

```
usuario@laptop ~ % install --verbose --owner "$(id -u)" --group "$(id -g)" --mode 0640 ~/Downloads/redes-kube-config.yaml ~/.kube/config
'/home/usuario/Downloads/redes-kube-config.yaml' -> '/home/usuario/.kube/config'
```

Verifica los permisos

```
usuario@laptop ~ % ls -la ~/.kube
total 12
drwx------ 2 usuario grupo 4096 May 25 23:48 .
drwxr-xr-x 8 usuario grupo 4096 May 25 23:48 ..
-rw-r----- 1 usuario grupo 2967 May 25 23:48 config
```

## Conectarse al cluster desde el equipo local

<!--
!!! note
    - Ejecuta el siguiente comando en tu máquina para configurar temporalmente la ruta hacia el archivo de configuración de `kubectl`

```console
usuario@laptop ~ % export KUBECONFIG=~/Downloads/redes-kube-config.yaml
```

!!! warning
    - Es necesario que ejecutes el comando para crear la variable de entorno **cada vez que abras una nueva terminal**
    - Si quieres que este cambio sea persistente, incluye esa línea de comando al final del archivo `~/.bashrc` en tu usuario
-->

Verifica que te puedas conectar al puerto `6443` del servidor con `netcat`

```console
usuario@laptop ~ % nc -vz k3s.tonejito.cf 6443
k3s.tonejito.cf [20.58.191.222] 6443 (?) open
```

Verifica que puedas conectarte al servidor API de Kubernetes con el comando `kubectl`

- La versión local del cliente de Kubernetes **debe ser la misma** que la que se ejecuta en el servidor
- La opción `--short` se utiliza para dar una salida compacta del comando
- La opción `--insecure-skip-tls-verify=false` se utiliza para forzar explícitamente la verificación del certificado SSL del servidor API del cluster de Kubernetes

<!--
```console
usuario@laptop ~ % kubectl version --insecure-skip-tls-verify=false
WARNING: This version information is deprecated and will be replaced with the output from kubectl version --short.  Use --output=yaml|json to get the full version.
Client Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.4", ...
Kustomize Version: v4.5.7
Server Version: version.Info{Major:"1", Minor:"26", GitVersion:"v1.26.4+k3s1", ...
```
-->

```console
usuario@laptop ~ % kubectl version --short --insecure-skip-tls-verify=false
Flag --short has been deprecated, and will be removed in the future. The --short output will become the default.
Client Version: v1.26.4
Kustomize Version: v4.5.7
Server Version: v1.26.4+k3s1
```

Lista los nodos presentes en el cluster de Kubernetes

```console
usuario@laptop ~ % kubectl get nodes
NAME              STATUS   ROLES                  AGE  VERSION
k3s.tonejito.cf   Ready    control-plane,master   1d   v1.26.4+k3s1
```

--------------------------------------------------------------------------------

## Verifica la configuración

Reinicia el equipo local y el equipo remoto para verificar que los cambios sean persistentes

- Equipo remoto

```console
root@laptop:~# systemctl reboot
```

- Equipo local

```console
root@example:~# reboot
```



!!! danger
    - Verifica que **TODAS** las configuraciones que hiciste estén presentes respués de reiniciar la máquina antes de continuar con [la siguiente sección][siguiente]

!!! note
    - Continúa en [la siguiente página][siguiente] si el servicio de `k3s` está en línea y puedes conectarte al cluster desde tu equipo local con el comando `kubectl`

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../prepare-vm
[siguiente]: ../k8s-ingress-nginx
[azure-portal]: https://portal.azure.com/
