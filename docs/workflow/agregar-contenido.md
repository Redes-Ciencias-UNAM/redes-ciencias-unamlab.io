---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Agregar contenido a tu fork del repositorio
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Agregar contenido a tu _fork_ del repositorio

## Agregar carpeta personal

- Accede al repositorio y crea una carpeta con tu nombre bajo la ruta `docs/tareas`

```console
$ mkdir -vp docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/
mkdir: created directory ‘docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/’
```

!!! warning
    No utilices **espacios**, **acentos**, **eñe**, **diéresis** o **caracteres especiales**

!!! note
    - Dentro de esa carpeta es donde debes poner los archivos de esta tarea
    - Cada práctica y tarea tendrá su propia carpeta y debes seguir la estructura de directorios propuesta

--------------------------------------------------------------------------------

### Agregar una imagen

- Crea el directorio de imagenes

```console
$ mkdir -vp docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/
mkdir: created directory ‘docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/’

$ touch docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/.gitkeep
```

!!! note
    Se crea el archivo oculto `.gitkeep` para versionar un directorio _"vacío"_, porque `git` no permite versionar directorios

- Copia una imagen en formato `jpg`, `png` o `gif` dentro del directorio `img`

!!! warning
    - Asegúrate de que la imagen no pese más de `100 KB`

```console
$ cp  /ruta/hacia/acerca-de.png  docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/
$ ls -lA docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/acerca-de.png
-rw-r--r-- 1 tonejito staff 65535 Sep 28 00:10 docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/acerca-de.png
```

!!! note
    El nombre de tu imagen segúramente es diferente, pero debe estar dentro de la capeta `img`

--------------------------------------------------------------------------------

### Agregar archivo con tu nombre

- Crea el archivo `.gitkeep` y un archivo `README.md` con el contenido de muestra
    - Agrega una referencia a la imagen que incluiste en el paso anterior para que se muestre dentro del archivo `README.md`
    - Puedes agregar otros campos si gustas

```console
$ touch  docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/README.md
$ editor docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/README.md
```

!!! note
    Reemplaza el comando `editor` con el editor de texto de tu preferencia

    - Puedes utilizar el [IDE de GitLab][gitlab-ide-live-preview] que cuenta con una función de [previsualización de Markdown][gitlab-markdown-live-preview]
    - También puedes visualizar cómo se convierte de Markdown a HTML en algún sitio como [stackedit.io][stackedit]

- Contenido de ejemplo para el archivo `README.md`
    - Este archivo contiene un título, algunas listas, una liga a una URL externa y una referencia a una imágen

```console
# Andrés Hernández

- Número de cuenta: `123456789`
- Usuario de GitLab: `@tonejito`
- [Tarea-1][liga-tarea-1]

Hola, estas son las caracteristicas de mi computadora:

| Elemento          | Valor
|:-----------------:|:-----:|
| CPU               | Intel Core i5
| Arquitectura      | x86_64
| RAM               | 8 GB
| Disco             | 256 GB
| Sistema Operativo | Ubuntu 22.04 LTS `jammy`
| Kernel            | `6.1.0-1004-oem`

Capturas de pantalla:

| ![](img/acerca-de.png)
|:----------------------:|
| Esta es la pantalla que muestra los detalles de mi sistema

[liga-tarea-1]: https://redes-ciencias-unam.gitlab.io/2023-2/tareas-redes/tareas/tarea-1/
```

!!! note
    El nombre de tu imagen seguramente es diferente, pero debe estar dentro de la capeta `img`

--------------------------------------------------------------------------------

## Probar los cambios localmente

Las cuentas creadas después de mayo de 2021 tienen restricciones para ejecutar los _pipelines_ de CI/CD de manera remota en la infraestructura de GitLab.

Esta restricción fue implementada porque [había muchos proyectos que hacian uso de los _pipelines_ para minar criptomonedas][gitlab-prevent-crypto-mining-abuse] y GitLab decidió poner límites para evitar el abuso de las cuentas.

No hay restricción para ejecutar los _pipelines_ si creaste tu cuenta antes de mayo de 2021.
En caso de que tu cuenta sea reciente, tendrás que ejecutar el equivalente del _pipeline_ de manera local para revisar si hay algún error en tu documentación.

Puedes [registrar tu proyecto público como código abierto][gitlab-opensource-perks], si así lo decides y [obtener ciertos beneficios][gitlab-saas-free-tier].
Otra opción es que le informes a GitLab cómo es que te afecta [utilizando esta encuesta][gitlab-crypto-mining-abuse-control-survey].

### Ejecutar `mkdocs` localmente

Se ejecuta `mkdocs` de manera local para visualizar errores, `mkdocs` se instala de manera local si es necesario:

```
$ make
which mkdocs || \
pip3 install --user --requirement requirements.txt
/home/tonejito/.local/bin/mkdocs
mkdocs serve --strict --verbose
INFO    -  Building documentation...
INFO    -  Cleaning site directory
WARNING -  Documentation file 'workflow/README.md' contains a link to 'workflow/img/000-workflow.png' which is not found in the documentation files.

Exited with 1 warnings in strict mode.
make: *** [Makefile:17: serve] Error 1
```

En este caso el mensaje de error muestra que la imagen `workflow/img/000-workflow.png` no existe y esta siendo referenciada en el archivo `workflow/README.md`.

Ejecutar `make` después de resolver los errores detectados por `mkdocs`.

```
$ make
which mkdocs || \
pip3 install --user --requirement requirements.txt
/home/tonejito/.local/bin/mkdocs
mkdocs serve --strict --verbose
INFO    -  Building documentation...
INFO    -  Cleaning site directory
INFO    -  Documentation built in 0.31 seconds
[I 210310 01:02:03 server:335] Serving on http://127.0.0.1:8000
INFO    -  Serving on http://127.0.0.1:8000
[I 210310 01:02:03 handlers:62] Start watching changes
INFO    -  Start watching changes
[I 210310 01:02:03 handlers:64] Start detecting changes
INFO    -  Start detecting changes

	...
```

Si los conflictos fueron resueltos, el sitio será visible en <http://localhost:1414/>

--------------------------------------------------------------------------------

## Envía los cambios a tu repositorio

- Una vez que hayas creado los archivos, revisa el estado del repositorio

```console
$ git status
On branch tarea-1
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/

nothing added to commit but untracked files present (use "git add" to track)
```

- Arega los archivos de tu carpeta con `git add`

```console
$ git add docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/
```

- Revisa que los archivos hayan sido agregados al _staging area_ utilizando `git status`

```console
$ git status
On branch tarea-1
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/README.md
	new file:   docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/.gitkeep
	new file:   docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/acerca-de.png
```

- Versiona los archivos con `git commit`

!!! warning
    - Utiliza **comillas simples** para especificar el mensaje del _commit_
    - Evita utilizar comillas dobles porque el _shell_ interpreta algunos caracteres como `*`, <code>&#96;</code>, etc.

```console
$ git commit -m 'Carpeta de Andrés Hernández'
[tarea-1 cb7fe99] Carpeta de Andrés Hernández
 3 files changed, 22 insertions(+)
 create mode 100644 docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/README.md
 create mode 100644 docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/.gitkeep
 create mode 100644 docs/tareas/tarea-1/HernandezBermudezAndresLeonardo/img/acerca-de.png
```

- Revisa que el _remote_ apunte a **tu repositorio** con `git remote`

```console
$ git remote -v
origin	https://gitlab.com/USUARIO/tareas-redes.git (fetch)
origin	https://gitlab.com/USUARIO/tareas-redes.git (push)
```

!!! danger
    Si la salida del comando anterior **no apunta a tu _fork_ del repositorio**, tendras que borrar el directorio y volver a repetir los pasos desde la sección "[clona el repositorio](#clona-el-repositorio)"

- Revisa la rama en la que estas para enviarla a GitLab

```console
$ git branch
* tarea-1
  entregados
  entregas
  main
```

- Envía los cambios a **tu repositorio** utilizando `git push`

```console
$ git push -u origin tarea-1
Username for 'https://gitlab.com': USUARIO
Password for 'https://USUARIO@gitlab.com':
Enumerating objects: 12, done.
Counting objects: 100% (12/12), done.
Delta compression using up to 12 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (10/10), 63.63 KiB | 1.0 MiB/s, done.
Total 10 (delta 1), reused 1 (delta 0), pack-reused 0
remote:
remote: To create a merge request for tarea-1, visit:
remote:   https://gitlab.com/USUARIO/tareas-redes/-/merge_requests/new?merge_request%5Bsource_branch%5D=tarea-1
remote:
To https://gitlab.com/USUARIO/tareas-redes.git
 * [new branch]      tarea-1 -> tarea-1
Branch 'tarea-1' set up to track remote branch 'tarea-1' from 'origin'.
```

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando hayas agregado el contenido a tu _fork_ y enviado los cambios a tu repositorio en GitLab

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
| [Página anterior][anterior] | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../configurar-precommit
[arriba]: ../../workflow
[siguiente]: ../crear-merge-request

[gitlab-prevent-crypto-mining-abuse]: https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/
[gitlab-opensource-perks]: https://about.gitlab.com/blog/2022/02/04/ultimate-perks-for-open-source-projects/
[gitlab-crypto-mining-abuse-control-survey]: https://docs.google.com/forms/d/e/1FAIpQLSeh0UbdhHtw1lGTdJt1wZNXqGKt3oKdzgAi962egNyP45vEOA/viewform
[gitlab-saas-free-tier]: https://about.gitlab.com/pricing/faq-efficient-free-tier/#public-projects-on-gitlab-saas-free-tier

[gitlab-ide-live-preview]: https://docs.gitlab.com/ee/user/project/web_ide/#live-preview
[gitlab-markdown-live-preview]: https://about.gitlab.com/blog/2021/09/21/introducing-markdown-live-preview/
[stackedit]: https://stackedit.io/app#
