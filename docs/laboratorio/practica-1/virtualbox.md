---
title: Instalación de VirtualBox en la máquina física
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Instalación de VirtualBox en la máquina física

!!! note
    - Si ya instalaste tu VirtualBox y tienes el adaptador de red `vboxnet*`, crea tus máquinas virtuales [la siguiente página](../debian-install)

<!-- -->

## Extensiones de virtualización

Habilitar las extensiones de virtualización en el BIOS o configuración de UEFI

!!! note
    Esto depende de la máquina, consultar el manual de servicio

### Linux

- En Linux revisar las características del CPU

```console
tonejito@linux:~$ grep --color 'vmx' /proc/cpuinfo | tail -n 1
```

### macOS

- En macOS revisar si el procesador tiene la característica `VMX`

```console
tonejito@macOS ~ % sysctl -a | grep 'machdep.cpu.features:' | grep --color=auto 'VMX'
machdep.cpu.features: FPU VME DE PSE TSC MSR PAE MCE CX8 APIC SEP MTRR PGE MCA
CMOV PAT PSE36 CLFSH DS ACPI MMX FXSR SSE SSE2 SS HTT TM PBE SSE3 PCLMULQDQ
DTES64 MON DSCPL VMX SMX EST TM2 SSSE3 FMA CX16 TPR PDCM SSE4.1 SSE4.2 x2APIC
MOVBE POPCNT AES PCID XSAVE OSXSAVE SEGLIM64 TSCTMR AVX1.0 RDRAND F16C
```

### Windows

- En Windows se puede ver si está habilitado utilizando el administrador de tareas

![](img/windows-task_manager-virt.png)

- Otra opción es ejecutar el siguiente comando en PowerShell

```PowerShell
PS C:\> Get-ComputerInfo -property "HyperV*"

HyperVisorPresent                                 : True
HyperVRequirementDataExecutionPreventionAvailable : True
HyperVRequirementSecondLevelAddressTranslation    : True
HyperVRequirementVirtualizationFirmwareEnabled    : True
HyperVRequirementVMMonitorModeExtensions          : True
```

<!-- -->

## Instalación de VirtualBox

- Descargar el instalador de VirtualBox de la página oficial

    - <https://www.virtualbox.org/wiki/Downloads>
    - <https://www.virtualbox.org/wiki/Linux_Downloads>

| Sistema operativo | Arquitectura | Archivo |
|:---------:|:----------------------:|:-----:|
| GNU/Linux | `x86_64` (`amd64`)     | _Instalar utilizando el gestor de paquetes_: <br/> [`apt` : Debian / Ubuntu][virtualbox-downloads-linux-rpm] <br/> [`yum`/`dnf` : CentOS / Rocky Linux / Alma Linux][virtualbox-downloads-linux-apt]
| macOS     | `x86_64` (`amd64`)     | `VirtualBox-*-*-OSX.dmg`
| macOS     | [Apple Silicon][apple-silicon] (`aarch64`) | `VirtualBox-*_BETA*-*-macOSArm64.dmg` <br/> **Developer Preview**
| Windows   | `x86_64` (`amd64`)     | `VirtualBox-*-*-Win.exe`

[virtualbox-downloads]: https://www.virtualbox.org/wiki/Downloads
[virtualbox-downloads-linux]: https://www.virtualbox.org/wiki/Linux_Downloads
[virtualbox-downloads-linux-apt]: https://www.virtualbox.org/wiki/Linux_Downloads#Debian-basedLinuxdistributions
[virtualbox-downloads-linux-rpm]: https://www.virtualbox.org/wiki/Linux_Downloads#RPM-basedLinuxdistributions
[apple-silicon]: https://support.apple.com/en-us/HT211814

<a id="vbox-extpack" name="vbox-extpack">

- Descargar el "VirtualBox Oracle VM VirtualBox Extension Pack" de la misma página

    - `Oracle_VM_VirtualBox_Extension_Pack-*-*.vbox-extpack`

### GNU/Linux

#### Debian / Ubuntu

Instalar las dependencias de desarrollo y las cabeceras del kernel

```console
# apt install build-essential dkms linux-headers-amd64 linux-headers-$(uname -r)
# apt-mark auto linux-headers-$(uname -r)
```

Agregar el repositorio de Oracle VirtualBox siguiendo las instrucciones oficiales

- <https://www.virtualbox.org/wiki/Linux_Downloads#Debian-basedLinuxdistributions>

Actualizar la lista de paquetes disponibles

```console
# apt update
```

Configurar temporalmente la [prioridad de `debconf`][debconf-priority] utilizando la [variable de entorno `DEBIAN_PRIORITY`][debconf-debian-priority]:

[debconf-priority]: https://manpages.debian.org/bullseye/debconf-doc/debconf.7.en.html#Priorities
[debconf-debian-priority]: https://manpages.debian.org/bullseye/debconf-doc/debconf.7.en.html#DEBIAN_PRIORITY

```console
# export DEBIAN_PRIORITY=low
```

!!! note
    Es necesario _exportar_ esta variable antes de ejecutar `apt`

Instalar la versión más reciente de VirtualBox disponible en el repositorio

```console
# apt install virtualbox-7.0
```

!!! warning
    Asegúrate de que el instalador te pregunte si quieres permitir que los usuarios miembros del grupo `vboxusers` puedan crear máquinas virtuales, de lo contrario tendrás que reconfigurar el paquete `virtualbox-7.0`.

Agrega a tu usuario al grupo `vboxusers` que fue creado por el instalador de VirtualBox

```console
# usermod -aG vboxusers tonejito
	...
# id tonejito
	...	vboxusers
```

Compila los módulos del kernel para la máquina física

```console
# /sbin/vboxconfig
```

!!! note
    Consulta el archivo de bitácora `/var/log/vbox-install.log` si hay algún error

Restaurar la prioridad de `debconf`

```console
# unset DEBIAN_PRIORITY
```

Reinicia la computadora

```console
# systemctl reboot
```

Lista los módulos cargados en el _kernel_, debería aparecer por lo menos el módulo `vboxdrv`

```console
# lsmod | egrep -i 'v(irtual)?box'
	...
```

| Módulo       | Descripción
|:------------:|:------------
| `vboxdrv`    | Módulo principal de VirtualBox para la máquina física
| `vboxnetadp` | Módulo que genera los adaptadores de red `vboxnet*`
| `vboxnetflt` | Módulo que maneja las redes **NAT** e **internal network**
| `vboxpci`    | Módulo que pasa dispositivos PCI de la máquina física a la máquina virtual

Verifica que tu usuario pueda acceder al programa `VirtualBox`

```console
$ VirtualBox &
	...
```

#### RHEL / CentOS Stream / RockyLinux / AlmaLinux

- <https://www.virtualbox.org/wiki/Linux_Downloads#RPM-basedLinuxdistributions>

## Instalación del _Extension Pack_

- Da doble clic en el archivo del _Extension Pack_ para instalarlo utilizando la interfaz gráfica

- Utiliza el programa [`VBoxManage`][vboxmanage-extpack] en la línea de comandos para instalar el _Extension Pack_

```console
# VBoxManage extpack install  Oracle_VM_VirtualBox_Extension_Pack-*-*.vbox-extpack
	...
```

[vboxmanage-extpack]: https://www.virtualbox.org/manual/ch08.html#vboxmanage-extpack

--------

### Configuración de VirtualBox

### Creación del adaptador de red _host-only_

Da clic en el menú **File** y selecciona **Host Network Manager**.

|      |
|:----:|
| ![](img/virtualbox/virtualbox-010-host-network-manager-menu.png)

Da clic en el botón **Create** para agregar un nuevo adaptador de red sólo anfitrión (_host-only_).

|      |
|:----:|
| ![](img/virtualbox/virtualbox-011-host-network-manager-create-adapter.png)

El nuevo adaptador aparecerá en la lista.
En este caso el adaptador es `vboxnet2`.
Asegúrate de habilitar la casilla de **DHCP Server**.

|      |
|:----:|
| ![](img/virtualbox/virtualbox-012-host-network-manager-adapter-created.png)

Abre la configuración de tu máquina virtual y asigna el adaptador sólo anfitrión a la **segunda** interfaz de red.
Guarda la configuración para preservar los cambios.

|      |
|:----:|
| ![](img/virtualbox/virtualbox-013-vm-settings-network-adapter.png)
