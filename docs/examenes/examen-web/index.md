---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Examen 3 - Implementación de un sitio web de producción
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Examen 3: Implementación de un sitio web de producción

--------------------------------------------------------------------------------

## Objetivos

- Instalación y configuración de un servidor de aplicaciones
- Creación del VirtualHost para la aplicación web
- Instalación de una aplicación web y configuración de ambiente productivo
- Instalación y configuración de la base de datos
- Configuración de características adicionales en el ambiente productivo

## Elementos de apoyo

!!! note
    Todos estos videos están en una [lista de reproducción dedicada a los temas de HTTP y SSL 📹][playlist-https]

- [Protocolo DNS 📼][video-protocolo-dns]
- [Configuración de OpenSSH y autenticación con llaves 📼][video-configuracion-ssh]
- [Configuración de Apache HTTPD en Debian 📼][video-configuracion-apache-debian]
- [Directivas de configuración de Apache HTTPD 📼][video-directivas-apache]
- [Configuración de VirtualHosts de Apache HTTPD 2.4 utilizando /etc/hosts 📼][video-virtualhosts-apache-etc-hosts]
- [Configuración de VirtualHosts de Apache HTTPD 2.4 con registros DNS 📼][video-virtualhosts-apache-registros-dns]
- [Certificados SSL x509 📼][video-certificados-ssl-x509]
- [Certificados SSL con OpenSSL y VirtualHost HTTPS en Apache HTTPD 📼][video-certificados-ssl-virtualhost-https-apache]
- [Trámite de un certificado SSL con Let's Encrypt utilizando certbot 📼][video-letsencrypt-certbot]

<br/>

???+ details "Páginas de manual de Apache HTTPD"

    - [Apache HTTP Server - Documentation - Version 2.4][apache-docs]
    - [Configuration Sections][apache-docs-config-sections]
    - [Security Tips][apache-docs-security]
    - [Server-Wide Configuration][apache-docs-server-wide]
    - [URL Rewriting with mod_rewrite][apache-docs-url-rewrite]
    - [Apache Virtual Host documentation][apache-docs-virtualhost]
    - [Apache SSL/TLS Encryption][apache-docs-ssl]
    - [Apache HTTP Server Tutorial: .htaccess files][apache-docs-htaccess]

## Restricciones

!!! danger
    - La evaluación de esta actividad corresponderá al **tercer examen parcial** del curso

!!! warning
    - **Esta actividad depende de los recursos implementados en la [práctica 8](../../laboratorio/practica-8) y [práctica 9](../../laboratorio/practica-9)**
    - Se recomienda que se realicen las actividades previas [siguiendo la calendarización][pagina-notas] con el objeto de dejar suficiente tiempo para la elaboración de este examen práctico

[pagina-notas]: https://redes-ciencias-unam.gitlab.io/2023-2/notas/

- La fecha límite de entrega es el **martes 23 de mayo de 2023** a las 23:59 horas
- Esta actividad debe ser entregada **en equipo** de acuerdo al [flujo de trabajo para la entrega de tareas y prácticas][flujo-de-trabajo]
    - Utilizar la carpeta `docs/examenes/examen-web/Equipo-ABCD-EFGH-IJKL-MNOP` para entregar la práctica
    - Donde `Equipo-ABCD-EFGH-IJKL-MNOP` representa el nombre del equipo que debió anotarse previamente en la [lista del grupo][lista-redes]
    - Hacer un _merge request_ a la rama `examen-web` del [repositorio de tareas][repo-tareas] para entregar la actividad

<!--
- Cada equipo tendrá que configurar uno de los servicios de red que se describen a continuación y anotar su elección en [la hoja de cálculo compartida][proyecto-web]

- Cada equipo tendrá que configurar una de las opciones presentadas en la [siguiente tabla](#tabla) y anotar su elección en [la hoja de cálculo compartida][proyecto-web]
-->

- No se permite tener proyectos repetidos. Revisar los requerimientos de la aplicación o sistema elegido para seleccionar la base de datos y motor de caché en memoria (si aplica).

--------------------------------------------------------------------------------

<!-- ## Implementación de un _stack_ web -->
## Instalación de una aplicación web en un ambiente productivo

### Tabla de definición de proyectos

<a id="tabla" name="tabla"></a>

<!--
| Lista de proyectos
|:-------------------------------------:|
| ![Proyecto web](img/proyecto-web.png)
-->

| #  | Sistema		| Motor PHP		| Base de datos			| Característica<br/>1		| Característica<br/>2				| Característica<br/>3
|:--:|:----------------:|:---------------------:|:-----------------------------:|:-----------------------------:|:---------------------------------------------:|:----------------:|
|  1 | WordPress	| PHP Debian FPM	| MariaDB Debian		| WP Offload Media Lite (S3)	| (_Ninguno_)					| Correo AWS SES
|  2 | WordPress	| PHP Debian `mod_php`	| MariaDB 10.11 APT		| `wp-cli`			| Instalador (`bash`)				| Correo AWS SES<br/>**Postfix**
|  3 | WordPress	| PHP Sury FPM		| MariaDB Debian		| `wp-cli`			| CRON respaldo `rclone`			| Correo AWS SES<br/>**Postfix**
|  4 | WordPress	| PHP Sury FPM		| MariaDB Debian		| Memcache / Redis		| Caché PHP:<br/>APC / APCu			| Correo AWS SES
|  5 | WordPress	| PHP Sury FPM		| MariaDB 10.11 APT		| wp2static / WP Super Cache / Simply Static	| Caché PHP:<br/>Zend OPcache	| Correo AWS SES
|  6 | WordPress	| PHP Sury FPM		| MySQL Community 8.0 APT	| Aplicación móvil		| Monitoreo externo				| Correo AWS SES
|  7 | WordPress	| PHP Sury `mod_php`	| MariaDB 10.11 APT		| Bitácoras SYSLOG		| NewRelic PHP					| Correo AWS SES<br/>**Postfix**
|  8 | WordPress	| PHP Debian `mod_php`	| MySQL Community 8.0 APT	| Multi-sitio			| Autenticación `digest`<br/>para `wp-admin`	| Correo AWS SES
|  9 | WordPress	| PHP Debian `mod_php`	| MariaDB Debian		| CloudFlare			| CAPTCHA / 2FA					| Correo AWS SES
| 10 | WordPress	| PHP Debian `mod_php`	| MariaDB Debian		| WebMin			| VirtualMin					| Correo AWS SES
| 11 | WordPress	| PHP Debian FPM	| MySQL Community 8.0 APT	| vHost para PHPMyAdmin con autenticación `htpasswd`	| Replicación MySQL	| Correo AWS SES

!!! warning
    - Revisar la [página de características adicionales](./caracteristicas) donde viene descrita cada una.

### Tabla de asignación de equipos

<!--
- Elegir una combinación de la [tabla](#tabla) en [la hoja de cálculo compartida][proyecto-web]
-->

| #  | Equipo				| Dominio DNS
|:--:|---------------------------------:|:--------------------------------
|  1 | Equipo-AVJA			|                  [`zinclemonade.me`](https://aplicacion.zinclemonade.me/)
|  2 | Equipo-AGKI-BHD-DTGM-NJMR-SVEL	|       [`AGKI-BHD-DTGM-NJMR-SVEL.me`](https://aplicacion.AGKI-BHD-DTGM-NJMR-SVEL.me/)
|  3 | Equipo-GCJ-MAVF-RGA-YZJP		|                    [`jpyamamoto.com`](https://aplicacion.jpyamamoto.com/)
|  4 | Equipo-DAE-RMDE-ZCDA-NVR-VMJM	| [`equipo-dae-rmde-zcda-nvr-vmjm.tech`](https://aplicacion.equipo-dae-rmde-zcda-nvr-vmjm.tech/)
|  5 | Equipo-BRRA-GTHL-MMM-OGA-VGM	|                      [`boruroad.me`](https://aplicacion.boruroad.me/)
|  6 | Equipo-NNA-PSJ-RFOD-RRI		|                    [`redeslocas.rocks`](https://aplicacion.redeslocas.rocks/)
|  7 | Equipo-AGR-HADB-HJFD-TEJC	|              [`saquenelfortnite.games`](https://aplicacion.saquenelfortnite.games/)
|  8 | Equipo-AAR-ATDI-BME-DAAV-LMAM	|                     [`waningnew.me`](https://aplicacion.waningnew.me/)
|  9 | Equipo-ACAA-MPKA-PMJM-RRSA-VGI	|                        [`merino.codes`](https://aplicacion.merino.codes/)
| 10 | Equipo-CCU-MGR-PCES-RSGA		|                    [`redes-urgs.me`](https://aplicacion.redes-urgs.me/)
| 11 | Equipo-ANLE-GGJ-GVII-LPEE-SCMA	|                     [`iangarcia.me`](https://aplicacion.iangarcia.me/)

### Procedimiento

- Instalar y configurar la aplicación web para que se conecte a la base de datos y que utilice la configuración solicitada de PHP

  - Montar la aplicación en el directorio `/opt/wordpress`

  - Crear un VirtualHost que responda al dominio `proyecto.example.com` y que sirva únicamente para redireccionar las peticiones HTTP y HTTPS al dominio `aplicacion.example.com`.

  - Configurar el VirtualHost de la aplicación para que responda en el dominio `aplicacion.example.com`

<!--
  - Crear un VirtualHost para que la aplicación responda en los dominios `proyecto.example.com` y `aplicacion.example.com` sobre HTTP y HTTPS

    !!! note
        Algunas aplicaciones únicamente responden en un nombre de dominio, si este es el caso habilitar la redirección del dominio `proyecto.example.com` hacia `https://aplicacion.example.com/`
-->
<!--
```
# Redirecciona "el otro nombre de dominio" al dominio de la aplicacion por HTTPS
<VirtualHost *:80>
  ServerName proyecto.tonejito.cf
  # Usa Redirect al vhost de la aplicación o pon la configuracion de HSTS
  Redirect / https://aplicacion.tonejito.cf/
</VirtualHost>

# Redirecciona "el otro nombre de dominio" al dominio de la aplicacion por HTTPS
<VirtualHost *:443>
  ServerName proyecto.tonejito.cf
  Redirect / https://aplicacion.tonejito.cf/
  # FIXME agregarDocumentRoot LogLevel, ErrorLog, CustomLog, certificado y llave
</VirtualHost>

# Redirecciona la aplicación de HTTP a HTTPS
<VirtualHost *:80>
  ServerName aplicacion.tonejito.cf
  # Usa Redirect al vhost de la aplicación o pon la configuracion de HSTS
  Redirect / https://aplicacion.tonejito.cf/
</VirtualHost>

# Este VHOST tiene la configuraion de la apliacion
<VirtualHost *:443>
  ServerName aplicacion.tonejito.cf
  # FIXME agregar DocumentRoot, LogLevel, ErrorLog, CustomLog, certificado y llave
  # TODO: Config apliacion
</VirtualHost>
```
-->

  - No utilizar los VirtualHosts predeterminados ni ningún otro que se haya creado en la [práctica 9](../../laboratorio/practica-9) para HTTP ni HTTPS

    !!! note
        Los sitios estáticos creados en la [práctica 9](../../laboratorio/practica-9) deben seguir funcionando

<!--
- Proteger la _sección administrativa_ o _sección de usuarios autenticados_ del sitio utilizando [autenticación de tipo `digest`][apache-auth-digest]

    !!! note
        Cada sitio tiene su propia ruta de la sección administrativa o la sección donde inician sesión los usuarios, consultar la documentación de cada _software_. Ejemplo:

        - `https://proyecto.example.com/login`
        - `https://aplicacion.example.com/admin`
        - `https://proyecto.example.com/user`
        - `https://aplicacion.example.com/manage`

        Al configurar la autenticación _digest_ el usuario tendrá que introducir las credenciales _digest_ en el navegador para siquiera poder visualizar la página de inicio de sesión y ahí introducir sus credenciales para acceder a la aplicación.
-->

- El sitio debe tener un certificado **wildcard** SSL emitido por Let's Encrypt y se debe utilizar el mismo nombre de dominio que en la [práctica 9](../../laboratorio/practica-9)

- El sitio debe hacer redirección de todas las peticiones HTTP hacía su versión en HTTPS

    - Se pueden usar redirecciones estándar `301` y `302` de HTTP

    - Utilizar la directiva [`Redirect`][apache-redirect-https] o la configuración de [`mod_rewrite`][apache-rewrite-https] (**pero no ambas porque son excluyentes entre si**)

<!--
- Habilitar el soporte de `userdir` donde cada usuario tenga en su directorio `HOME` una carpeta llamada [`public_html`][apache-userdir] o `public_tomcat` que sirva para que el usuario suba sus archivos y que estén disponibles en `/~usuario` en el servidor

```
$ echo '<html><body>Carpeta userdir</body></html>' > /home/redes/public_html/index.html
```

    - Ej. `/home/redes/public_html` ⇨ `https://example.com/~redes`

    !!! note
        Configurar el módulo `userdir` para que **únicamente** sirva los directorios `public_html` (o `public_tomcat`) en el VirtualHost `default_ssl` (`_default_:443`) del dominio principal `example.com` que transmite los datos utilizando HTTPS
-->

--------------------------------------------------------------------------------

## Envío de correo electrónico a través de un _relay_

Se utilizará el _relay_ de correo de AWS SES para el envío de correo electrónico a través del protocolo SMTP

Se mencionará a los alumnos la opción que deben implementar:

- Seguir el procedimiento para [configurar el MTA local con Postfix](./aws-ses) y que utilize el [_relay_ SMTP de AWS SES][aws-ses-postfix]

- Otra opción es configurar el remitente y las credenciales SMTP directamente en la aplicación web.

!!! danger
    Las credenciales de SMTP e información de S3 están en la [carpeta compartida de Google Drive][carpeta-drive]

<!--
--------------------------------------------------------------------------------

## Servidor de monitoreo

- El sitio debe tener un certificado **wildcard** SSL emitido por Let's Encrypt y se debe utilizar el mismo nombre de dominio que en la [práctica 9](../../laboratorio/practica-9)

- El sitio debe hacer redirección de todas las peticiones HTTP hacía su versión en HTTPS

    - Se pueden usar redirecciones estándar 301 y 302 de HTTP

    - Utilizar la directiva [`Redirect`][apache-redirect-https] o la configuración de [`mod_rewrite`][apache-rewrite-https] (pero no ambas porque son excluyentes entre si)

- Instalar un servidor de monitoreo mediante el software Nagios o Icinga

  - Crear un VirtualHost para que la aplicación responda en los dominios `proyecto.example.com` y `aplicacion.example.com` sobre HTTP y HTTPS

  - No utilizar los VirtualHosts predeterminados para HTTP ni HTTPS

- Se debe configurar el servidor para llevar a cabo el monitoreo de estado de los siguientes servicios de red:

    - DNS
    - HTTP y HTTPS
    - IMAP
    - SMTP
    - MemCache
    - Redis
    - MySQL
    - PostgreSQL
    - Validez del certificado SSL
    - Expiración del dominio

- Generar alertas en caso de falla y avisos de recuperación de los servicios utilizando scripts guardados en el directorio `/usr/local/bin`

    - [Telegram][api-telegram]: Crear un canal y publicar un mensaje cada que un host o servicio cambie de estado
    - [Twitter][api-twitter]: Crear una nueva cuenta y publicar un _tweet_ cada que un host o servicio cambie de estado

- Los elementos que se deberán implementar en este servicio se muestran en el diagrama:

|                                                   |
|:-------------------------------------------------:|
| ![Proyecto monitoreo](img/proyecto-monitoreo.png) |
-->

--------------------------------------------------------------------------------

## Entregables

- Enviar el [reporte](#reporte) de la implementación en un _merge request_

- Subir los [archivos adjuntos](#archivos-adjuntos) solicitados a la [carpeta compartida de Google Drive][carpeta-drive]

### Reporte

Hacer un tutorial que explique cómo se implementó el sitio web asignado:

- Explicar **brevemente** la instalación del _stack_ web en la máquina virtual de la nube:
    - Registros DNS
    - VirtualHost de Apache HTTPD
    - Instalación y configuración de PHP
    - Instalación y configuración de MySQL/MariaDB

- Especificar **DETALLADAMENTE** cómo se implementaron las [características adicionales](./caracteristicas) en la instalación de WordPress.

- Explicar **brevemente** cómo se configuró el envío de correo a través de AWS SES ya sea [en Postfix](./aws-ses) o directamente [en WordPress][aws-ses-software].

!!! note
    - Subir las imagenes necesarias al directorio `img`

### Archivos adjuntos

<!-- -->
Subir a la [carpeta compartida de Google Drive][carpeta-drive] los siguientes archivos de respaldo que sustentan el trabajo elaborado en el desarrollo del proyecto:

- Par de llaves SSH con las que se accedió al servidor en un archivo `TAR` (archivos `equipo_redes_rsa` y `equipo_redes_rsa.pub`)

- Lista de usuarios y contraseñas para acceder a la aplicación web, base de datos y demás, en un archivo de texto llamado `accesos.txt`

- Archivo `TAR` con el respaldo del directorio `/etc/letsencrypt` donde está el certificado y llave privada que utilizan los sitios web hospedados

- Respaldo de configuraciones de todo el servidor en un archivo `TAR` (directorio `/etc`)

- Respaldo de configuraciones de Apache HTTPD en un archivo `TAR` (directorio `/etc/apache2`)

- Respaldo de configuraciones de MySQL/MariaDB en un archivo `TAR` (directorio `/etc/mysql` o `/etc/mariadb`)

- Respaldo de configuraciones de PHP en un archivo `TAR` (directorio `/etc/php` o `/etc/php-fpm`)

- Respaldo de bitácoras del sistema en un archivo `TAR` (directorio `/var/log`)

- Respaldo de la aplicación web en un archivo `TAR` (directorio `/opt/wordpress`)

- Respaldo de la base de datos utilizada en formato SQL comprimido con `gzip`, `bzip2` o `7zip`

#### Otros archivos adjuntos

Subir los siguientes archivos <u>**si es que aplican para el proyecto que fue asignado**</u>:

- Respaldo del _bucket_ de S3 en formato `TAR`

- Archivo del _script_ de instalación escrito en `bash`

- Archivo del _script_ de respaldo y configuración de CRON para respaldos

- Respaldo del sitio web estático en un archivo `TAR` (directorio `/opt/estatico`)

- Respaldo de la configuración de `rsyslog`

- Respaldo de la configuración de NewRelic para PHP

- Respaldo de los archivos de autenticación para Apache HTTPD generados con `htpasswd(1)` y/o `htdigest(1)`

- Respaldo de la aplicación web PHPMyAdmin en un archivo `TAR` (directorio `/opt/phpmyadmin`)

- Respaldo de la configuración de WebMin y VirtualMin

- Configuración de replicación para MySQL/MariaDB

<!-- -->

<!--
### General
-->

<!--
- Ver el siguiente video y emitir un comentario sobre la relación del contenido presentado y los conceptos utilizados en este proyecto

    - [Seguridad en AWS - AWS Public Sector Summit Mexico City 2020 📼][youtube-video-aws-summit-2020-cdmx-seguridad]
-->
<!--
    - [Revitalize your security with the AWS Security Reference Architecture (SEC203) - AWS re:Invent 2022 📼][youtube-video-aws-reinvent-2022-security]
-->

<!--
### Para los proyectos del _stack_ web
-->

<!-- - Subir adicionalmente a la carpeta compartida: -->
<!--

-->
<!--
### Para el proyecto de monitoreo

- Subir adicionalmente a la carpeta compartida:

    - Respaldo de los archivos de trabajo del sistema de monitoreo en un archivo `TAR`

        - Nagios: directorio `/var/lib/nagios4`

        - Icinga: directorio `/var/lib/icinga2`

    - Respaldo de los _scripts_ utilizados para enviar las alertas por Telegram y Twitter (directorio `/usr/local/bin`)
-->

--------------------------------------------------------------------------------

## Extra

<!--
### General
-->

<!--
- [Crear una imágen AMI][aws-ec2-ami] de la instancia EC2 y [compartirla con los profesores][aws-ec2-ami-share]

    - Cuenta de AWS `374417498684`
-->

- Implementar [**HTTP Strict Transport Security (HSTS)**][hsts] en las cabeceras del sitio para forzar a que se pida el contenido del sitio a través de HTTPS

    - Establecer estas configuraciones en los VirtualHosts de la aplicación para evitar conflictos con la configuración de los otros VirtualHosts de los demás sitios implementados en las prácticas anteriores

    - Establecer un timeout bajo de entre `60` y `300` segundos para probar que HSTS funciona

    - Probar esta configuración en el VirtualHost de HTTP y HTTPS para ver la configuración que se debe dejar

    - Deshabilitar la redirección de HTTP a HTTPS si se está habilitando **HTTP Strict Transport Security (HSTS)**

- Implementar la cabecera [`X-Robots-Tag`][x-robots-tag] para evitar que los motores de búsqueda indexen el sitio

<!--
### Para los proyectos del _stack_ web

- Respaldo (movido a la sección de implementación)
-->

<!--
### Para el proyecto de monitoreo

- El sistema de monitoreo instala una autenticación de tipo `basic` de manera predeterminada, [cambiar el tipo de autenticación a `digest`][apache-auth-digest]

- Automatizar el respaldo de las bitácoras del servidor y los archivos de trabajo de Nagios o Icinga

    - Script y configuración de la tarea programada para respaldar la base de datos [**diario a media noche**][cron]

    - Creación de archivo `TAR` con las bitácoras del servidor

    - Creación de archivo `TAR` con los archivos de trabajo de Nagios o Icinga
-->
--------------------------------------------------------------------------------

## Notas adicionales

- No se aceptan instalaciones que provengan de _scripts_ que automaticen el proceso, ni de soluciones todo en uno (_one click install_)

- Redacte un reporte por equipo, en el que consigne los pasos que considere necesarios para explicar cómo realizó el proyecto, incluya capturas de pantalla que justifiquen su trabajo

- Incluya en su reporte un apartado de conclusiones referentes al trabajo realizado

- Puede agregar posibles errores, complicaciones, opiniones, críticas de el proyecto, o cualquier comentario relacionado

- Entregue su reporte de acuerdo a la [forma de entrega de tareas y prácticas][lineamientos-entrega] definida al inicio del curso


<!--
--------------------------------------------------------------------------------

## Monitoreo del estado de la infraestructura

![Nagios](https://nagios.redes.tonejito.cf/map.png)
-->

--------------------------------------------------------------------------------

[apache-userdir]: https://httpd.apache.org/docs/2.4/howto/public_html.html
[apache-redirect-https]: https://cwiki.apache.org/confluence/plugins/servlet/mobile?contentId=115522478#content/view/115522444
[apache-rewrite-https]: https://cwiki.apache.org/confluence/plugins/servlet/mobile?contentId=115522478#content/view/115522478
[api-telegram]: https://core.telegram.org/
[api-twitter]: https://developer.twitter.com/en/docs/twitter-api
[aws-ec2-ami]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/creating-an-ami-ebs.html
[aws-ec2-ami-share]: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/sharingamis-explicit.html
[hsts]: https://https.cio.gov/hsts/
[x-robots-tag]: https://developers.google.com/search/docs/advanced/robots/robots_meta_tag
[apache-auth-digest]: https://httpd.apache.org/docs/2.4/howto/auth.html
[cron]: https://opensource.com/article/17/11/how-use-cron-linux
[lineamientos-entrega]: https://redes-ciencias-unam.gitlab.io/workflow/

[flujo-de-trabajo]: https://redes-ciencias-unam.gitlab.io/workflow/
[repo-tareas]: https://gitlab.com/Redes-Ciencias-UNAM/2023-2/tareas-redes/-/merge_requests

[proyecto-web]: https://tinyurl.com/Redes-2023-2-Proyecto-Web
[lista-redes]: https://tinyurl.com/Lista-Redes-2023-2
<!-- [carpeta-drive]: https://tinyurl.com/ProyectoWeb-Redes-2023-2 -->
[carpeta-drive]: https://tinyurl.com/ProyectosRedes-2023-2

[playlist-https]: https://www.youtube.com/playlist?list=PLN1TFzSBXi3QGCMqARFoO1ePBX1P38erB
[video-protocolo-dns]: https://www.youtube.com/watch?v=r4PntflJs9E
[video-configuracion-ssh]: https://youtu.be/Hnu7BHBDcoM&t=1390
[video-configuracion-apache-debian]: https://youtu.be/XbQ_dBuERdM
[video-directivas-apache]: https://youtu.be/3JkQs3KcjxQ
[video-virtualhosts-apache-etc-hosts]: https://youtu.be/ZnqSNXIr-h4
[video-virtualhosts-apache-registros-dns]: https://youtu.be/JYo5rc4mhf0
[video-certificados-ssl-x509]: https://youtu.be/rXqkJi_FTuQ
[video-certificados-ssl-virtualhost-https-apache]: https://youtu.be/66dOHHD6L5I
[video-letsencrypt-certbot]: https://youtu.be/kpiChLT5JPs

[youtube-video-aws-summit-2020-cdmx-seguridad]: https://youtu.be/d3jnbtaLb24&list=PL2yQDdvlhXf_h40vMoMoh2SBa05geKLDq&index=10&
[youtube-video-aws-reinvent-2022-security]: https://www.youtube.com/watch?v=uFrj0jHN848&list=PL2yQDdvlhXf8bvQJuSP1DQ8vu75jdttlM&index=1&

[apache-docs]: https://httpd.apache.org/docs/2.4/
[apache-docs-config-sections]: https://httpd.apache.org/docs/2.4/sections.html
[apache-docs-security]: https://httpd.apache.org/docs/2.4/misc/security_tips.html
[apache-docs-server-wide]: https://httpd.apache.org/docs/2.4/server-wide.html
[apache-docs-url-rewrite]: https://httpd.apache.org/docs/2.4/rewrite/
[apache-docs-virtualhost]: https://httpd.apache.org/docs/2.4/vhosts/
[apache-docs-ssl]: https://httpd.apache.org/docs/2.4/ssl/
[apache-docs-htaccess]: https://httpd.apache.org/docs/2.4/howto/htaccess.html

[aws-ses-postfix]: https://docs.aws.amazon.com/ses/latest/dg/postfix.html
[aws-ses-software]: https://docs.aws.amazon.com/ses/latest/dg/send-email-smtp-software-package.html

[certbot-instructions-debian-10-buster]: https://certbot.eff.org/instructions?ws=apache&os=debianbuster

[rclone-gdrive]: https://rclone.org/drive/
[rclone-authorize]: https://rclone.org/commands/rclone_authorize/

[test-memcache]: files/test-memcache.sh
[test-redis]: files/test-redis.sh
